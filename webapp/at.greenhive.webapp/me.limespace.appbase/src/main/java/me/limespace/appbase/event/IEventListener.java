package me.limespace.appbase.event;

public interface IEventListener {

	void notify(IEvent event, IEventSource source);

}
