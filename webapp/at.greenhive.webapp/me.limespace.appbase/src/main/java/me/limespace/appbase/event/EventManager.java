package me.limespace.appbase.event;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class EventManager {
		
	List<IEventListener> eventListeners;
	List<IEventListener> errorListeners;
	
	public EventManager() {
		this.eventListeners = new CopyOnWriteArrayList<IEventListener>();
		this.errorListeners = new CopyOnWriteArrayList<IEventListener>();
	}
	
	synchronized public void registerListener(IEventListener listener) {
		eventListeners.add(listener);
	}
	
	synchronized public void unregisterListener(IEventListener listener) {
		eventListeners.remove(listener);
	}
	
	synchronized public void registerErrorListener(IEventListener listener) {
		errorListeners.add(listener);
	}
	
	synchronized public void unregisterErrorListener(IEventListener listener) {
		errorListeners.remove(listener);
	}
	
	public void fire (IEvent event, IEventSource source) {
		
		for (IEventListener listener: eventListeners) {
			try {
				listener.notify(event, source);
			}
			catch (Exception  e){ 
				fireError(new ApplicationErrorEvent(e), source);
			}
		}
	}
	
	public void fireError (ApplicationErrorEvent error,  IEventSource source) {
		
		for (IEventListener errorListener: errorListeners) {
			try {
				errorListener.notify(error, source);
			}
			catch (Exception  e){ 
				// Ignored - if error handling fails, we can't report errors anymore :-(
			}
		}
	}
}
