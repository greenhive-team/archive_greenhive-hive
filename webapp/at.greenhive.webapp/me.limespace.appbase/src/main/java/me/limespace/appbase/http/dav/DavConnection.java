package me.limespace.appbase.http.dav;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.List;
import java.util.Map.Entry;

public class DavConnection {

	private final URL url;
	private final String userid;
	private final String password;

	public DavConnection(String urlString, String user, String pwd) throws MalformedURLException {
		super();
		this.url = new URL(urlString);
		;
		this.userid = user;
		this.password = pwd;
	}

	public void discover() throws IOException {
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String encoding = Base64.getEncoder().encodeToString((userid + ":" + password).getBytes());

		conn.setRequestMethod("OPTIONS");

		conn.setDoOutput(false);
		conn.setRequestProperty("Authorization", "Basic " + encoding);

		for (Entry<String, List<String>> entry:conn.getHeaderFields().entrySet()) {
			System.out.print(entry.getKey());
			System.out.print("->");
			for (String value: entry.getValue()) {
				System.out.print(value + " , ");
			}
			System.out.println(".");
		}
		System.out.println(conn.getResponseMessage());
		System.out.println(conn.getContent());
		InputStream content = (InputStream) conn.getInputStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(content));
		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}
	}

}
