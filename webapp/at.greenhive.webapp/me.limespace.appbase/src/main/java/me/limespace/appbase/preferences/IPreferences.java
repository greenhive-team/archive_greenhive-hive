package me.limespace.appbase.preferences;

public interface IPreferences {
	public enum Context {
		APPLICATION, THINGY
	}
	
	String getValue(Enum<?> property);
	void setValue(Enum<?> property, String value);
	
}
