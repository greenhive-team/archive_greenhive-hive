package me.limespace.appbase.event;

public class UserRegisterRequestedEvent implements IEvent {
	private String userid;
	private String password;

	public UserRegisterRequestedEvent(String userid, String password) {
		this.userid = userid;
		this.password = password;
	}
	
	public String getUserid() {
		return userid;
	}
	
	public String getPassword() {
		return password;
	}
}
