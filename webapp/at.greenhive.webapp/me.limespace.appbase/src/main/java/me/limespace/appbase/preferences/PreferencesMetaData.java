package me.limespace.appbase.preferences;

import java.util.ArrayList;
import java.util.Collection;

public class PreferencesMetaData {
	Collection<PreferenceEntry> entries = new ArrayList<PreferenceEntry>();
	
	
//	public void addEntry(String name, PreferenceEntry.Type type) {
//		entries.add(new PreferenceEntry(name, type) );
//	}
	
	public void addEntry(PreferenceEntry entry) {
		entries.add(entry);
	}

	public void addEntry(Enum<?> name, PreferenceEntry.Type type) {
		entries.add(new PreferenceEntry(name.name(), type) );
	}
	
}
