package me.limespace.appbase.environment;

import me.limespace.appbase.preferences.IPreferences;

public interface IEnvironment {

	IPreferences getUserPreferences(IPreferences.Context context, String name, IUser user);

}
