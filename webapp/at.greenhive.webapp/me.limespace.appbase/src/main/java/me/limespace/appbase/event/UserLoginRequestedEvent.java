package me.limespace.appbase.event;

public class UserLoginRequestedEvent implements IEvent {

	private String userid;
	private String password;

	public UserLoginRequestedEvent(String userid, String password) {
		this.userid = userid;
		this.password = password;
	}
	
	public String getUserid() {
		return userid;
	}
	
	public String getPassword() {
		return password;
	}

}
