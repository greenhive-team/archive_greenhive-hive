package me.limespace.appbase.preferences;

public class PreferenceEntry {
	public enum Type {
		bool, text, number, color, file
	}
	
	final String name;
	final Type type;
	
	public PreferenceEntry(String name, Type type) {
		super();
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}
	
	public Type getType() {
		return type;
	}
	
	
}
