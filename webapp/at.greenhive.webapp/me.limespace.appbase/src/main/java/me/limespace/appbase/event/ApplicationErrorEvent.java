package me.limespace.appbase.event;

public class ApplicationErrorEvent implements IEvent{

	
	private final Exception cause;
	private final String message;

	public ApplicationErrorEvent(Exception exception) {
		this(null,exception);
	}
	
	public ApplicationErrorEvent(String message) {
		this(message,null);
	}
	
	public ApplicationErrorEvent(String message, Exception cause) {
		this.message = message;
		this.cause = cause;
	}

	public Exception getCause() {
		return cause;
	}
	
	public String getMessage() {
		return message;
	}
	
}
