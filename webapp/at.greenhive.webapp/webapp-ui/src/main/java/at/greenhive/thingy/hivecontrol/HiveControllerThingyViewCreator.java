package at.greenhive.thingy.hivecontrol;

import com.vaadin.ui.Component;

import at.greenhive.thingy.SmartView;
import at.greenhive.thingy.ThingyViewCreator;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class HiveControllerThingyViewCreator extends ThingyViewCreator<HiveControllerThingy>{

	public HiveControllerThingyViewCreator(HiveControllerThingy thingy, GreenhiveEventBus eventBus) {
		super(thingy,eventBus);
	}

	@Override
	public Component getRightView() {
		return new SmartView(thingy);
	}

	@Override
	public Component getMiddleView() {
		return new HiveControllerView(eventBus);
	}

	@Override
	public Component getLeftView() {
		return null;
	}
}