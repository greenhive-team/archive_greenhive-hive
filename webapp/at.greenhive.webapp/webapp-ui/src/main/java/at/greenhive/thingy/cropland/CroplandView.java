package at.greenhive.thingy.cropland;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addon.vol3.OLMap;
import org.vaadin.addon.vol3.OLView;
import org.vaadin.addon.vol3.OLViewOptions;
import org.vaadin.addon.vol3.client.OLCoordinate;
import org.vaadin.addon.vol3.client.OLExtent;
import org.vaadin.addon.vol3.client.OLOverlay;
import org.vaadin.addon.vol3.client.Projections;
import org.vaadin.addon.vol3.layer.OLTileLayer;
import org.vaadin.addon.vol3.source.OLOSMSource;
import org.vaadin.addon.vol3.source.OLOSMSourceOptions;

import com.peertopark.java.geocalc.DegreeCoordinate;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.GoogleMap.MapType;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapPolygon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapPolyline;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.hive.HiveGPSUpdateEvent;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.thingy.cropland.event.CroplandSelectionEvent;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class CroplandView extends Panel implements IEventListener, IEventSource {

	private static final long serialVersionUID = 1L;

	private static final String TRANSIT_WAYPOINT_ICON = "VAADIN/themes/greenhive/icons/map/TransitWaypoint.png";
	private static final String OUTLINE_WAYPOINT_ICON = "VAADIN/themes/greenhive/icons/map/OutlineWaypoint.png";
	private static final String ROW_WAYPOINT_ICON = "VAADIN/themes/greenhive/icons/map/RowWaypoint.png";

	@Autowired
	CroplandService croplandservice;
	PointCollector collector = new PointCollector();

	private GreenhiveEventBus eventBus;

	private GoogleMap googleMap;
	Collection<GoogleMapPolygon> activePolygon = new ArrayList<>();
	Collection<GoogleMapPolyline> activePolyline = new ArrayList<>();

	OLMap map = new OLMap();

	private IToolHandler mapToolHandler;

	private boolean updateFromDrone;

	private CroplandMetadata currentCroplandMetadata;

	public CroplandView(GreenhiveEventBus eventBus) {

		this.eventBus = eventBus;

		addAttachListener(e -> init());
		addDetachListener(e -> deinit());
	}

	void init() {
		HorizontalSplitPanel panel = new HorizontalSplitPanel();

		panel.setSplitPosition(33);
		panel.setFirstComponent(createLeftPanel());
		panel.setSecondComponent(createRightPanel());

		setContent(panel);
		setSizeFull();

		eventBus.register(this);
		GlobalEventManager.registerListener(this);
	}

	void deinit() {
		GlobalEventManager.unregisterListener(this);
		eventBus.unregister(this);
	}

	private Component createRightPanel() {
		TabSheet tab = new TabSheet();
		tab.setSizeFull();
		tab.addTab(createMapPanel(), "Map", VaadinIcons.MAP_MARKER);
		return tab;
	}

	private Component createLeftPanel() {
		return createCroplandGrid();
	}

	private Component createCroplandGrid() {

		Grid<CroplandMetadata> croplandGrid = new Grid<CroplandMetadata>("Ackerflächen");

		croplandGrid.addColumn(c -> c.getClid()).setCaption("ID");
		croplandGrid.addColumn(c -> c.getName()).setCaption("Name");
		croplandGrid.addColumn(c -> c.getCroptype().toString()).setCaption("Type");

		croplandGrid.setItems(croplandservice.getAll());
		croplandGrid.setSizeFull();

		croplandGrid
				.addSelectionListener(e -> handleCroplandSelection(e.getFirstSelectedItem(), e.getAllSelectedItems()));

		return croplandGrid;
	}

	private Component createMapPanel() {

		AbsoluteLayout layout = new AbsoluteLayout();
		// HorizontalLayout layout = new HorizontalLayout();

		layout.setSizeFull();

		Component toolbar = createMapToolbar();
		Component map = createMap();
		Component map2 = createOSMMap();

		layout.addComponent(map);
		layout.addComponent(toolbar, "left: 5px; top: 5px");

		return layout;
	}

	private Component createMapToolbar() {
		VerticalLayout toolbar = new VerticalLayout();
		toolbar.setSpacing(false);
		toolbar.setMargin(false);
		Button outlineTool = new Button(VaadinIcons.THIN_SQUARE, e -> handleOutlineTool());
		Button rowTool = new Button(VaadinIcons.LINES, e -> handleRowTool());
		Button transitTool = new Button(VaadinIcons.ROAD_SPLIT, e -> handleTransitTool());

		Button getFromDrone = new Button(VaadinIcons.ABSOLUTE_POSITION, e -> handleGetFromDrone());

		toolbar.addComponents(outlineTool, rowTool, transitTool, getFromDrone);

		return toolbar;
	}

	private void handleGetFromDrone() {

	}

	private void handleRowTool() {
		mapToolHandler = new RowToolHandler(collector);
	}

	private void handleTransitTool() {
		// TODO Auto-generated method stub
		mapToolHandler = new TransitToolHandler(collector);
	}

	private void handleOutlineTool() {
		mapToolHandler = new OutlineToolHandler(collector);
	}

	private Component createOSMMap() {
		map = new OLMap();

		// add layer to map
		OLOSMSourceOptions opts = new OLOSMSourceOptions();
		opts.setCustomAttributions(new String[] { "custom attribution" });
		opts.setMaxZoom(1000);
		opts.setShowAttributions(true);
		OLOSMSource mapSource = new OLOSMSource(opts);
		OLTileLayer layer = new OLTileLayer(mapSource);
		layer.setTitle("MapQuest OSM");
		map.addLayer(layer);

		map.addClickListener(e -> {
			if (mapToolHandler != null) {
				mapToolHandler.handleMapClick(e);
			}
		});
		map.setView(createView());

		map.setSizeFull();
		// add the map to the layout

		return map;

	}

	protected OLView createView() {
		OLViewOptions opts = new OLViewOptions();
		opts.setInputProjection(Projections.EPSG4326);
		OLView view = new OLView(opts);
		view.setZoom(1);
		view.setCenter(0, 0);
		return view;
	}

	private Component createMap() {
		// String APIKEY = System.getProperty("APIKEY",
		// "AIzaSyA0mTfKiCnQGnjQXsQ38wWjMj6wDIXDUgc");

		String APIKEY = System.getProperty("APIKEY", "AIzaSyAw_FCALO6J57ZEPwmesaaI5d-hcSoD66A");

		googleMap = new GoogleMap(APIKEY, null, null);
		// googleMap = new GoogleMap(null, null, null);
		googleMap.setCenter(new LatLon(47.067007, 16.145151));
		googleMap.setZoom(50);
		googleMap.setMapType(MapType.Hybrid);
		googleMap.setSizeFull();

		googleMap.addMapClickListener(e -> {
			if (mapToolHandler != null) {
				mapToolHandler.handleMapClick(e);
			}
		});

		googleMap.addMarkerDragListener((e, p) -> {
			if (mapToolHandler != null) {
				mapToolHandler.handleMarkerDrag(e, p);
			}
		});
		return googleMap;
	}

	private void handleCroplandSelection(Optional<CroplandMetadata> firstSelectedItem,
			Set<CroplandMetadata> allSelectedItems) {
		if (firstSelectedItem.isPresent()) {
			currentCroplandMetadata = firstSelectedItem.get();
			showMap(currentCroplandMetadata);
			eventBus.post(new CroplandSelectionEvent(firstSelectedItem, allSelectedItems, this));
		}
	}

	@Override
	public void notify(IEvent event, IEventSource source) {
		if (source == this) {
			return;
		}

		if (updateFromDrone && event instanceof DroneStatusUpdateEvent) {

			updateFromDrone = false;

			DroneStatusUpdateEvent de = ((DroneStatusUpdateEvent) event);

			GPSPoint gpsPoint = new GPSPoint(new DegreeCoordinate(de.getLatitude()),
					new DegreeCoordinate(de.getLongitute()), (int) de.getAltitute());
			if (mapToolHandler != null) {
				mapToolHandler.setPoint(gpsPoint);
			}
		}
	}

	private void showMap2(CroplandMetadata croplandmeta) {
		map.removeOverlays();
	}

	private void showMap(CroplandMetadata croplandmeta) {

		googleMap.clearMarkers();
		activePolygon.forEach(p -> googleMap.removePolygonOverlay(p));
		activePolyline.forEach(p -> googleMap.removePolyline(p));

		// Get & Draw Outline
		Cropland cropland = croplandservice.getCroplandDetail(croplandmeta.getClid());
		List<GeoPoint> croplandOutline = cropland.getOutline();

		ArrayList<LatLon> points = new ArrayList<LatLon>(croplandOutline.size());
		boolean first = true;
		OLExtent extent = new OLExtent();

		for (GeoPoint gp : croplandOutline) {
			LatLon latLon = new LatLon(gp.getLatitudeDecimalDegrees(), gp.getLongitudeDecimalDegrees());
			points.add(latLon);

			extent.extend(new OLCoordinate(gp.getLongitudeDecimalDegrees(), gp.getLatitudeDecimalDegrees()));

			GoogleMapMarker marker = new GoogleMapMarker("", latLon, true, OUTLINE_WAYPOINT_ICON);
			marker.setAnimationEnabled(false);
			googleMap.addMarker(marker);
			if (first) {
				first = false;
				googleMap.setCenter(latLon);
				// OLCoordinate center = new OLCoordinate(gp.getLatitudeDecimalDegrees(),
				// gp.getLongitudeDecimalDegrees());
				OLCoordinate center = new OLCoordinate(gp.getLongitudeDecimalDegrees(), gp.getLatitudeDecimalDegrees());
				map.getView().setCenter(center);

			}
		}

		map.getView().fitExtent(extent);
		GoogleMapPolyline outlineOverlay = new GoogleMapPolyline(points, "#5bc653", 0.7, 3);
		googleMap.addPolyline(outlineOverlay);
		activePolyline.add(outlineOverlay);
		// GoogleMapPolygon overlay = new GoogleMapPolygon(points, "#5bc653", 0.5,
		// "#194915", 0.5, 1);
		// activePolygon.add(overlay);
		// googleMap.addPolygonOverlay(overlay);

		// Get & Draw Transit
		List<CroplandPath> transitPath = cropland.getPaths();

		first = true;
		for (CroplandPath path : transitPath) {
			ArrayList<LatLon> transitPoints = new ArrayList<LatLon>();
			for (GeoPoint gp : path.getPath()) {
				LatLon latLon = new LatLon(gp.getLatitudeDecimalDegrees(), gp.getLongitudeDecimalDegrees());
				GoogleMapMarker marker = new GoogleMapMarker("", latLon, true, TRANSIT_WAYPOINT_ICON);
				marker.setAnimationEnabled(false);
				googleMap.addMarker(marker);

				transitPoints.add(latLon);
			}
			GoogleMapPolyline transitOverlay = new GoogleMapPolyline(transitPoints, "#ff2222", 0.7, 3);

			activePolyline.add(transitOverlay);
			googleMap.addPolyline(transitOverlay);
		}

		// Get & Draw Rows
		List<CroplandRow> croplandRows = cropland.getRows();

		for (CroplandRow row : croplandRows) {
			List<GeoPoint> path = row.getPath();
			points = new ArrayList<LatLon>(path.size());
			for (GeoPoint gp : path) {
				LatLon latLon = new LatLon(gp.getLatitudeDecimalDegrees(), gp.getLongitudeDecimalDegrees());
				GoogleMapMarker marker = new GoogleMapMarker("", latLon, true, ROW_WAYPOINT_ICON);
				marker.setAnimationEnabled(false);
				googleMap.addMarker(marker);

				points.add(latLon);
			}
			GoogleMapPolyline rowline = new GoogleMapPolyline(points, "#5b1e25", 0.5, 2);
			googleMap.addPolyline(rowline);
			activePolyline.add(rowline);
		}

		googleMap.setZoom(100);

	}
}
