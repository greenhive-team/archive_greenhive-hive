package at.greenhive.thingy.settings;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.peertopark.java.geocalc.DegreeCoordinate;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.ProgressListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import at.greenhive.bl.event.GGAMessageUpdateEvent;
import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.hive.HiveGPSUpdateEvent;
import at.greenhive.bl.hive.HiveService;
import at.greenhive.bl.imports.XLSXImporter;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.thingy.cropland.CroplandService;
import at.greenhive.thingy.operation.OperationService;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class SettingsView extends Panel implements IEventListener, IEventSource {

	private static final long serialVersionUID = 1L;
	
	
	@Autowired
	CroplandService croplandService;
	
	@Autowired
	OperationService operationService;
	
	private final GreenhiveEventBus eventBus;

	private long lastTime = 0;
	
	TextField cLat = new TextField("Lat.");
	TextField cLon = new TextField("Lon.");
	TextField cAlt = new TextField("Alt.");

	private boolean updateFromDrone = false;
	private boolean updateFromHive = false;

	// private List<MissionTask> currentsTasks;

	static class PosViewBean {

		double lat;
		double lon;
		long alt;

		public long getAlt() {
			return alt;
		}

		public double getLat() {
			return lat;
		}

		public double getLon() {
			return lon;
		}

		public void setAlt(long alt) {
			this.alt = alt;
		}

		public void setLat(double lat) {
			this.lat = lat;
		}

		public void setLon(double lon) {
			this.lon = lon;
		}
	}

	class UploadHandler implements Receiver, ProgressListener, FailedListener, SucceededListener {
		private static final long serialVersionUID = 1L;

		ProgressBar progress;
		ByteArrayOutputStream os = 	new ByteArrayOutputStream(10240);

		UploadHandler(ProgressBar progress) {
			this.progress = progress;
		}

		@Override
		public OutputStream receiveUpload(String filename, String mimeType) {
			return os;
		}

		@Override
		public void uploadSucceeded(SucceededEvent event) {
			progress.setVisible(false);
			try {
				importData(os);
				Notification.show("Import done","All data has been inserted sucessfully",Notification.Type.HUMANIZED_MESSAGE);
			} catch (Exception e) {
				Notification.show("Import failed", Notification.Type.ERROR_MESSAGE);
			}

		}

		@Override
		public void uploadFailed(FailedEvent event) {
			Notification.show("Upload failed", Notification.Type.ERROR_MESSAGE);

		}

		@Override
		public void updateProgress(long readBytes, long contentLength) {
			progress.setVisible(true);
			if (contentLength == -1)
				progress.setIndeterminate(true);
			else {
				progress.setIndeterminate(false);
				progress.setValue(((float) readBytes) / ((float) contentLength));

			}
		}

	}
	
	public void importData(ByteArrayOutputStream os) throws IOException {
		
		XLSXImporter importer = new XLSXImporter(croplandService, operationService);
		importer.importData(new ByteArrayInputStream(os.toByteArray()));
		
		//importService.importXLSX(new ByteArrayInputStream(os.toByteArray()));
	}

	public SettingsView(GreenhiveEventBus eventBus) {

		this.eventBus = eventBus;

		addAttachListener(e -> init());
		addDetachListener(e -> deinit());

	}

	void init() {
		eventBus.register(this);
		GlobalEventManager.registerListener(this);

		VerticalLayout main = new VerticalLayout();
		main.setSizeFull();
		main.setMargin(false);
		main.setSpacing(true);

		Panel hiveSettingsPanel = getHiveSettingsPanel();
		Panel rtkPanel = getHiveRTKPanel();
		Panel importPanel = getImportPanel();

		main.addComponents(hiveSettingsPanel, rtkPanel, importPanel);
		setContent(main);
		setSizeFull();
		
		updateHiveLocationInfo();

	}

	private void updateHiveLocationInfo() {
		GPSPoint hiveLocation = HiveService.getHiveLocation();
		
		hiveLocation.getLatitudeDecimalDegrees();
		
		cLat.setValue(String.valueOf(hiveLocation.getLatitudeDecimalDegrees()));
		cLon.setValue(String.valueOf(hiveLocation.getLongitudeDecimalDegrees()));
		cAlt.setValue(String.valueOf(hiveLocation.getAltitude()));
		
	}

	private Panel getImportPanel() {
		Panel panel = new Panel("Import");
		ProgressBar progress = new ProgressBar(0.0f);
		progress.setVisible(false);

		Upload upload = new Upload("Upload Corpland / Operation data:", null);
		
		UploadHandler uploadHandler = new UploadHandler(progress);
		upload.setReceiver(uploadHandler);
        upload.addProgressListener(uploadHandler);
        upload.addFailedListener(uploadHandler);
        upload.addSucceededListener(uploadHandler);

		VerticalLayout panelContent = new VerticalLayout();
		panelContent.setSpacing(true);
		panel.setContent(panelContent);
		panelContent.addComponent(upload);
		panelContent.addComponent(progress);

		return panel;
	}

	private Panel getHiveRTKPanel() {
		
		Panel panel = new Panel("RTK");
		CssLayout hwSelector = new CssLayout();
		hwSelector.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

		ComboBox<String> hwAvail = new ComboBox<>("RTK Serial", HiveService.getHiveController().listHardware());

		Button hwConnect = new Button(VaadinIcons.CONNECT);
		hwConnect.addClickListener((e) -> {
			String id = hwAvail.getSelectedItem().get();
			if (HiveService.getHiveController().startRTKListening(id)) {
				Notification.show("Connected", "Successfully connect to '" + id + "'", Type.HUMANIZED_MESSAGE);
			} else {
				Notification.show("NOT Connected", "Couldn't connect to '" + id + "'", Type.ERROR_MESSAGE);
			}
		});

		hwAvail.addSelectionListener((e) -> {
			hwConnect.setEnabled(true);
		});

		Button hwRefresh = new Button(VaadinIcons.REFRESH);
		hwRefresh.addClickListener((e) -> {
			hwAvail.setItems(HiveService.getHiveController().listHardware());
		});

		hwSelector.addComponents(hwAvail, hwRefresh, hwConnect);

		VerticalLayout layout = new VerticalLayout(hwSelector);
		panel.setContent(layout);
		return panel;

	}
	private Panel getHiveSettingsPanel() {
		Panel panel = new Panel("Hive Einstellungen");

		HorizontalLayout currentHivePosLayout = new HorizontalLayout();
		Label cCap = new Label("Aktuelle Position");
		cCap.setWidth(105, Unit.PIXELS);
//		TextField cLat = new TextField("Lat.");
//		TextField cLon = new TextField("Lon.");
//		TextField cAlt = new TextField("Alt.");

		currentHivePosLayout.addComponents(cCap, cLat, cLon, cAlt);
		currentHivePosLayout.setComponentAlignment(cCap, Alignment.BOTTOM_LEFT);
		currentHivePosLayout.setComponentAlignment(cLat, Alignment.BOTTOM_LEFT);
		currentHivePosLayout.setComponentAlignment(cLon, Alignment.BOTTOM_LEFT);
		currentHivePosLayout.setComponentAlignment(cAlt, Alignment.BOTTOM_LEFT);

		HorizontalLayout newHivePosLayout = new HorizontalLayout();
		Label nCap = new Label("Neue Position");
		nCap.setWidth(105, Unit.PIXELS);
		TextField nLat = new TextField("Lat.");
		TextField nLon = new TextField("Lon.");
		TextField nAlt = new TextField("Alt.");

		Binder<PosViewBean> binder = new Binder<>();
		binder.forField(nLon)
				.withValidator(string -> string != null && !string.isEmpty(), "Input values should not be empty")
				.withConverter(Double::parseDouble, String::valueOf, "Input value should be an double")
				.bind(PosViewBean::getLon, PosViewBean::setLon);

		binder.forField(nLat)
				.withValidator(string -> string != null && !string.isEmpty(), "Input values should not be empty")
				.withConverter(Double::parseDouble, String::valueOf, "Input value should be an double")
				.bind(PosViewBean::getLat, PosViewBean::setLat);

		binder.forField(nAlt)
				.withValidator(string -> string != null && !string.isEmpty(), "Input values should not be empty")
				.withConverter(Long::valueOf, String::valueOf, "Input value should be an double")
				.bind(PosViewBean::getAlt, PosViewBean::setAlt);

		Button setButton = new Button(("Set"));
		setButton.addClickListener(e -> {
			PosViewBean posViewBean = new PosViewBean();
			try {
				binder.writeBean(posViewBean);
				updateHiveLocation(posViewBean);
				updateHiveLocationInfo();
			} catch (ValidationException e1) {

			}

		});

		newHivePosLayout.addComponents(nCap, nLat, nLon, nAlt, setButton);
		newHivePosLayout.setComponentAlignment(nCap, Alignment.BOTTOM_LEFT);
		newHivePosLayout.setComponentAlignment(nLat, Alignment.BOTTOM_LEFT);
		newHivePosLayout.setComponentAlignment(nLon, Alignment.BOTTOM_LEFT);
		newHivePosLayout.setComponentAlignment(nAlt, Alignment.BOTTOM_LEFT);
		newHivePosLayout.setComponentAlignment(setButton, Alignment.BOTTOM_LEFT);

		Button button = new Button(("Set from Drone"));
		button.addClickListener(e -> {
			updateFromDrone = true;
		});
			
		Button button2 = new Button(("Set from Hive GPS"));
		button2.addClickListener(e -> {
			updateFromHive = true;
		});
		
		VerticalLayout layout = new VerticalLayout(currentHivePosLayout, newHivePosLayout, button ,button2);
		panel.setContent(layout);
		return panel;
	}

	private void updateHiveLocation(PosViewBean posViewBean) {

		GPSPoint gpsPoint = new GPSPoint(new DegreeCoordinate(posViewBean.getLat()),
				new DegreeCoordinate(posViewBean.getLon()), posViewBean.getAlt());

		HiveGPSUpdateEvent event = new HiveGPSUpdateEvent(gpsPoint);
		GlobalEventManager.fire(event, this);
	}

	void deinit() {
		GlobalEventManager.unregisterListener(this);
		eventBus.unregister(this);
	}

	@Override
	public void notify(IEvent event, IEventSource source) {
		if (source == this) {
			return;
		}

		if (event instanceof DroneStatusUpdateEvent) {
			
			if (updateFromDrone ) {
				updateFromDrone = false;
				
				DroneStatusUpdateEvent de = ((DroneStatusUpdateEvent) event);
				
				GPSPoint gpsPoint = new GPSPoint(new DegreeCoordinate(de.getLatitude()),
												 new DegreeCoordinate(de.getLongitute()),
												 (int)de.getAltitute()) ;

				GlobalEventManager.fire(new HiveGPSUpdateEvent(gpsPoint), this);
				updateHiveLocationInfo();
			}
		}
		
		if (event instanceof GGAMessageUpdateEvent) {
			
			if (updateFromHive ) {
				updateFromHive = false;
				
				GGAMessageUpdateEvent de = ((GGAMessageUpdateEvent) event);
				
				String strLat = de.getLat();
				
				double degLat = Double.parseDouble(strLat.substring(0,2));
				double minLat = Double.parseDouble(strLat.substring(2));
				
				degLat += minLat/60;
				
				String strLon = de.getLon();
				
				System.out.println("Set Hive: LON:" + strLon +", LAT:" + strLat);
				double degLon = Double.parseDouble(strLon.substring(0,3));
				double minLon = Double.parseDouble(strLon.substring(3));
				
				degLon += minLon/60;
				
				GPSPoint gpsPoint = new GPSPoint(new DegreeCoordinate(degLat),
												 new DegreeCoordinate(degLon),
												 (int)300) ;

				GlobalEventManager.fire(new HiveGPSUpdateEvent(gpsPoint), this);
				updateHiveLocationInfo();
			}
		}

	}

}
