package at.greenhive.thingy.hivecontrol;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import at.greenhive.bl.hive.HiveService;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class HiveControllerView extends Panel {

	private static final long serialVersionUID = 1L;
	private GreenhiveEventBus eventBus;
	

	final private int XSTEPS = 50000;
	final private int YSTEPS = 80000;
	final private int ZSTEPS = 100000;
	
	public HiveControllerView(GreenhiveEventBus eventBus) {

		this.eventBus = eventBus;

		addAttachListener(e -> init());
		addDetachListener(e -> deinit());

	}

	void init() {
		eventBus.register(this);

		VerticalLayout panel = new VerticalLayout();
		CssLayout hwSelector = new CssLayout();
		hwSelector.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

		ComboBox<String> hwAvail = new ComboBox<>("Connections", HiveService.getHiveController().listHardware());

		Button hwConnect = new Button(VaadinIcons.CONNECT);
		hwConnect.addClickListener((e) -> {
			String id = hwAvail.getSelectedItem().get();
			if (HiveService.getHiveController().connectToHardware(id)) {
				Notification.show("Connected", "Successfully connect to '" + id + "'", Type.HUMANIZED_MESSAGE);
			} else {
				Notification.show("NOT Connected", "Couldn't connect to '" + id + "'", Type.ERROR_MESSAGE);
			}
		});

		hwAvail.addSelectionListener((e) -> {
			hwConnect.setEnabled(true);
		});

		Button hwRefresh = new Button(VaadinIcons.REFRESH);
		hwRefresh.addClickListener((e) -> {
			hwAvail.setItems(HiveService.getHiveController().listHardware());
		});

		hwSelector.addComponents(hwAvail, hwRefresh, hwConnect);

		panel.addComponent(hwSelector);

		Component ctrlButtonGrid = createCtrlButtonGrid();
		panel.addComponent(ctrlButtonGrid);

		setContent(panel);
		setSizeFull();
	}

	private GridLayout createCtrlButtonGrid() {
		GridLayout ctrlButtonGrid = new GridLayout(3, 4);

		Button btnLeft = new Button(VaadinIcons.ARROW_LEFT);
		Button btnRight = new Button(VaadinIcons.ARROW_RIGHT);
		
		Button btnUp = new Button(VaadinIcons.ARROW_UP);
		Button btnDown = new Button(VaadinIcons.ARROW_DOWN);
		
		Button btnFwd = new Button(VaadinIcons.ARROW_FORWARD);
		Button btnBwd = new Button(VaadinIcons.ARROW_BACKWARD);

		btnLeft.addClickListener((e) -> moveLeft());
		btnRight.addClickListener((e) -> moveRight());

		btnUp.addClickListener((e) -> moveUp());
		btnDown.addClickListener((e) -> moveDown());
		
		btnFwd.addClickListener((e) -> moveForward());
		btnBwd.addClickListener((e) -> moveBackward());
		
		ctrlButtonGrid.addComponent(btnLeft, 0, 1);
		ctrlButtonGrid.addComponent(btnRight, 2, 1);
		ctrlButtonGrid.addComponent(btnUp, 1, 0);
		ctrlButtonGrid.addComponent(btnDown, 1, 2);
		
		ctrlButtonGrid.addComponent(btnFwd, 0, 3);
		ctrlButtonGrid.addComponent(btnBwd, 2, 3);
		
		
		return ctrlButtonGrid;
	}

	private void moveForward() {
		HiveService.getHiveController().moveY(YSTEPS);
	}
	
	private void moveBackward() {
		HiveService.getHiveController().moveY(-YSTEPS);
	}
	
	private void moveLeft() {
		HiveService.getHiveController().moveX(-XSTEPS);
	}

	private void moveRight() {
		HiveService.getHiveController().moveX(XSTEPS);
	}

	private void moveUp() {
		HiveService.getHiveController().moveZ(ZSTEPS);
	}

	private void moveDown() {
		HiveService.getHiveController().moveZ(-ZSTEPS);
	}

	void deinit() {
		eventBus.unregister(this);
	}

}
