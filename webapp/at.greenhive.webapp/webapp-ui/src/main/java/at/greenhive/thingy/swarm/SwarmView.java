package at.greenhive.thingy.swarm;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.eventbus.Subscribe;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalSplitPanel;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.protobuf.Types.DroneStatus;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.thingy.swarm.event.SwarmChangeEvent;
import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class SwarmView extends Panel implements IEventListener {
	private static final long serialVersionUID = 1L;

	@Autowired
	SwarmManager swarmManager;

	private GreenhiveEventBus eventBus;
	
	private Grid<DroneStatusViewBean> grid;
	private GoogleMap googleMap;
	
	HorizontalSplitPanel outerSplit;

	Set<String> registeredBees = new HashSet<>();

	LinkedList<DroneStatusViewBean> events = new LinkedList<>();

	private static class DroneStatusViewBean {
		private String id;
		private double battery;
		private DroneStatus status;
		private double altitute;
		private double longitute;
		private double latitude;
		private LocalDateTime timestamp;

		public DroneStatusViewBean(DroneStatusUpdateEvent event) {

			this.id = event.getDroneId();

			this.latitude = event.getLatitude();
			this.longitute = event.getLongitute();
			this.altitute = event.getAltitute();

			this.status = event.getStatus();
			this.battery = event.getBattery();

			this.timestamp = event.getTimestmp();
		}

		public String getId() {
			return id;
		}

		public Double getLatitude() {
			return latitude;
		}

		public Double getLongitute() {
			return longitute;
		}

		public Double getAltitute() {
			return altitute;
		}

		public Double getBattery() {
			return battery;
		}

		public String getStatus() {
			return status.name();
		}

		public LocalDateTime getTimestamp() {
			return timestamp;
		}
	}

	public SwarmView(GreenhiveEventBus eventBus) {
		this.eventBus = eventBus;
		addAttachListener(e -> init());
	}

	void init() {

		outerSplit = new HorizontalSplitPanel();

		VerticalSplitPanel mainLayout = new VerticalSplitPanel();
		mainLayout.setSizeFull();

		outerSplit.setFirstComponent(mainLayout);
		outerSplit.setSecondComponent(createMap());
		outerSplit.setSplitPosition(100, Unit.PERCENTAGE);

		Component swarmPanel = createSwarmPanel();

		grid = createDrohneStatusGrid();

		mainLayout.setFirstComponent(swarmPanel);
		mainLayout.setSecondComponent(grid);

		mainLayout.setSplitPosition(70f);
		setContent(mainLayout);
		setSizeFull();
	}

	private Component createMap() {
		googleMap = new GoogleMap("AIzaSyA0mTfKiCnQGnjQXsQ38wWjMj6wDIXDUgc", null, null);
		googleMap.setSizeFull();
		return googleMap;
	}

	private Grid<DroneStatusViewBean> createDrohneStatusGrid() {
		Grid<DroneStatusViewBean> grid = new Grid<>();

		grid.addColumn(DroneStatusViewBean::getId).setCaption("ID");
		grid.addColumn(DroneStatusViewBean::getBattery).setCaption("Battery");
		grid.addColumn(DroneStatusViewBean::getStatus).setCaption("Status");
		grid.addColumn(DroneStatusViewBean::getLatitude).setCaption("Lat");
		grid.addColumn(DroneStatusViewBean::getLongitute).setCaption("Long");
		grid.addColumn(DroneStatusViewBean::getAltitute).setCaption("Alt");
		grid.addColumn(DroneStatusViewBean::getTimestamp)
				// .setRenderer( new DateRenderer("%1$tB %1$te, %1$tY", Locale.ENGLISH))
				.setCaption("Timestamp");

		grid.setSizeFull();

		return grid;

	}

	private Component createSwarmPanel() {
		Collection<Swarm> swarmList = swarmManager.getSwarmList();

		if (swarmList.size() == 0) {
			return new Label("no Swarm defined so far");
		}

		if (swarmList.size() > 1) {
			TabSheet parent = new TabSheet();
			parent.setSizeFull();
			for (Swarm swarm : swarmList) {
				Component swarmPanel = createSwarmPage(swarm);
				parent.addTab(swarmPanel, swarm.getName());
			}
			return parent;
		}

		Component swarmPanel = createSwarmPage(swarmList.iterator().next());
		return swarmPanel;

	}

	private Component createSwarmPage(Swarm swarm) {
		int cols = swarm.getDrones().size();
		if (cols > 0) {
			GridLayout layout = new GridLayout(cols, 1);
			layout.setSizeFull();
			for (Drone bee : swarm.getDrones()) {
				DronePanel drohnePanel = new DronePanel(eventBus, bee);
				layout.addComponent(drohnePanel);
			}
			return layout;
		}
		return new Label("No Bumbelbees defined for this swarm.");
	}

	@Override
	public void attach() {
		super.attach();
		eventBus.register(this);
		GlobalEventManager.registerListener(this);
	}

	@Override
	public void detach() {
		super.detach();
		eventBus.unregister(this);
		GlobalEventManager.unregisterListener(this);
	}

	@Subscribe
	void updateDrohneStatus(DroneStatusUpdateEvent event) {

		DroneStatusViewBean ds = new DroneStatusViewBean(event);
		events.addFirst(ds);
		updateMap(ds);
		this.getUI().access(() -> {
			grid.setItems(events);
			grid.markAsDirty();
		});
	}

	@Subscribe
	void updateDrohneMap(RegisterDrone4MapEvent event) {
		if (event.isRegister()) {
			registeredBees.add(event.getBee().getSerialNumber());
		} else {
			registeredBees.remove(event.getBee().getSerialNumber());
		}
		
		if (registeredBees.size() > 0) {
			if (outerSplit.getSplitPosition() > 98) {
				outerSplit.setSplitPosition(75, Unit.PERCENTAGE);
			}
		} else {
			if (outerSplit.getSplitPosition() < 98) {
				outerSplit.setSplitPosition(100, Unit.PERCENTAGE);
			}
		}
	}

	private void updateMap(DroneStatusViewBean ds) {
		
		if (registeredBees.contains(ds.getId()) ) {
			Map<String,GoogleMapMarker> markers = new HashMap<>();
			
			GoogleMapMarker currentMarker = markers.get(ds.id);
			if (currentMarker != null ) {
				googleMap.removeMarker(currentMarker);
			}
			
			GoogleMapMarker marker = new GoogleMapMarker(ds.getId(), new LatLon(ds.getLatitude(), ds.getLongitute()), false, null);
			googleMap.addMarker(marker);
		}
	}
	
	
	@Override
	public void notify(IEvent event, IEventSource source) {
		if (event instanceof DroneStatusUpdateEvent) {
			updateDrohneStatus((DroneStatusUpdateEvent) event);
		} else if (event instanceof SwarmChangeEvent) {
			reInit();
		}
	}

	private void reInit() {
		this.getUI().access(() -> {
			init();
		});
	}

}
