package at.greenhive.thingy.cropland;

import org.vaadin.addon.vol3.OLMap.OLClickEvent;

import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;

import at.greenhive.bl.util.geo.GPSPoint;

public class OutlineToolHandler implements IToolHandler {

	private final PointCollector collector;

	public OutlineToolHandler(PointCollector collector) {
		this.collector = collector;
	}

	@Override
	public void handleMapClick(LatLon position) {
		System.out.println("outline tool:" + asString(position));
	}

	@Override
	public void handleMarkerDrag(GoogleMapMarker marker, LatLon position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleMapClick(OLClickEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPoint(GPSPoint gpsPoint) {
		collector.addBoundaryPoint(gpsPoint);
	}

}
