package at.greenhive.webapp.login;


import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.UserLoginRequestedEvent;
import me.limespace.appbase.event.UserRegisterRequestedEvent;

public class LoginView extends HorizontalLayout {
	private static final long serialVersionUID = 1L;

	final private int INPUT_WIDTH = 300;
	final Button btnLogin;
	final Button btnRegister;

	private GreenhiveEventBus eventbus;

	public LoginView(GreenhiveEventBus eventbus) {

		this.eventbus = eventbus;
		
		setSpacing(true);
		setSizeFull();
		
		
		VerticalLayout panel = new VerticalLayout();
		
		addStyleName("logon");
		//addStyleName("fancy");
		addStyleName(ValoTheme.PANEL_BORDERLESS);

		Label spacer1 = new Label();
		panel.addStyleName("logonpanel");
		panel.addComponent(spacer1);
		panel.setExpandRatio(spacer1, 2);
		panel.setHeight(100,Unit.PERCENTAGE);
		panel.setWidth(600, Unit.PIXELS);
		
		/* Welcome Text */

		Label welcome = new Label("Welcome to Greenhive");
		welcome.addStyleName("welcome1");
		
		Label motto = new Label("every grower want’s to live unburdend");
		motto.addStyleName("welcome2");
		
		AbsoluteLayout welcomeArea = new AbsoluteLayout();
		
		welcomeArea.setHeight(80, Unit.PIXELS);
		welcomeArea.setWidth(550, Unit.PIXELS);
		

		welcomeArea.addComponent(welcome, "top:16px");
		welcomeArea.addComponent(motto, "left:230px");

		panel.addComponent(welcomeArea);
		panel.setComponentAlignment(welcomeArea, Alignment.MIDDLE_CENTER);
		
		panel.addComponent(new Label("<hr/>", ContentMode.HTML));

		/* Username / Password */
		final TextField username = addUsernameTextfield(panel);
		final PasswordField password = addPasswordField(panel);

		/* Button Bar */
		HorizontalLayout buttonbar = new HorizontalLayout();
		buttonbar.setWidth(INPUT_WIDTH, Unit.PIXELS);
		buttonbar.setHeight(50, Unit.PIXELS);
		buttonbar.setMargin(false);
		btnLogin = addOKButton(username, password);
		btnRegister = addRegisterButton(username, password);
		buttonbar.addComponent(btnLogin);
		buttonbar.addComponent(btnRegister);
		buttonbar.setComponentAlignment(btnLogin, Alignment.TOP_LEFT);
		buttonbar.setComponentAlignment(btnRegister, Alignment.TOP_RIGHT);
		panel.addComponent(buttonbar);
		panel.setComponentAlignment(buttonbar, Alignment.MIDDLE_CENTER);

		
		Label spacer2 = new Label();
		panel.addComponent(spacer2);
		panel.setExpandRatio(spacer2, 3);
		
		
		Label spacer3 = new Label();
		Label spacer4 = new Label();
		addComponent(spacer3);
		addComponent(panel);
		addComponent(spacer4);
		setExpandRatio(spacer3, 1);
		//setExpandRatio(panel, 5);
		setExpandRatio(spacer4, 10);
		username.focus();
	}

	
	private PasswordField addPasswordField_inlineIcon() {
		final PasswordField password = new PasswordField();
		password.setId("password_txt");
		password.setIcon(new ThemeResource("icons/login/Password-25.png"));
		//password.setInputPrompt("password");
		//password.addStyleName(ValoTheme.TEXTFIELD_BORDERLESS);
		password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		password.addStyleName("fancy");
		password.setWidth(INPUT_WIDTH, Unit.PIXELS);
		
		
		password.addValueChangeListener(event -> {
				if (event.getValue().length() > 0) {
					setButtonEnableState(true);
				} else {
					setButtonEnableState(false);
				}
		});
		
		addComponent(password);
		setComponentAlignment(password, Alignment.MIDDLE_CENTER);
		
		return password;
		
	}
	
	private TextField addUsernameTextfield_inlineIcon() {
		final TextField username = new TextField();
		username.setId("username_txt");
		username.setIcon( new ThemeResource("icons/login/Gender Neutral User-26.png"));
		//username.setInputPrompt("username");
      	//username.addStyleName(ValoTheme.TEXTFIELD_BORDERLESS);
      	username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
      	username.addStyleName("fancy");
      	username.setWidth(INPUT_WIDTH, Unit.PIXELS);
		
      	

      	username.addValueChangeListener(event -> {
				if (event.getValue().length() > 0) {
					setButtonEnableState(true);
				} else {
					setButtonEnableState(false);
				}
		});
		
		
		addComponent(username);
		setComponentAlignment(username, Alignment.MIDDLE_CENTER);
		return username;
	}
	
	private PasswordField addPasswordField(VerticalLayout parent) {

		final PasswordField password = new PasswordField();
		password.setId("password_txt");
		//password.setInputPrompt("password");
		password.addStyleName(ValoTheme.TEXTFIELD_BORDERLESS);

		password.addValueChangeListener(event -> {
				if (event.getValue().length() > 0) {
					setButtonEnableState(true);
				} else {
					setButtonEnableState(false);
				}
		});

		HorizontalLayout passwordField = new HorizontalLayout();
		passwordField.addStyleName("fancy");
		passwordField.setWidth(INPUT_WIDTH, Unit.PIXELS);
		Image image = new Image(null, new ThemeResource("icons/login/Password-25.png"));
	
		passwordField.addComponent(image);
		passwordField.addComponent(password);
		
		passwordField.setComponentAlignment(image, Alignment.MIDDLE_LEFT);
		passwordField.setComponentAlignment(password, Alignment.MIDDLE_LEFT);
		passwordField.setExpandRatio(password, 1);
		parent.addComponent(passwordField);
		parent.setComponentAlignment(passwordField, Alignment.MIDDLE_CENTER);

		return password;
	}

	private TextField addUsernameTextfield(VerticalLayout parent) {
		final TextField username = new TextField();
		username.setId("username_txt");
		
      	username.addStyleName(ValoTheme.TEXTFIELD_BORDERLESS);

      	username.addValueChangeListener(event -> {
				if (event.getValue().length() > 0) {
					setButtonEnableState(true);
				} else {
					setButtonEnableState(false);
				}
		});

		HorizontalLayout userField = new HorizontalLayout();
		userField.addStyleName("fancy");
		userField.setWidth(INPUT_WIDTH, Unit.PIXELS);
		Image image = new Image(null, new ThemeResource("icons/login/Gender Neutral User-26.png"));
		userField.addComponent(image);
		userField.addComponent(username);
		
		userField.setComponentAlignment(image, Alignment.MIDDLE_LEFT);
		userField.setComponentAlignment(username, Alignment.MIDDLE_LEFT);
		userField.setExpandRatio(username, 1);
		
		parent.addComponent(userField);
		parent.setComponentAlignment(userField, Alignment.MIDDLE_CENTER);
		return username;
	}

	private Button addRegisterButton(final TextField username,
			final PasswordField password) {
		final Button btn_Register = new Button("register");
		btn_Register.addClickListener(e -> {
				eventbus.post(new UserRegisterRequestedEvent(username.getValue(), password.getValue()));
			}
		);

		btn_Register.addStyleName(ValoTheme.BUTTON_QUIET);
		btn_Register.addStyleName(ValoTheme.BUTTON_LINK);
		btn_Register.setClickShortcut(KeyCode.R, ModifierKey.ALT,
				ModifierKey.SHIFT);
		btn_Register.setEnabled(false);
		return btn_Register;
	}

	private Button addOKButton(final TextField username,
			final PasswordField password) {
		Button btn_Login = new Button("login");
		btn_Login.addClickListener(e -> {
				eventbus.post(new UserLoginRequestedEvent(username.getValue(), password.getValue()));
			}
		);

		btn_Login.setClickShortcut(KeyCode.ENTER);
		btn_Login.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btn_Login.addStyleName(ValoTheme.BUTTON_LINK);
		btn_Login.setEnabled(false);
		return btn_Login;
	}

	private void setButtonEnableState(boolean state) {
		btnLogin.setEnabled(state);
		btnRegister.setEnabled(state);
	}
}
