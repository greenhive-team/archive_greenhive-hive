package at.greenhive.webapp.view;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class MainView extends HorizontalLayout {
	private static final long serialVersionUID = 1L;

	public MainView() {
        setSizeFull();
        addStyleName("mainview");

        addComponent(new Label("Hello"));

//        ComponentContainer content = new CssLayout();
//        content.addStyleName("view-content");
//        content.setSizeFull();
//        addComponent(content);
//        setExpandRatio(content, 1.0f);
//
//        new DashboardNavigator(content);
    }
}