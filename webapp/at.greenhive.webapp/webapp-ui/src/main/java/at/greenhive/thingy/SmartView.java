package at.greenhive.thingy;

import com.vaadin.ui.Panel;
import com.vaadin.ui.themes.ValoTheme;

import at.greenhive.bl.Thingy;

public class SmartView extends Panel {
	
	private static final long serialVersionUID = 1L;

	public SmartView(Thingy thingy) {
		addStyleName(ValoTheme.PANEL_BORDERLESS);
		addStyleName("smartview");
		setCaption("SmartView for " + thingy.getDisplayName(getLocale()));
	}
}
