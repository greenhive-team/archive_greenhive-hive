package at.greenhive.webapp.messaging;

import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class MessageRouterServer {
	
	public void startRouterServer(ApplicationContext applicationContext) {
		
		ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");
		taskExecutor.setThreadGroupName("RouterServer");
		taskExecutor.setThreadNamePrefix("RouterServer");
		MessageRouterTask task = (MessageRouterTask) applicationContext.getBean("messageRouterTask");
		//task.setName("MessageSubscriberTask 1");
	    taskExecutor.execute(task);
	
	}
}
