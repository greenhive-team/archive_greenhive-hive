package at.greenhive.webapp.event;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.event.IEvent;

public class ThingyActivateRequestedEvent implements IEvent {

	private Thingy thingy;

	public ThingyActivateRequestedEvent(Thingy thingy) {
		this.thingy = thingy;
	}

	public Thingy getThingy() {
		return thingy;
	}
}
