package at.greenhive.thingy;

import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Layout;

import at.greenhive.bl.ScreenLayout;
import at.greenhive.bl.Thingy;
import at.greenhive.webapp.event.GreenhiveEventBus;

public abstract class  ThingyViewCreator<T extends Thingy> implements IThingyViewCreator {

	protected T thingy;
	protected GreenhiveEventBus eventBus;

	
	public ThingyViewCreator(T thingy, GreenhiveEventBus eventBus) {
		this.thingy = thingy;
		this.eventBus = eventBus;
	}
	
	@Override
	public Component createView() {
		return createLayout(thingy.getLayout());
	}

	public Layout createLayout(ScreenLayout layoutType) {

		switch (layoutType) {
		case FULLSCREEN:
			return createFullScreenLayout();
		case COLUMNS_2:
			return createCol2Layout(false);
		case COLUMNS_2_wide:
			return createCol2Layout(true);
		case COLUMNS_3:
			return createCol3Layout();
		}
		throw new IllegalArgumentException("Screen Layout " + layoutType.name() + "is not supported");

	}

	private Layout createFullScreenLayout() {
		CssLayout l = new CssLayout();
		l.setSizeFull();
		l.addComponent(getMiddleView());
		return l;
	}
	
	private Layout createCol2Layout(boolean wide) {
		GridLayout l = new GridLayout(2, 1);
		l.setSizeFull();
		
		Component leftView = getLeftView();
		leftView.setSizeFull();
		l.addComponent(leftView, 0, 0);

		Component rightView = getRightView();
		rightView.setSizeFull();
		l.addComponent(rightView, 1, 0);
		
		if (wide) {
			l.setColumnExpandRatio(0, 4);
			l.setColumnExpandRatio(1, 1);
		}
		return l;
	}

	
	private Layout createCol3Layout() {
		GridLayout l = new GridLayout(3, 1);
		l.setSizeFull();
		
		Component leftView = getLeftView();
		leftView.setSizeFull();
		l.addComponent(leftView, 0, 0);

		Component middleView = getMiddleView();
		middleView.setSizeFull();
		l.addComponent(middleView, 1, 0);
		
		Component rightView = getRightView();
		rightView.setSizeFull();
		l.addComponent(rightView, 2, 0);
		
		return l;
	}

}
