package at.greenhive.webapp;

public class GreenhiveStyles {
	public static final String STYLE_MAINHEADER = "mainheader";
	public static final String STYLE_MAINHEADER_CAPTION = "mainheadercaption";
	public static final String STYLE_MAINHEADER_CAPTIONMODE = "mainheadercaptionmode";
	public static final String STYLE_MAINHEADER_SPACER = "mainheaderspacer";
	
	public static final String STYLE_MENUITEM = "menuitem";
	public static final String STYLE_MENUACTIVE= "menuactive";
	
	public static final String STYLE_PANEL_LIGHT = "light";
	public static final String STYLE_BUTTON_SMALL = "small";
	
	public static final String STYLE_TOOLBAR = "toolbar";
	public static final String STYLE_DEFAULTBUTTON = "primary";
}
