package at.greenhive.thingy.cropland;

import com.vaadin.ui.Component;

import at.greenhive.thingy.SmartView;
import at.greenhive.thingy.ThingyViewCreator;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class CroplandThingyViewCreator extends ThingyViewCreator<CroplandThingy>{

	public CroplandThingyViewCreator(CroplandThingy thingy, GreenhiveEventBus eventBus) {
		super(thingy,eventBus);
	}

	@Override
	public Component getRightView() {
		return new SmartView(thingy);
	}

	@Override
	public Component getMiddleView() {
		return new CroplandView(eventBus);
	}

	@Override
	public Component getLeftView() {
		return null;
	}
}