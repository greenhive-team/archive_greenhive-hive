package at.greenhive.thingy.swarm;

public class RegisterDrone4MapEvent {

	private boolean register;
	private Drone bee;

	public RegisterDrone4MapEvent(boolean value, Drone bee) {
		this.bee = bee;
		this.register = value;
	}
	
	public Drone getBee() {
		return bee;
	}
	
	public boolean isRegister() {
		return register;
	}

}
