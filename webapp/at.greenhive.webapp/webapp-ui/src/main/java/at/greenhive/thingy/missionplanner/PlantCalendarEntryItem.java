package at.greenhive.thingy.missionplanner;

import java.time.ZonedDateTime;

import org.vaadin.addon.calendar.item.BasicItem;

import com.vaadin.icons.VaadinIcons;

import at.greenhive.thingy.missionplaner.PlantCalendarEntry;

public class PlantCalendarEntryItem extends BasicItem {
	private static final long serialVersionUID = 1L;

	private final PlantCalendarEntry entry;

	/**
	 * constructor
	 *
	 * @param meeting A meeting
	 */

	public PlantCalendarEntryItem(PlantCalendarEntry entry) {
		super(entry.getDetails(), null, entry.getStart(), entry.getEnd());
		this.entry = entry;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof PlantCalendarEntryItem)) {
			return false;
		}
		PlantCalendarEntryItem that = (PlantCalendarEntryItem) o;
		return getEntry().equals(that.getEntry());
	}

	public PlantCalendarEntry getEntry() {
		return entry;
	}

	@Override
	public String getStyleName() {
		return "state-" + entry.getState().name().toLowerCase();
	}

	@Override
	public int hashCode() {
		return getEntry().hashCode();
	}

	@Override
	public boolean isAllDay() {
		return entry.isLongTimeEvent();
	}

	@Override
	public boolean isMoveable() {
		return entry.isEditable();
	}

	@Override
	public boolean isResizeable() {
		return entry.isEditable();
	}

//    @Override
//    public boolean isClickable() {
//        return meeting.isEditable();
//    }

	@Override
	public void setEnd(ZonedDateTime end) {
		entry.setEnd(end);
		super.setEnd(end);
	}

	@Override
	public void setStart(ZonedDateTime start) {
		entry.setStart(start);
		super.setStart(start);
	}

	@Override
	public String getDateCaptionFormat() {
		// return CalendarItem.RANGE_TIME;
		return VaadinIcons.CLOCK.getHtml() + " %s<br>" + VaadinIcons.ARROW_CIRCLE_RIGHT_O.getHtml() + " %s";
	}
}
