package at.greenhive.webapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import at.greenhive.bl.hive.HiveService;
import at.greenhive.webapp.messaging.MessageRouterServer;
import at.greenhive.webapp.messaging.MessageSubscriberServer;

@Component
public class ContextStartHandler implements ApplicationListener<ContextStartedEvent> {
    @Autowired ThreadPoolTaskExecutor executor;

    @Override
    public void onApplicationEvent(ContextStartedEvent event) {
    	ApplicationContext context = event.getApplicationContext();
    	
    	startHiveService(context);
    	startMessaging(context);
    	
    }
    
    private void startHiveService(ApplicationContext context) {
    	HiveService.initInstance("TESTID");
		
	}

	private void startMessaging(ApplicationContext context) {
    	MessageSubscriberServer server  = new MessageSubscriberServer();
    	server.startSubscriberServer(context);
    	
    	MessageRouterServer server2  = new MessageRouterServer();
    	server2.startRouterServer(context);
    }

}
