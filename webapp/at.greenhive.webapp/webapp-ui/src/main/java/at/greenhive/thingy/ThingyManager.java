package at.greenhive.thingy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.greenhive.bl.Thingy;
import at.greenhive.thingy.cropland.CroplandThingy;
import at.greenhive.thingy.cropland.CroplandThingyViewCreator;
import at.greenhive.thingy.hivecontrol.HiveControllerThingy;
import at.greenhive.thingy.hivecontrol.HiveControllerThingyViewCreator;
import at.greenhive.thingy.missionplaner.MissionPlanerThingy;
import at.greenhive.thingy.missionplanner.MissionPlanerThingyViewCreator;
import at.greenhive.thingy.operation.OperationThingy;
import at.greenhive.thingy.operation.OperationThingyViewCreator;
import at.greenhive.thingy.settings.SettingsThingy;
import at.greenhive.thingy.settings.SettingsThingyViewCreator;
import at.greenhive.thingy.swarm.SwarmThingy;
import at.greenhive.thingy.swarm.SwarmThingyViewCreator;
import at.greenhive.webapp.event.GreenhiveEventBus;


public class ThingyManager {

	List<ThingyRegistry> registerdThingies;
	private GreenhiveEventBus eventbus; 
	
	public ThingyManager(GreenhiveEventBus eventbus) {
		this.eventbus = eventbus;
		this.registerdThingies = new ArrayList<ThingyRegistry>();
		registerThingies();
	}
	
	private void registerThingies() {
		registerdThingies.add(new ThingyRegistry(eventbus, new  CroplandThingy(), CroplandThingyViewCreator.class));
		registerdThingies.add(new ThingyRegistry(eventbus, new  MissionPlanerThingy(), MissionPlanerThingyViewCreator.class));
		registerdThingies.add(new ThingyRegistry(eventbus, new  OperationThingy(), OperationThingyViewCreator.class));
		registerdThingies.add(new ThingyRegistry(eventbus, new  SwarmThingy(), SwarmThingyViewCreator.class));
		registerdThingies.add(new ThingyRegistry(eventbus, new  HiveControllerThingy(), HiveControllerThingyViewCreator.class));
		registerdThingies.add(new ThingyRegistry(eventbus, new  SettingsThingy(), SettingsThingyViewCreator.class));
	}

	public List<ThingyRegistry> getThingyList() {
		return Collections.unmodifiableList(registerdThingies);
	}

	public IThingyViewCreator getViewCreator(Thingy thingy) {
		for (ThingyRegistry r: registerdThingies) {
			if (r.getThingy() == thingy) {
				return r.getViewCreator(thingy);
			}
		}
		return null;
	}
}
