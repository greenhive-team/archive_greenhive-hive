package at.greenhive.webapp.rest;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.greenhive.thingy.missionplaner.mission.MissionData;
import at.greenhive.thingy.operation.OperationService;

/**
 *
 * @author chris
 */
@RestController
@RequestMapping(path = "/missionplanner")
public class MissionPlannerRestController {

	@Autowired
	OperationService service;
	
    @RequestMapping(method = GET)
    public List<Object> list() {

        return null;
    }

    @RequestMapping(value = "/{id}", method = GET)
    public MissionData get(@PathVariable String id) {
    	return null;
    }

    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity<?> put(@PathVariable String id, @RequestBody Object input) {
        return null;
    }

    @RequestMapping(value = "/{id}", method = POST)
    public ResponseEntity<?> post(@PathVariable String id, @RequestBody Object input) {
        return null;
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<Object> delete(@PathVariable String id) {
        return null;
    }

}
