package at.greenhive.thingy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import at.greenhive.bl.Thingy;
import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.ApplicationErrorEvent;

public class ThingyRegistry {

	final Thingy thingy;
	final Class<? extends IThingyViewCreator> viewCreatorClass;
	private GreenhiveEventBus eventbus;

	public ThingyRegistry(GreenhiveEventBus eventbus, Thingy thingy, Class<? extends IThingyViewCreator> viewCreatorClass) {
		super();
		this.eventbus = eventbus;
		this.thingy = thingy;
		this.viewCreatorClass = viewCreatorClass;
	}

	public Thingy getThingy() {
		return thingy;
	}

	public IThingyViewCreator getViewCreator(Thingy thingy) {
		if (viewCreatorClass != null) {

			try {
				
				Constructor<? extends IThingyViewCreator> c = viewCreatorClass.getConstructor(thingy.getClass(), GreenhiveEventBus.class);
				return c.newInstance(thingy,eventbus);
				
//				Constructor<? extends IThingyViewCreator> c = viewCreatorClass.getConstructor(thingy.getClass());
//				return c.newInstance(thingy);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				eventbus.post(new ApplicationErrorEvent(e));
			}

		}
		return null;
	}

}
