package at.greenhive.webapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import at.greenhive.bl.hive.HiveService;

@Component
public class ContextCloseHandler implements ApplicationListener<ContextClosedEvent> {
	@Autowired
	ThreadPoolTaskExecutor executor;

	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		executor.shutdown();
		HiveService.shutdown();
	}
}
