package at.greenhive.webapp.rest;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.thingy.cropland.Cropland;
import at.greenhive.thingy.cropland.CroplandDeleteInfo;
import at.greenhive.thingy.cropland.CroplandMetadata;
import at.greenhive.thingy.cropland.CroplandRow;
import at.greenhive.thingy.cropland.CroplandService;

/**
 *
 * @author chris
 */
@RestController
@RequestMapping(path = "/cropland")
public class CropLandRestController {

	@Autowired
	CroplandService cropLandService;
	
    @RequestMapping(method = GET)
    public List<CroplandMetadata> list() {
        return cropLandService.getAll();
    }
    
    @RequestMapping(value = "/detailed", method = GET)
    public List<Cropland> detailedList() {
        return cropLandService.getDetailedCroplandList();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public Cropland get(@PathVariable String id) {
    	int clid = Integer.parseInt(id);
    	return cropLandService.getCroplandDetail(clid);
    }
    
    @RequestMapping(value = "/{id}/outline", method = GET)
    public List<GeoPoint> getOutline(@PathVariable String id) {
    	int clid = Integer.parseInt(id);
    	return cropLandService.getCroplandOutline(clid);
    }
    
    @RequestMapping(value = "/{id}/rows", method = GET)
    public List<CroplandRow> getRows(@PathVariable String id) {
    	int clid = Integer.parseInt(id);
    	return cropLandService.getCroplandRows(clid);
    }

    @RequestMapping(value = "/update/meta", method = PUT)
    public ResponseEntity<?> put( @RequestBody  List<CroplandMetadata> input) {
    	cropLandService.insertCroplands(input);
        return ResponseEntity.ok("ok");
    }

    @RequestMapping(value = "/insert/meta", method = POST)
    public ResponseEntity<?> post(@RequestBody  List<CroplandMetadata> input) {
        cropLandService.updateCroplands(input);
        return ResponseEntity.ok("ok");
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<Object> delete(@PathVariable String id) {
    	int clid = Integer.parseInt(id);
    	CroplandDeleteInfo rs = cropLandService.deleteCropland(clid);
    	return ResponseEntity.ok(rs);
    }

}
