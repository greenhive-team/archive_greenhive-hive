package at.greenhive.thingy.missionplanner;

import com.vaadin.ui.Component;

import at.greenhive.bl.hive.HiveService;
import at.greenhive.thingy.SmartView;
import at.greenhive.thingy.ThingyViewCreator;
import at.greenhive.thingy.missionplaner.MissionPlanerThingy;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class  MissionPlanerThingyViewCreator extends ThingyViewCreator< MissionPlanerThingy>{

	public MissionPlanerThingyViewCreator( MissionPlanerThingy thingy, GreenhiveEventBus eventBus) {
		super(thingy, eventBus);
	}

	@Override
	public Component getRightView() {
		return new SmartView(thingy);
	}

	@Override
	public Component getMiddleView() {
		return new MissionPlanerView(eventBus , () -> HiveService.getOperationManager() );
	}

	@Override
	public Component getLeftView() {
		return null;
	}
}