package at.greenhive.thingy.operation;

import com.vaadin.ui.Component;

import at.greenhive.bl.hive.HiveService;
import at.greenhive.thingy.SmartView;
import at.greenhive.thingy.ThingyViewCreator;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class  OperationThingyViewCreator extends ThingyViewCreator< OperationThingy>{

	public OperationThingyViewCreator( OperationThingy thingy, GreenhiveEventBus eventBus) {
		super(thingy, eventBus);
	}

	@Override
	public Component getRightView() {
		return new SmartView(thingy);
	}

	@Override
	public Component getMiddleView() {
		return new OperationView(eventBus , () -> HiveService.getOperationManager() );
	}

	@Override
	public Component getLeftView() {
		return null;
	}
}