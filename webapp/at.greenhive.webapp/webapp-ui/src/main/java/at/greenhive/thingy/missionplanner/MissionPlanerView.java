package at.greenhive.thingy.missionplanner;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.Month;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addon.calendar.Calendar;
import org.vaadin.addon.calendar.handler.BasicDateClickHandler;
import org.vaadin.addon.calendar.item.BasicItemProvider;
import org.vaadin.addon.calendar.ui.CalendarComponentEvents;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalSplitPanel;

import at.greenhive.thingy.cropland.CroplandMetadata;
import at.greenhive.thingy.cropland.CroplandService;
import at.greenhive.thingy.missionplaner.PlantCalendarEntry;
import at.greenhive.thingy.operation.OperationManagerProvider;
import at.greenhive.thingy.operation.OperationService;
import at.greenhive.thingy.swarm.SwarmService;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class MissionPlanerView extends Panel {

	private static final long serialVersionUID = 1L;

	@Autowired
	OperationService missionService;

	@Autowired
	SwarmService swarmService;

	@Autowired
	CroplandService croplandservice;

	private final GreenhiveEventBus eventBus;
	
	private final class PlantEventDataProvider extends BasicItemProvider<PlantCalendarEntryItem> {
		private static final long serialVersionUID = 1L;

		void removeAllEvents() {
            this.itemList.clear();
            fireItemSetChanged();
        }
    }

	private PlantEventDataProvider eventProvider;
	
	// private List<MissionTask> currentsTasks;

	public MissionPlanerView(GreenhiveEventBus eventBus, OperationManagerProvider operationManagerProvider) {

		this.eventBus = eventBus;

		addAttachListener(e -> init());
		addDetachListener(e -> deinit());

	}

	void init() {
		eventBus.register(this);
		eventProvider = new PlantEventDataProvider();

		HorizontalSplitPanel panel = new HorizontalSplitPanel();

		panel.setSplitPosition(33);

		panel.setFirstComponent(createCroplandGrid());
		panel.setSecondComponent(createRightPanel());

		setContent(panel);
		setSizeFull();

	}

	private Component createCroplandGrid() {

		Grid<CroplandMetadata> croplandGrid = new Grid<CroplandMetadata>("Ackerflächen");

		croplandGrid.addColumn(c -> c.getClid()).setCaption("ID");
		croplandGrid.addColumn(c -> c.getName()).setCaption("Name");
		croplandGrid.addColumn(c -> c.getCroptype().toString()).setCaption("Type");

		croplandGrid.setItems(croplandservice.getAll());
		croplandGrid.setSizeFull();

		// croplandGrid.addSelectionListener(e ->
		// handleCroplandSelection(e.getFirstSelectedItem(), e.getAllSelectedItems()));

		return croplandGrid;
	}

	void deinit() {
		eventBus.unregister(this);
	}

	private Component createLeftPanel() {

		VerticalSplitPanel leftPanel = new VerticalSplitPanel();

		return leftPanel;
	}

	private Component createRightPanel() {
		Calendar<PlantCalendarEntryItem> c = new Calendar<>("Saison Kalender",eventProvider);
		c.setSizeFull();
		c.setResponsive(true);
        c.setItemCaptionAsHtml(true);
        c.setContentMode(ContentMode.HTML);
		c.withMonth(Month.SEPTEMBER);
		addCalendarEventListeners(c);
		return c;
	}

	private void addCalendarEventListeners(Calendar<?> calendar) {
		calendar.setHandler(new BasicDateClickHandler(true));
		calendar.setHandler(this::onCalendarClick);
		calendar.setHandler(this::onCalendarRangeSelect);
	}

	private void onCalendarClick(CalendarComponentEvents.ItemClickEvent event) {

		PlantCalendarEntryItem item = (PlantCalendarEntryItem) event.getCalendarItem();

		final PlantCalendarEntry entry = item.getEntry();

		Notification.show(entry.getName(), entry.getDetails(), Type.HUMANIZED_MESSAGE);
	}

	private void onCalendarRangeSelect(CalendarComponentEvents.RangeSelectEvent event) {

		PlantCalendarEntry entry = new PlantCalendarEntry(
				!event.getStart().truncatedTo(DAYS).equals(event.getEnd().truncatedTo(DAYS)));

		entry.setStart(event.getStart());
		entry.setEnd(event.getEnd());

		entry.setName("Task");
		entry.setDetails("Here we need to seed, spray or harvest something");

		// Random state
		entry.setState(PlantCalendarEntry.State.planned);

		eventProvider.addItem(new PlantCalendarEntryItem(entry));
	}

	private Component createButtonBar() {
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setSizeUndefined();
		// buttonBar.setWidth(100, Unit.PERCENTAGE);

		// Just for testing purposes, will we replaced by MissionManager / Controller
		// buttonBar.addComponent(new Button("Send take off checks", e->
		// operationManager.sendTakeOffChecks()));
		// buttonBar.addComponent(new Button("Send take off", e->
		// operationManager.sendTakeOff()));
		// buttonBar.addComponent(new Button("Send to Drone", e->
		// operationManager.sendCurrentMission()));

		return buttonBar;
	}

}
