package at.greenhive.thingy;

import com.vaadin.ui.Component;

public interface IThingyViewCreator {

	Component createView();

	Component getRightView();

	Component getMiddleView();

	Component getLeftView();

}
