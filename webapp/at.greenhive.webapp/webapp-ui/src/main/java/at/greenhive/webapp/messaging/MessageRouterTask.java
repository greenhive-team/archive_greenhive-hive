package at.greenhive.webapp.messaging;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMsg;

import at.greenhive.bl.airtrafficcontroller.SendDroneMessageEvent;
import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.protobuf.Message.Container;
import me.limespace.appbase.event.ApplicationErrorEvent;

@Component
@Scope("prototype")
public class MessageRouterTask extends MessageTask implements Runnable {

	private static final int ROUTER_SERVER_WORKER = 2;
	
	private static Logger log = Logger.getLogger(MessageRouterTask.class.getName());

	@Autowired 
	private ApplicationContext applicationContext;
	
	private static class server_worker implements Runnable {
		private ZContext ctx;

		public server_worker(ZContext ctx) {
			this.ctx = ctx;
		}

		public void run() {
			Socket worker = ctx.createSocket(SocketType.DEALER);
			worker.connect("inproc://backend");

			while (!Thread.currentThread().isInterrupted()) {
				// The DEALER socket gives us the address envelope and message
				ZMsg msg = ZMsg.recvMsg(worker);
				ZFrame address = msg.pop();
				ZFrame content = msg.pop();

				msg.destroy();

				Container container;
				try {
					container = Container.parseFrom(content.getData());
					String droneId = new String(address.getData());
					Container result = handleMessage(droneId, container);
					
					if (result != null) {
						address.send(worker, ZFrame.REUSE + ZFrame.MORE);
						ZFrame resultFrame = new ZFrame(result.toByteArray());
						resultFrame.send(worker, ZFrame.REUSE);
						resultFrame.destroy();
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					GlobalEventManager.fire(new ApplicationErrorEvent(e), null);
				}

				address.destroy();
				content.destroy();
			}
			ctx.destroy();
		}
	}
	
	public void run() {
		
		ZContext ctx = new ZContext();

		// Frontend socket talks to clients over TCP
		Socket frontend = ctx.createSocket(SocketType.ROUTER);
		frontend.bind("tcp://*:5570");

		log.info("Message Router bound to Port 5570." );
		
		GlobalEventManager.registerListener((e,s) -> {
			if (e instanceof SendDroneMessageEvent) {
				
				SendDroneMessageEvent sm = (SendDroneMessageEvent)e;
				
				log.info("Sending: " + sm.toString());
				
				ZFrame address = new ZFrame(sm.getId());
				address.send(frontend, ZFrame.REUSE + ZFrame.MORE);
				
				ZFrame data = new ZFrame("BLA");
				data.send(frontend, ZFrame.REUSE+ ZFrame.MORE );
				
				ZFrame data1 = new ZFrame(sm.getMessage().toByteArray());
				data1.send(frontend, ZFrame.REUSE);
				
				address.destroy();
				data.destroy();
				data1.destroy();
			} 
		});
		
		// Backend socket talks to workers over inproc
		Socket backend = ctx.createSocket(SocketType.DEALER);
		backend.bind("inproc://backend");

		// Launch pool of worker threads
		ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");
		
		for (int threadNbr = 0; threadNbr < ROUTER_SERVER_WORKER; threadNbr++) {
			taskExecutor.execute(new server_worker(ctx));
		}

		// Connect backend to frontend via a proxy
		ZMQ.proxy(frontend, backend, null);
		
		ctx.destroy();
	}
}