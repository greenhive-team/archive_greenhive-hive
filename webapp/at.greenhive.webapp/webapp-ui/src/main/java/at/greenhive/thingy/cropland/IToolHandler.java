package at.greenhive.thingy.cropland;

import org.vaadin.addon.vol3.OLMap.OLClickEvent;

import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;

import at.greenhive.bl.util.geo.GPSPoint;

public interface IToolHandler {

	void handleMapClick(LatLon position);
	void handleMarkerDrag(GoogleMapMarker marker, LatLon position);
	
	default String asString(LatLon position) {
		return position.getLat() +","+ position.getLon();
	}
	void handleMapClick(OLClickEvent event);
	
	void setPoint(GPSPoint gpsPoint);

}
