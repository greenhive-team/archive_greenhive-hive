package at.greenhive.thingy.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.GoogleMap.MapType;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapPolygon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapPolyline;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.util.HSLColor;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.thingy.cropland.Cropland;
import at.greenhive.thingy.cropland.CroplandPath;
import at.greenhive.thingy.cropland.CroplandRow;
import at.greenhive.thingy.cropland.CroplandService;
import at.greenhive.thingy.missionplaner.HiveCoordinateConverter;
import at.greenhive.thingy.missionplaner.WorkCoordinate;
import at.greenhive.thingy.missionplaner.WorkPath;
import at.greenhive.thingy.missionplaner.event.MissionSelectionEvent;
import at.greenhive.thingy.missionplaner.mission.MissionData;
import at.greenhive.thingy.missionplaner.mission.MissionTask;
import at.greenhive.thingy.missionplaner.mission.OperationData;
import at.greenhive.thingy.missionplaner.mission.OperationMetaData;
import at.greenhive.thingy.missionplaner.mission.enTaskState;
import at.greenhive.thingy.swarm.Drone;
import at.greenhive.thingy.swarm.Swarm;
import at.greenhive.thingy.swarm.SwarmService;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.thingy.swarm.event.SwarmSelectionEvent;
import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class OperationView extends Panel implements IEventListener {

	private static final long serialVersionUID = 1L;
	private static final String TRANSIT_WAYPOINT_ICON = "VAADIN/themes/greenhive/icons/map/TransitWaypoint.png";

	@Autowired
	OperationService operationService;

	@Autowired
	SwarmService swarmService;

	@Autowired
	CroplandService croplandservice;

	private final GreenhiveEventBus eventBus;
	private final OperationManagerProvider operationManager;

	private GoogleMap googleMap;
	Collection<GoogleMapPolygon> activePolygon = new ArrayList<>();
	Collection<GoogleMapPolyline> activePolyline = new ArrayList<>();
	Collection<GoogleMapPolyline> activeBeeTrail = new ArrayList<>();
	Collection<GoogleMapPolyline> activeSegments = new ArrayList<>();

	Map<String, List<LatLon>> beeTrail = new HashMap<>();
	// private Grid<MissionTask> taskGrid;
	private TreeGrid<MissionTaskItem> taskGrid;

	private OperationMetaData selectedOperation;

	private GPSPoint hiveLocation;

	private Grid<OperationState> operationStateGrid;

	private Button btnStart;
	private Button btnPause;
	private Button btnStop;

	private Binder<OperationInfo> operationInfoBinder;

	private long lastTime = 0;

	// private List<MissionTask> currentsTasks;

	public OperationView(GreenhiveEventBus eventBus, OperationManagerProvider operationManagerProvider) {

		this.eventBus = eventBus;
		this.operationManager = operationManagerProvider;

		addAttachListener(e -> init());
		addDetachListener(e -> deinit());

	}

	void init() {
		eventBus.register(this);
		GlobalEventManager.registerListener(this);

		HorizontalSplitPanel panel = new HorizontalSplitPanel();

		panel.setSplitPosition(33);
		panel.setFirstComponent(createLeftPanel());
		panel.setSecondComponent(createRightPanel());

		setContent(panel);
		setSizeFull();

		updateSegments();
		updateControls();
		updateOperationStateGrid();

	}

	void deinit() {
		GlobalEventManager.unregisterListener(this);
		eventBus.unregister(this);
	}

	private Component createLeftPanel() {
		TabSheet sheet = new TabSheet();
		sheet.setSizeFull();

		sheet.addTab(createLeftPanelFirstTab(), "Settings", VaadinIcons.TOOLS);
		sheet.addTab(createTaskPanel(), "Tasks", VaadinIcons.TASKS);
		sheet.addTab(createOperationPanel(), "Control", VaadinIcons.PLAY);
		return sheet;
	}

	private Component createLeftPanelFirstTab() {

		VerticalSplitPanel leftPanel = new VerticalSplitPanel();
		leftPanel.setSizeFull();
		leftPanel.setFirstComponent(createMissionGrid());
		leftPanel.setSecondComponent(createSwarmGrid());

		return leftPanel;
	}

	private Component createOperationPanel() {
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();

		HorizontalLayout infoButtonBar = new HorizontalLayout();
		infoButtonBar.setWidth(100, Unit.PERCENTAGE);
		infoButtonBar.addComponent(createOperationInfoPanel());
		infoButtonBar.addComponent(createOperationInfoButtons());

		mainLayout.addComponent(infoButtonBar);
		// mainLayout.addComponent(new Label("<hr/>", ContentMode.HTML));

		operationStateGrid = createOperationStatusGrid();
		mainLayout.addComponent(operationStateGrid);
		mainLayout.setExpandRatio(operationStateGrid, 1);

		return mainLayout;
	}

	private Grid<OperationState> createOperationStatusGrid() {

		Grid<OperationState> grid = new Grid<>();
		grid.addColumn(OperationState::getState).setCaption("Status");
		grid.addColumn(OperationState::getTimestamp).setCaption("Timestamp");
		grid.setSizeFull();

		return grid;
	}

	private Component createOperationInfoButtons() {
		VerticalLayout btnLayout = new VerticalLayout();
		btnLayout.setMargin(false);
		btnStart = new Button("Start Operation", VaadinIcons.PLAY);
		btnStart.addClickListener((e) -> startOperation());

		btnPause = new Button("Pause", VaadinIcons.PAUSE);
		btnPause.addClickListener((e) -> pauseOperation());

		btnStop = new Button("Stop Operation", VaadinIcons.STOP);
		btnStop.addClickListener((e) -> stopOperation());

		Button btnReset = new Button("Reset Operation", VaadinIcons.DEL);
		btnReset.addClickListener((e) -> operationManager.get().resetOperationManager());

		btnLayout.addComponents(btnStart, btnPause, btnStop, btnReset);
		return btnLayout;
	}

	private Component createOperationInfoPanel() {

		FormLayout infoLayout = new FormLayout();
		infoLayout.setMargin(false);

		TextField missions = new TextField("Missionen");
		TextField tasks = new TextField("Tasks");
		TextField finishedTasks = new TextField("Tasks fertig");
		TextField flightTime = new TextField("Flugzeit");
		TextField area = new TextField("Fläche");
		TextField estSprayAoumt = new TextField("Verbrauch geplant");
		TextField sprayAoumt = new TextField("Verbrauch");

		infoLayout.addComponent(missions);
		infoLayout.addComponent(tasks);
		infoLayout.addComponent(finishedTasks);
		infoLayout.addComponent(flightTime);
		infoLayout.addComponent(area);
		infoLayout.addComponent(estSprayAoumt);
		infoLayout.addComponent(sprayAoumt);

		operationInfoBinder = new Binder<>();

		operationInfoBinder.bind(missions, b -> String.valueOf(b.getNumMissions()), null);
		operationInfoBinder.bind(tasks, b -> String.valueOf(b.getNumTasks()), null);
		operationInfoBinder.bind(finishedTasks, b -> String.valueOf(b.getNumFinishedTasks()), null);
		operationInfoBinder.bind(flightTime, b -> String.valueOf(b.getEstTime()), null);
		operationInfoBinder.bind(area, b -> String.valueOf(b.getSprayArea()), null);
		operationInfoBinder.bind(sprayAoumt, b -> String.valueOf(b.getSprayAmount()), null);
		operationInfoBinder.bind(estSprayAoumt, b -> String.valueOf(b.getEstSprayAmount()), null);

		operationInfoBinder.setReadOnly(true);

		return infoLayout;
	}

	private void startOperation() {
		try {
			operationManager.get().startOperation();
		} catch (OperationDataNotCompleteException e) {
			Notification.show("Operation not defined.", Type.ASSISTIVE_NOTIFICATION);
		}
	}

	private void pauseOperation() {
		operationManager.get().pauseOperation();
	}

	private void stopOperation() {
		operationManager.get().stopOperation();
	}

	private Component createSwarmGrid() {

		Grid<Swarm> swarmGrid = new Grid<Swarm>();

		swarmGrid.addColumn(s -> s.getName()).setCaption("Name");
		swarmGrid.addColumn(s -> makeList(s)).setCaption("Bumblebees");

		swarmGrid.setItems(swarmService.getAll());
		swarmGrid.setSizeFull();

		swarmGrid.addSelectionListener(e -> handleSwarmSelection(e.getFirstSelectedItem(), e.getAllSelectedItems()));

		return swarmGrid;
	}

	private String makeList(Swarm s) {
		StringBuilder sb = new StringBuilder();
		sb.append(s.getDrones().size()).append(":");
		s.getDrones().forEach(bee -> sb.append(bee.getName()).append(", "));
		return sb.substring(0, sb.length() - 2);
	}

	private Component createMissionGrid() {
		Grid<OperationMetaData> missionGrid = new Grid<>(OperationMetaData.class);

		missionGrid.setColumnOrder("name");

		Collection<OperationMetaData> missionList = operationService.getAll();

		missionGrid.setItems(missionList);
		missionGrid.setSizeFull();

		missionGrid
				.addSelectionListener(e -> handleMissionSelection(e.getFirstSelectedItem(), e.getAllSelectedItems()));
		return missionGrid;
	}

	private void handleSwarmSelection(Optional<Swarm> firstSelectedItem, Set<Swarm> allSelectedItems) {
		eventBus.post(new SwarmSelectionEvent(firstSelectedItem, allSelectedItems, this));
	}

	private void handleMissionSelection(Optional<OperationMetaData> firstSelectedItem,
			Set<OperationMetaData> allSelectedItems) {
		eventBus.post(new MissionSelectionEvent(firstSelectedItem, allSelectedItems, this));
	}

	@Subscribe
	void onMissionSelection(MissionSelectionEvent se) {
		se.getFirstSelectedItem().ifPresent(this::updateOperationData);
	}

	@Subscribe
	void onSwarmSelection(SwarmSelectionEvent se) {
		se.getFirstSelectedItem().ifPresent(this::updateSwarmData);
		updateSegments();
	}

	void updateOperationData(OperationMetaData operationData) {

		selectedOperation = operationData;
		updateOperationManager();

		googleMap.clearMarkers();
		activePolygon.forEach(p -> googleMap.removePolygonOverlay(p));
		activePolyline.forEach(p -> googleMap.removePolyline(p));
		activeBeeTrail.forEach(p -> googleMap.removePolyline(p));

		if (hiveLocation != null) {
			String url = "VAADIN/themes/greenhive/icons/greenhive-icon-64.png";
			GoogleMapMarker hive = new GoogleMapMarker("Hive",
					new LatLon(hiveLocation.getLatitudeDecimalDegrees(), hiveLocation.getLongitudeDecimalDegrees()),
					false, url);
			googleMap.addMarker(hive);
		}

		int croplandId = operationData.getCroplandId();
		Cropland cropland = croplandservice.getCroplandDetail(croplandId);
		// Get & Draw Outline
		List<GeoPoint> croplandOutline = cropland.getOutline();

		ArrayList<LatLon> points = new ArrayList<LatLon>(croplandOutline.size());
		boolean first = true;
		for (GeoPoint gp : croplandOutline) {
			LatLon latLon = new LatLon(gp.getLatitudeDecimalDegrees(), gp.getLongitudeDecimalDegrees());
			points.add(latLon);
			if (first) {
				first = false;
				googleMap.setCenter(latLon);
			}
		}

		GoogleMapPolygon overlay = new GoogleMapPolygon(points, "#5bc653", 0.5, "#194915", 0.5, 1);
		activePolygon.add(overlay);
		googleMap.addPolygonOverlay(overlay);

		// GoogleMapPolyline outline = new GoogleMapPolyline(points, "#5bc653", 0.5, 2);
		// googleMap.addPolyline(outline);

		// Get & Draw Transit
		List<CroplandPath> transitPath = cropland.getPaths();

		
		first = true;
		for (CroplandPath path:transitPath) {
			ArrayList<LatLon> transitPoints = new ArrayList<LatLon>();
			for (GeoPoint gp : path.getPath()) {
				LatLon latLon = new LatLon(gp.getLatitudeDecimalDegrees(), gp.getLongitudeDecimalDegrees());
				GoogleMapMarker marker = new GoogleMapMarker("",latLon,true, TRANSIT_WAYPOINT_ICON);
				marker.setAnimationEnabled(false);
				googleMap.addMarker(marker);
				transitPoints.add(latLon);
			}
			GoogleMapPolyline transitOverlay = new GoogleMapPolyline(transitPoints, "#ff2222", 0.7, 3);
			activePolyline.add(transitOverlay);
			googleMap.addPolyline(transitOverlay);
		}
		
		

		// Get & Draw Rows
		List<CroplandRow> croplandRows = croplandservice.getCroplandRows(croplandId);

		for (CroplandRow row : croplandRows) {
			List<GeoPoint> path = row.getPath();
			points = new ArrayList<LatLon>(path.size());
			for (GeoPoint gp : path) {
				LatLon latLon = new LatLon(gp.getLatitudeDecimalDegrees(), gp.getLongitudeDecimalDegrees());
				points.add(latLon);
			}
			GoogleMapPolyline rowline = new GoogleMapPolyline(points, "#5b1e25", 0.5, 2);
			googleMap.addPolyline(rowline);
			activePolyline.add(rowline);
		}

		googleMap.setZoom(100);
		updateSegments();
	}

	private void updateOperationInfo() {
		operationInfoBinder.setBean(operationManager.get().getOperationInfo());
	}

	private void updateOperationStateGrid() {
		Deque<OperationState> operationStates = operationManager.get().getOperationStates();
		if (!CollectionUtils.isEmpty(operationStates)) {
			operationStateGrid.setItems(operationStates);
		} else {
			operationStateGrid.setItems(Collections.emptyList());
		}
	}

	private void updateSwarmData(Swarm swarm) {
		operationManager.get().updateSwarm(swarm);
	}

	private void updateOperationManager() {
		if (selectedOperation != null) {
			Cropland cropland = croplandservice.getCroplandDetail(selectedOperation.getCroplandId());
			operationManager.get().addOperation(selectedOperation, cropland);
		}
	}

	private void updateSegments() {
		updateTaskList();
		updateSegmentsOnMap(null);
	}

	private void updateTaskList() {

		Collection<MissionData> missionDataList = operationManager.get().getMissionData();

		if (CollectionUtils.isEmpty(missionDataList)) {
			taskGrid.setItems(Collections.emptyList());
			return;
		}

		Collection<MissionTaskItem> rootItems = new ArrayList<>();

		for (MissionData md : missionDataList) {
			MissionTaskItem rootItem = new MissionTaskItem(md.getTasks().iterator().next().getTaskId(), md,
					operationManager);
			rootItems.add(rootItem);
			
			for (MissionTask task : md.getToStart()) {
				MissionTaskItem item = new MissionTaskItem(task.getTaskId(), md, operationManager);
				rootItem.add(item);
			}
			for (MissionTask task : md.getTasks()) {
				MissionTaskItem item = new MissionTaskItem(task.getTaskId(), md, operationManager);
				rootItem.add(item);
			}
			for (MissionTask task : md.getToHive()) {
				MissionTaskItem item = new MissionTaskItem(task.getTaskId(), md, operationManager);
				rootItem.add(item);
			}
		}
		taskGrid.setItems(rootItems, MissionTaskItem::getItems);
	}

	private void updateSegmentsOnMap(MissionTaskItem hightlight) {

		OperationData operationData = operationManager.get().getOperationData();
		HiveCoordinateConverter converter = operationManager.get().getConverter();
		if (operationData != null) {
			Collection<MissionTask> tasks = operationData.getTasks();

			activeSegments.forEach(p -> googleMap.removePolyline(p));
			activeSegments.clear();

			// if (1==1) return;
			if (hightlight != null && hightlight.getItems().size() > 3) {
				// return;
			}
			int ix = 0;
			for (MissionTask task : tasks) {
				GeoPoint startPoint = task.getStart(converter);
				GeoPoint endPoint = task.getEnd(converter);

				ArrayList<LatLon> points = new ArrayList<LatLon>(2);

				LatLon latLon = new LatLon(startPoint.getLatitudeDecimalDegrees(),
						startPoint.getLongitudeDecimalDegrees());
				points.add(latLon);

				latLon = new LatLon(endPoint.getLatitudeDecimalDegrees(), endPoint.getLongitudeDecimalDegrees());
				points.add(latLon);
				float hueDist = 360 * (ix / (1.0f * tasks.size()));
				float hue = (ix % 2 == 0) ? hueDist : 180 + hueDist;
				HSLColor color = new HSLColor(hue, 100, 50);
				GoogleMapPolyline segment = new GoogleMapPolyline(points, color.getRGB().getHexRGB(), 1, 2);
				if (isSelected(hightlight, task)) {
					segment.setStrokeWeight(4);
				} else {
					segment.setStrokeWeight(1);
				}
				// segment.
				// segment.setId( task.getTaskId());
				googleMap.addPolyline(segment);

				activeSegments.add(segment);
				ix++;
			}
			
			List<WorkPath> transitPaths = operationData.getTransitPaths();
			for (WorkPath path:transitPaths) {
				ArrayList<LatLon> points = new ArrayList<LatLon>();
				for (WorkCoordinate c:path.getPath()) {
					GeoPoint point = converter.toAbsolut(c);
					

					LatLon latLon = new LatLon(point.getLatitudeDecimalDegrees(),
											   point.getLongitudeDecimalDegrees());
					points.add(latLon);
					GoogleMapPolyline segment = new GoogleMapPolyline(points,"#ffff00", 0.5, 1);
					googleMap.addPolyline(segment);

					activeSegments.add(segment);
				}
			}
			
			
			
			googleMap.markAsDirtyRecursive();
		}
	}

	private boolean isSelected(MissionTaskItem hightlight, MissionTask task) {
		if (hightlight == null) {
			return false;
		}

		if (hightlight.getTaskId() == task.getTaskId()) {
			return true;
		}

		for (MissionTaskItem item : hightlight.getItems()) {
			if (item.getTaskId() == task.getTaskId()) {
				return true;
			}
		}
		return false;
	}

	private void updateDronesOnMap() {

		googleMap.clearMarkers();
		Swarm swarm = operationManager.get().getSwarm();
		if (swarm == null) {
			return;
		}
		String iconUrl = "VAADIN/themes/greenhive/icons/Bumblebee-25.png";
		LatLon center = null;

		activeBeeTrail.forEach(p -> googleMap.removePolyline(p));
		activeBeeTrail.clear();

		for (Drone bee : swarm.getDrones()) {
			GPSPoint position = bee.getState().getPosition();
			if (position != null) {
				LatLon latLon = new LatLon(position.getLatitudeDecimalDegrees(), position.getLongitudeDecimalDegrees());

				List<LatLon> trail = beeTrail.computeIfAbsent(bee.getSerialNumber(), id -> new ArrayList<>());

				trail.add(latLon);
				GoogleMapPolyline pTrail = new GoogleMapPolyline(trail, "#00FF00", 1.0, 1);
				activeBeeTrail.add(pTrail);
				GoogleMapMarker marker = new GoogleMapMarker(bee.getSerialNumber(), latLon, false, iconUrl);
				center = latLon;
				googleMap.addMarker(marker);
			}
		}
		if (center != null) {
			googleMap.setCenter(center);
		}
	}

	private Component createRightPanel() {
		TabSheet tab = new TabSheet();
		tab.setSizeFull();
		tab.addTab(createMapPanel(), "Map", VaadinIcons.MAP_MARKER);
		// tab.addTab(createTaskPanel(), "Tasks", VaadinIcons.TASKS);
		return tab;
	}

	private Component createTaskPanel() {
//		VerticalLayout panel = new VerticalLayout();
//		panel.setSizeFull();

		taskGrid = createTaskTreeGrid();
		taskGrid.setSizeFull();
//		panel.addComponent(taskGrid);
//		panel.setExpandRatio(taskGrid, 1);
//
//		Component buttonBar = createButtonBar();
//		panel.addComponent(buttonBar);
//		panel.setExpandRatio(buttonBar, 0);
//		panel.setComponentAlignment(buttonBar, Alignment.MIDDLE_RIGHT);

		return taskGrid;
	}

	private static class MissionTaskItem {
		private final int taskId;
		private final MissionData missionData;
		private Collection<MissionTaskItem> items;
		private OperationManagerProvider omp;

		public MissionTaskItem(int taskId, MissionData missionData, OperationManagerProvider omp) {
			this.taskId = taskId;
			this.missionData = missionData;
			this.omp = omp;
			this.items = new ArrayList<>();
		}

		public void add(MissionTaskItem item) {
			items.add(item);
		}

		public Collection<MissionTaskItem> getItems() {
			return items;
		}

		public int getTaskId() {
			return taskId;
		}

		public int getMissionId() {
			return missionData.getMissionId();
		}

		public String getDroneId() {
			return missionData.getTaskState(taskId).getDroneId();
		}

		public enTaskState getTaskState() {
			return missionData.getTaskState(taskId).getTaskState();
		}
		
		public String getTaskType() {
			return missionData.getTask(taskId).getType();
		}

		public GeoPoint getStart() {
			return missionData.getTask(taskId).getStart(omp.get().getConverter());
		}

		public GeoPoint getEnd() {
			return missionData.getTask(taskId).getEnd(omp.get().getConverter());
		}
	}

	private TreeGrid<MissionTaskItem> createTaskTreeGrid() {
		TreeGrid<MissionTaskItem> grid = new TreeGrid<>();

		grid.addColumn(MissionTaskItem::getMissionId).setCaption("Missionid");
		grid.addColumn(MissionTaskItem::getTaskId).setCaption("Task ID");
		grid.addColumn(MissionTaskItem::getDroneId).setCaption("Drone");
		grid.addColumn(MissionTaskItem::getTaskState).setCaption("State");
		grid.addColumn(MissionTaskItem::getTaskType).setCaption("Type");
		grid.addColumn((mt) -> convertGeoPoint(mt.getStart())).setCaption("Start");
		grid.addColumn((mt) -> convertGeoPoint(mt.getEnd())).setCaption("End");

		grid.setSizeFull();

		grid.addSelectionListener(e -> {
			e.getFirstSelectedItem().ifPresent(item -> handleSelected(item));
		});

		return grid;
	}

	private void handleSelected(MissionTaskItem selected) {
		updateSegmentsOnMap(selected);
	}

	private String convertGeoPoint(GeoPoint point) {
		return "Lat:" + point.getLatitudeDecimalDegrees() + "°, Lon:" + point.getLongitudeDecimalDegrees() + "°, Alt:"
				+ point.getAltitude() + "m";
	}

	private Component createMapPanel() {
		String APIKEY = System.getProperty("APIKEY", "AIzaSyA0mTfKiCnQGnjQXsQ38wWjMj6wDIXDUgc");

		googleMap = new GoogleMap(APIKEY, null, null);
		// googleMap = new GoogleMap(null, null, null);
		googleMap.setCenter(new LatLon(47.067007, 16.145151));
		googleMap.setZoom(50);
		googleMap.setMapType(MapType.Satellite);
		googleMap.setSizeFull();

		googleMap.addMapClickListener(e -> {
			System.out.println("Lat, Lon" + e.getLat() + "," + e.getLon());
		});
		return googleMap;
	}

	@Override
	public void notify(IEvent event, IEventSource source) {
		if (source == this) {
			return;
		}

		if (event instanceof OperationEvent) {
			updateControls();
			updateTaskList();
			updateOperationStateGrid();
			updateOperationInfo();
		} else if (event instanceof DroneStatusUpdateEvent) {
			long cTime = System.currentTimeMillis();
			if (cTime - lastTime > 2000) {
				lastTime = cTime;
				updateDronesOnMap();
			}

		}

	}

	private void updateControls() {
		boolean running = operationManager.get().isRunning();
		btnStop.setEnabled(running);

		btnStart.setEnabled(operationManager.get().isReadyForStart());
	}

}
