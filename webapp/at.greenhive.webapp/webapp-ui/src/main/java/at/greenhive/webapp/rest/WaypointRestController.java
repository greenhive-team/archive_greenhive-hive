package at.greenhive.webapp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.event.WaypointReceivedEvent;
import at.greenhive.thingy.swarm.SwarmService;

/**
 *
 * @author chris
 */
@RestController
@RequestMapping(path = "/waypoint",
params = {"lat", "lon", "alt"} )
public class WaypointRestController {

	@Autowired
	SwarmService service;
	
    @RequestMapping(method = RequestMethod.POST)
    public String callWaypoint(
    				@RequestParam("lat") double lat,
    				@RequestParam("lon") double lon,
    				@RequestParam("alt") double alt 		
    		) {
    	String r = "Lat:" + lat + ", Lon:" + lon + "Alt:" + alt;
    	System.out.println(r);
    	GlobalEventManager.fire(new WaypointReceivedEvent(lat,lon,alt), null);
    	return r;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String callWaypointGet(
    				@RequestParam("lat") double lat,
    				@RequestParam("lon") double lon,
    				@RequestParam("alt") double alt 		
    		) {
    	return callWaypoint(lat,lon,alt);
    }
}
