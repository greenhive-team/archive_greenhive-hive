package at.greenhive.thingy.cropland;

import org.vaadin.addon.vol3.OLMap.OLClickEvent;

import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;

import at.greenhive.bl.util.geo.GPSPoint;

public class RowToolHandler implements IToolHandler {
	
	private final PointCollector collector;
	
	public RowToolHandler(PointCollector collector) {
		this.collector = collector;
	}

	@Override
	public void handleMapClick(LatLon position) {
		System.out.println("Row tool:" + asString(position));
	}

	@Override
	public void handleMarkerDrag(GoogleMapMarker marker, LatLon position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleMapClick(OLClickEvent event) {
		
		
	}

	@Override
	public void setPoint(GPSPoint gpsPoint) {	
		collector.addRowPoint(gpsPoint);
	}

}
