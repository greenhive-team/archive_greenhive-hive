package at.greenhive.thingy.settings;

import com.vaadin.ui.Component;

import at.greenhive.thingy.ThingyViewCreator;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class  SettingsThingyViewCreator extends ThingyViewCreator< SettingsThingy>{

	public SettingsThingyViewCreator( SettingsThingy thingy, GreenhiveEventBus eventBus) {
		super(thingy, eventBus);
	}

	@Override
	public Component getRightView() {
		return null;
	}

	@Override
	public Component getMiddleView() {
		return new SettingsView(eventBus );
	}

	@Override
	public Component getLeftView() {
		return null;
	}
}