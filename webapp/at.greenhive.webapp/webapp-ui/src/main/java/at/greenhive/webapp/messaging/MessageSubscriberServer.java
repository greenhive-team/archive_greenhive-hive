package at.greenhive.webapp.messaging;

import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class MessageSubscriberServer {

	public void startSubscriberServer(ApplicationContext applicationContext) {
		
		ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");
		taskExecutor.setThreadGroupName("SubscriberServer");
		taskExecutor.setThreadNamePrefix("SubscriberServer");
		MessageSubscriberTask task = (MessageSubscriberTask) applicationContext.getBean("messageSubscriberTask");
		//task.setName("MessageSubscriberTask 1");
	    taskExecutor.execute(task);
	
	}
}
