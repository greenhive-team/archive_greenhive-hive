package at.greenhive.webapp.messaging;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import at.greenhive.bl.airtrafficcontroller.DroneTakeoffStatusUpdateEvent;
import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.hive.HiveGPSUpdateEvent;
import at.greenhive.bl.util.geo.CoordinateConversions;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.protobuf.Airtrafficcontroller.GoingToHive;
import at.greenhive.protobuf.HeartbeatOuterClass.Heartbeat;
import at.greenhive.protobuf.Hive.HiveHeartbeat;
import at.greenhive.protobuf.Message.Container;
import at.greenhive.protobuf.MissionOuterClass.MissionAck;
import at.greenhive.protobuf.MissionOuterClass.TaskComplete;
import at.greenhive.protobuf.MissionOuterClass.TaskCompleteAck;
import at.greenhive.protobuf.Types.ContainerType;
import at.greenhive.thingy.swarm.event.DroneGoingToHiveEvent;
import at.greenhive.thingy.swarm.event.DroneMissionAckEvent;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.thingy.swarm.event.DroneTaskCompleteEvent;
import me.limespace.appbase.event.ApplicationErrorEvent;
import me.limespace.appbase.event.IEvent;

public class MessageTask {

	//private static Logger log = Logger.getLogger(MessageTask.class.getCanonicalName());
	private final static Logger log = LoggerFactory.getLogger(MessageTask.class);
	
	protected static Container handleMessage(String droneId, Container container) {

		log.debug("Got Message of Type: " + container.getType().name() +" from drone '"+droneId+"'");
		try {
			log.debug(JsonFormat.printer().print(container));
		} catch (InvalidProtocolBufferException e) {
			// Ignored
		}
		ContainerType type = container.getType();

		switch (type) {
            case GH_PING:
                return Container.newBuilder().setType(ContainerType.GH_PING_ACKNOWLEDGE).build();
            case GH_HEARTBEAT:
				Heartbeat hb = container.getHeartbeat();
				GlobalEventManager.fire( getEventFromMessage(hb) , null);
                return null;
			case GH_TAKEOFF_STATUS:
				boolean ready = container.getTakeoffStatus().getReady();
				DroneTakeoffStatusUpdateEvent event = new DroneTakeoffStatusUpdateEvent( droneId, ready);
				GlobalEventManager.fire( event , null);
                return null;
			case GH_HIVEHEARTBEAT:
				HiveHeartbeat hhb = container.getHiveHeartbeat();
				GlobalEventManager.fire( getEventFromMessage( hhb) , null);
                return null;
			case GH_TASK_COMPLETE:
				DroneTaskCompleteEvent eventTaskComplete = getEventFromMessage(droneId, container.getTaskComplete());
				GlobalEventManager.fire(eventTaskComplete , null);
				return buildTaskCompleteAckMessage(eventTaskComplete.getTaskId());
			case GH_MISSION_ACK:
				GlobalEventManager.fire( getEventFromMessage(droneId, container.getMissionAck()) , null);
                return null;
			case GH_GOING_TO_HIVE: //TODO: ACK Message 
				GlobalEventManager.fire( getEventFromMessage(droneId, container.getGoingToHive()) , null);
                return null;
				
			default:
				GlobalEventManager.fire( new ApplicationErrorEvent("Unsupported Messagetype:" + type.name()) , null);
				return null;
		}
	}

	private static Container buildTaskCompleteAckMessage(int taskId) {
		return Container.newBuilder()
					.setType(ContainerType.GH_TASK_COMPLETE_ACK)
					.setTaskCompleteAck(TaskCompleteAck.newBuilder()
											.setTaskId(taskId)
											.build()
										)
					.build();
	}

	private static IEvent getEventFromMessage(String droneId, GoingToHive goingToHive) {
		DroneGoingToHiveEvent event = new DroneGoingToHiveEvent(droneId, goingToHive.getReason());
		return event;
	}

	private static IEvent getEventFromMessage(String droneId, MissionAck missionAck) {
		int missionId = missionAck.getMissionId();
		DroneMissionAckEvent event = new DroneMissionAckEvent(droneId, missionId);
		return event;
	}

	private static DroneTaskCompleteEvent getEventFromMessage(String droneId,TaskComplete taskComplete) {
		int taskId = taskComplete.getTaskId();
		DroneTaskCompleteEvent event = new DroneTaskCompleteEvent(droneId, taskId, taskComplete.getSprayAmount());
		return event;
	}

	private static IEvent getEventFromMessage(HiveHeartbeat hhb) {
		GPSPoint p = CoordinateConversions.fromMessageGPS(hhb.getPosition());
		HiveGPSUpdateEvent event = new HiveGPSUpdateEvent(p);
		return event;
	}

	private static DroneStatusUpdateEvent getEventFromMessage(Heartbeat hb) {
		DroneStatusUpdateEvent event = new DroneStatusUpdateEvent(hb.getId());
		event.setPositon(	hb.getPosition().getCoordinates().getLatitude(),
							hb.getPosition().getCoordinates().getLongitude(),
							hb.getPosition().getCoordinates().getAltitude());
		
		event.setBattery(hb.getBattery());
		event.setStatus(hb.getStatus());
		event.setFillLevel(hb.getFillLevel());
		
		return event;
	}
}
