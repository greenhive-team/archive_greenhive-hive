package at.greenhive.webapp.rest;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.greenhive.thingy.swarm.DroneState;
import at.greenhive.thingy.swarm.Drone;
import at.greenhive.thingy.swarm.Swarm;
import at.greenhive.thingy.swarm.SwarmService;

/**
 *
 * @author chris
 */
@RestController
@RequestMapping(path = "/swarm")
public class SwarmRestController {

	@Autowired
	SwarmService service;
	
    @RequestMapping(method = GET)
    public Collection<Swarm> list() {
    	return service.getAll();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public Swarm get(@PathVariable String id) {
    	return service.getSwarm(id);
    }
    
    @RequestMapping(value = "/bumblebee/{swarmId}/{beeId}", method = GET)
    public Drone getBumblebee(@PathVariable String swarmId,@PathVariable String beeId) {
    	return service.getBumblebee(swarmId, beeId);
    }
    
    @RequestMapping(value = "/bumblebee/state/{swarmId}/{beeId}", method = GET)
    public DroneState getBumblebeeState(@PathVariable String swarmId,@PathVariable String beeId) {
    	return service.getBumblebee(swarmId, beeId).getState();
    }

    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity<?> put(@PathVariable String id, @RequestBody Object input) {
        return null;
    }

    @RequestMapping(value = "/{id}", method = POST)
    public ResponseEntity<?> post(@PathVariable String id, @RequestBody Object input) {
        return null;
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<Object> delete(@PathVariable String id) {
        return null;
    }

}
