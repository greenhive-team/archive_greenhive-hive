package at.greenhive.webapp;

import java.util.List;
import java.util.logging.Logger;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import at.greenhive.bl.Thingy;
import at.greenhive.bl.User;
import at.greenhive.thingy.IThingyViewCreator;
import at.greenhive.thingy.ThingyManager;
import at.greenhive.thingy.ThingyRegistry;
import at.greenhive.webapp.event.GreenhiveEventBus;
import at.greenhive.webapp.event.ThingyActivateRequestedEvent;
import at.greenhive.webapp.login.LoginView;
import at.greenhive.webapp.view.MainView;
import me.limespace.appbase.environment.IEnvironment;
import me.limespace.appbase.environment.IUser;
import me.limespace.appbase.event.ApplicationErrorEvent;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.UserLoggedOutEvent;
import me.limespace.appbase.event.UserLoginRequestedEvent;
import me.limespace.appbase.event.utils.StringUtils;
import me.limespace.appbase.preferences.IPreferences;

/**
 *
 */

@SpringUI
@Theme("greenhive")
@Push
public class GreenhiveUI extends UI implements IEnvironment {

	private @Autowired AutowireCapableBeanFactory beanFactory;
	
	private class MenuClickListener implements ClickListener {

		Thingy thingy;

		public MenuClickListener(Thingy thingy) {
			this.thingy = thingy;
		}

		private static final long serialVersionUID = 1L;

		@Override
		public void click(ClickEvent event) {
			Notification.show(thingy.getName(), Type.TRAY_NOTIFICATION);
			eventBus.post(new ThingyActivateRequestedEvent(thingy));
		}
	}

	private static final long serialVersionUID = 1L;

	private Label userInfo;
	private Label modeInfo;
	private Panel contentArea;
	private Panel statusArea;
	private Panel menuArea;

	private final GreenhiveEventBus eventBus = new GreenhiveEventBus();

	private ThingyManager thingyManager;

	private ThingyManager getThingyManager() {
		if (thingyManager == null) {
			thingyManager = new ThingyManager(eventBus);
		}
		return thingyManager;
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		updateContent(null);
	}

	@Override
	public void attach() {
		eventBus.register(this);
		super.attach();
	}

	@Override
	public void detach() {
		eventBus.unregister(this);
		super.detach();
	}

	private void updateContent(User user) {
		if (user != null) {
			// Authenticated user
			contentArea.setContent(new MainView());
			removeStyleName("loginview");
			// getNavigator().navigateTo(getNavigator().getState());
		} else {
			setContent(new LoginView(eventBus));
			addStyleName("loginview");
		}
	}

	private void createAppUI() {

		VerticalLayout vLayout = new VerticalLayout();
		vLayout.setMargin(false);
		vLayout.addComponent(createHeader());
		vLayout.setSizeFull();

		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.addStyleName("contentarea");
		hLayout.addComponent(createMenu(getUser()));
		hLayout.setSizeFull();

		contentArea = createContent();
		hLayout.addComponent(contentArea);
		hLayout.setExpandRatio(contentArea, 1f);

		vLayout.addComponent(hLayout);
		vLayout.setExpandRatio(hLayout, 1f);
		setSizeFull();
		setContent(vLayout);
	}


	private void createThingyMenus(AbsoluteLayout menuLayout) {

		List<ThingyRegistry> thingyList = getThingyManager().getThingyList();

		int top = 20;
		for (ThingyRegistry tr : thingyList) {
			Thingy thingy = tr.getThingy();
			Image item = new Image();
			String tooltip = thingy.getTooltipText(getLocale());
			
			item.setDescription(thingy.getDisplayName(getLocale()) + (tooltip != null ? (", " + tooltip) : "" ));
			item.setSource(new ThemeResource("icons/" + thingy.getIconPath()));
			item.addStyleName(GreenhiveStyles.STYLE_MENUITEM);
			menuLayout.addComponent(item, "left:0px;top:" + top + "px");
			item.addClickListener(new MenuClickListener(thingy));
			top += 120;
		}
	}

	private Panel createContent() {
		Panel content = new Panel();
		content.addStyleName(ValoTheme.PANEL_BORDERLESS);
		content.setSizeFull();
		content.setContent(new Label("hello world"));
		return content;

	}

	private Component createHeader() {

		HorizontalLayout header = new HorizontalLayout();

		header.addStyleName(GreenhiveStyles.STYLE_MAINHEADER);
		header.setHeight(64, Unit.PIXELS);
		header.setWidth(100, Unit.PERCENTAGE);
		header.setSpacing(true);
		header.setMargin(false);

		Label spacer = new Label();
		spacer.setWidth(30, Unit.PIXELS);
		header.addComponent(spacer);

		Label userInfo = createUserInfo(null);
		header.addComponent(userInfo);
		header.setComponentAlignment(userInfo, Alignment.MIDDLE_RIGHT);

		spacer = new Label();
		spacer.addStyleName(GreenhiveStyles.STYLE_MAINHEADER_SPACER);
		spacer.setWidth(20, Unit.PIXELS);
		spacer.setHeight(40, Unit.PIXELS);
		header.addComponent(spacer);
		header.setComponentAlignment(spacer, Alignment.MIDDLE_LEFT);

		Label modeInfo = createModeInfo();
		header.addComponent(modeInfo);
		header.setComponentAlignment(modeInfo, Alignment.MIDDLE_LEFT);

		
		Embedded appLogo = new Embedded();
		appLogo.setSizeUndefined();
		appLogo.setHeight(55, Unit.PIXELS);
		appLogo.setWidth(173, Unit.PIXELS);
		appLogo.setSource(new ThemeResource("icons/greenhive-logo-hell-55.png"));
		appLogo.setType(1);
		appLogo.setMimeType("image/png");
		appLogo.addStyleName(GreenhiveStyles.STYLE_MAINHEADER_CAPTION);
		header.addComponent(appLogo);
		header.setComponentAlignment(appLogo, Alignment.MIDDLE_RIGHT);
		
		Embedded appPic = new Embedded();
		appPic.setSizeUndefined();
		appPic.setHeight(55, Unit.PIXELS);
		appPic.setWidth(64, Unit.PIXELS);
		appPic.setSource(new ThemeResource("icons/greenhive-icon-64.png"));
		appPic.setType(1);
		appPic.setMimeType("image/png");
		appPic.addStyleName(GreenhiveStyles.STYLE_MAINHEADER_CAPTION);
		
		
		header.addComponent(appPic);
		header.setComponentAlignment(appPic, Alignment.MIDDLE_RIGHT);

		// header.setExpandRatio(appPic, 0.10f);
		// header.setExpandRatio(appCaption, 0.50f);
		header.setExpandRatio(modeInfo, 0.3f);
		// header.setExpandRatio(userInfo, 0.4f);
		return header;

	}

	private Component createMenu(User user) {

		AbsoluteLayout menuLayout = new AbsoluteLayout();
		menuLayout.setWidth(65, Unit.PIXELS);
		menuLayout.setHeight(100, Unit.PERCENTAGE);

		if (user != null) {
			createThingyMenus(menuLayout);
		}

		if (menuArea == null) {
			menuArea = new Panel();
			menuArea.setWidth(65, Unit.PIXELS);
			menuArea.setHeight(100, Unit.PERCENTAGE);
			menuArea.addStyleName(ValoTheme.PANEL_BORDERLESS);

		}
		menuArea.setContent(menuLayout);

		return menuArea;
	}

	private Component createStatusBar() {
		
		Panel statusArea = new Panel();
		statusArea.setWidth(100, Unit.PERCENTAGE);
		statusArea.setHeight(25, Unit.PIXELS);
		
		return statusArea;
	}
	
	private Label createModeInfo() {
		modeInfo = new Label();
		modeInfo.addStyleName(GreenhiveStyles.STYLE_MAINHEADER_CAPTION);
		modeInfo.addStyleName(GreenhiveStyles.STYLE_MAINHEADER_CAPTIONMODE);
		modeInfo.setSizeUndefined();

		return modeInfo;
	}

	private void updateModeInfo(String strModeInfo) {
		modeInfo.setValue(strModeInfo);
	}

	private Label createUserInfo(User user) {

		userInfo = new Label();
		updateUserInfo(user);
		userInfo.addStyleName(GreenhiveStyles.STYLE_MAINHEADER_CAPTION);
		userInfo.setSizeUndefined();
		return userInfo;
	}

	private void updateUserInfo(User user) {
		String strUserInfo = "Please login ...";
		if (user != null) {
			strUserInfo = StringUtils.capitalizeFirstLetter(user.getUserName());
		}
		userInfo.setValue(strUserInfo);
	}

	private User getUser() {
		User user = (User) VaadinSession.getCurrent().getAttribute(User.class.getName());
		return user;
	}

	@Subscribe
	public void userLoginRequested(final UserLoginRequestedEvent event) {
		User user = new User(event.getUserid(), event.getPassword());
		VaadinSession.getCurrent().setAttribute(User.class.getName(), user);

		if (user != null) {
			createAppUI();

			updateUserInfo(user);
			createMenu(user);
			updateContent(user);
		}
	}

	@Subscribe
	public void logEvents(IEvent event) {
		Logger.getLogger(GreenhiveUI.class.getName()).info("Event: " + event.getClass().getName() + " risen.");
	}

	@Subscribe
	public void userLoggedOut(final UserLoggedOutEvent event) {
		// When the user logs out, current VaadinSession gets closed and the
		// page gets reloaded on the login screen. Do notice the this doesn't
		// invalidate the current HttpSession.
		updateUserInfo(null);
		VaadinSession.getCurrent().setAttribute(User.class.getName(), null);
		VaadinSession.getCurrent().close();
		Page.getCurrent().reload();
	}

	@Subscribe
	public void thingyActivate(final ThingyActivateRequestedEvent event) {
		Thingy thingy = event.getThingy();
		thingy.startup(this, getUser());
		updateModeInfo(thingy.getDisplayName(getLocale()));

		IThingyViewCreator viewCreator = getThingyManager().getViewCreator(thingy);

		if (viewCreator != null) {
			Component content = viewCreator.createView();
			springify(content);
			
			content.setSizeFull();
			contentArea.setContent(content);
		}
	}

	private void springify(Component content) {
		beanFactory.autowireBean(content);
		if (content instanceof HasComponents) {
			for (Component c: (HasComponents)content) {
				springify(c);
			}
		}
	}

	@Subscribe
	public void handleError(ApplicationErrorEvent error) {
		Notification.show("Application Error occured", error.getCause().getMessage(), Type.ERROR_MESSAGE);
	}

	public static GreenhiveEventBus getEventbus() {
		return ((GreenhiveUI) getCurrent()).eventBus;
	}

	@WebServlet(urlPatterns = "/*", name = "GreenhiveUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = GreenhiveUI.class, productionMode = false)
	public static class GreenhiveUIServlet extends VaadinServlet {
		private static final long serialVersionUID = 1L;
	}
	
	@Override
	public IPreferences getUserPreferences(IPreferences.Context context, String name, IUser user) {
		return null;
	}
	
}
