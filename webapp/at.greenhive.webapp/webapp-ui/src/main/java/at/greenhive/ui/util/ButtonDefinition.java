package at.greenhive.ui.util;

public class ButtonDefinition {
	
	@FunctionalInterface 
	public interface ICallback {
		void call();
	}
	
	String caption;
	ICallback callback;
	
	public ButtonDefinition(String caption, ICallback callback) {
		this.caption = caption;
		this.callback = callback;
	}
	
	
	
}
