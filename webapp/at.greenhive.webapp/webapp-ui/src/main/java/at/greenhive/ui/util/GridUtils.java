package at.greenhive.ui.util;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

import at.greenhive.bl.IService;

public class GridUtils {
	public static <T> Component createCRUDGrid(Class<T> entityClass, IService<T> service ) {
		
		VerticalLayout layout = new VerticalLayout();
		
		
		 ButtonDefinition[] defs  = {
					new ButtonDefinition("test1", GridUtils::func1),
					new ButtonDefinition("test2", GridUtils::func2)
			};
		 
		Component buttonBar = createButtonBar(defs);
		layout.addComponentAsFirst(buttonBar);
		
		Grid<T> grid = new Grid<T>();
		
		layout.addComponent(grid);
		return layout;
	}
	
	private static void func2() {
		// TODO Auto-generated method stub
	}

	private static void func1() {
		// TODO Auto-generated method stub
	}

	public static Component createButtonBar(ButtonDefinition ... btnDefs) {
		
		HorizontalLayout buttonBar = new HorizontalLayout();
		for ( ButtonDefinition btnDef:btnDefs) {
			Button btn = new Button( btnDef.caption);
			btn.addClickListener(e -> btnDef.callback.call());
		}
		return buttonBar;
	}
}
