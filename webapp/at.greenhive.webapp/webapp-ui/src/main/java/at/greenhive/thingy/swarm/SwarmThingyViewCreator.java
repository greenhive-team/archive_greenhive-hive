package at.greenhive.thingy.swarm;

import com.vaadin.ui.Component;

import at.greenhive.thingy.SmartView;
import at.greenhive.thingy.ThingyViewCreator;
import at.greenhive.webapp.event.GreenhiveEventBus;

public class  SwarmThingyViewCreator extends ThingyViewCreator<SwarmThingy>{

	public SwarmThingyViewCreator( SwarmThingy thingy, GreenhiveEventBus eventbus) {
		super(thingy, eventbus);
	}

	@Override
	public Component getRightView() {
		return new SmartView(thingy);
	}

	@Override
	public Component getMiddleView() {
		return new SwarmView(eventBus);
	}

	@Override
	public Component getLeftView() {
		return null;
	}
}