package at.greenhive.thingy.swarm;

import com.google.common.eventbus.Subscribe;
import com.google.protobuf.ByteString;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import at.greenhive.bl.airtrafficcontroller.SendDroneMessageEvent;
import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.hive.HiveService;
import at.greenhive.bl.util.HexUtil;
import at.greenhive.bl.util.geo.CoordinateConversions;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.protobuf.Message.Container;
import at.greenhive.protobuf.Rtk;
import at.greenhive.protobuf.Types.ContainerType;
import at.greenhive.webapp.event.GreenhiveEventBus;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class DronePanel extends Panel implements IEventListener {
	private static final long serialVersionUID = 1L;
	
	String[] testRTKs = {
			"D3 00 50 43 20 00 8B DF 4F E6 00 00 50 40 53 00 00 00 00 00 20 00 00 00 7F 48 43 4D 4B 4B 46 46 53 DE 66 BA 39 6D 78 A4 60 7A 78 11 60 89 85 E7 06 3C 73 E6 2C 88 10 D5 17 FE F9 40 44 8C 85 72", 
			"D3 00 7C 43 50 00 8B DF 4F E6 00 00 50 40 53 00 00 00 00 00 20 00 00 00 7F 48 43 4D 4B 4B 46 46 00 00 00 05 3D E6 6B A3 96 D7 8A 46 3D EB FE DC 28 5F DE C0 26 FB B4 11 E1 E9 DD 02 2B 40 89 86" ,
			"D3 00 37 43 C0 00 D2 D8 C4 A4 00 00 18 00 30 00 00 00 00 00 20 00 00 00 7A 0A 2A 22 11 89 A8 19 B9 FF B4 2D 3D 47 F7 80 99 7F 0E B7 E6 FD 30 77 6B E0 02 E7 7F FF 84 F5 59 00 77 B5 00", 
			"D3 00 06 4C E0 00 88 10 97 C2 44 8B", 
			"D3 00 13 3E D0 00 03 09 BB F9 79 66 82 D1 72 3A 57 0A D1 DC FC 3F 1C BD 66" 
	};
	
	private GreenhiveEventBus eventBus;
		
	private Drone bee;

	private Label stateLbl;
	
	public DronePanel(GreenhiveEventBus eventBus, Drone bee) {
		
		super(bee.getSerialNumber());
		this.eventBus = eventBus;
		this.bee = bee;
		
		setSizeFull();

		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
		
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setMargin(true);
		buttonBar.addComponent(new Button("Test Message", e -> sendTestMessage()));
		buttonBar.addComponent(new Button("Send TakeOff Status Message", e -> sendTakeOffCheckMessage()));
		buttonBar.addComponent(new Button("Send TakeOff Message", e -> sendTakeOffMessage()));
		buttonBar.addComponent(new Button("Send RTK", e -> sendTestRTK()));
		layout.addComponent(buttonBar);
		
		stateLbl = createStatePanel();
		layout.addComponent(stateLbl);
		
		layout.setExpandRatio(buttonBar, 0);
		layout.setExpandRatio(stateLbl, 1);
		
		CheckBox showDroneOnMap = new CheckBox("Show on map");
		showDroneOnMap.addValueChangeListener(e -> eventBus.post(new RegisterDrone4MapEvent(e.getValue(), bee)));
		setContent(layout);
	}
	
	private Label createStatePanel() {
		
		DroneState state = bee.getState();
		
		String stateTxt = createStateHTML(state);
		Label stateLbl = new Label(stateTxt, ContentMode.HTML);
		return stateLbl;
		
	}

	private String createStateHTML(DroneState state) {
		StringBuilder sb = new StringBuilder();
		
		if (state != null) {
			sb.append("<p>");
			
			sb.append("<b>");
			sb.append("State: ");
			sb.append("</b>");
			sb.append(state.getStatus());
			sb.append("<br />");
			
			sb.append("<b>");
			sb.append("Fill: ");
			sb.append("</b>");
			sb.append(state.getFillLevel());
			sb.append("<br />");
			
			sb.append("<b>");
			sb.append("Power: ");
			sb.append("</b>");
			sb.append(state.getPowerLevel());
			sb.append("<br />");
			
			sb.append("<b>");
			sb.append("Position: ");
			sb.append("</b>");
			sb.append(formatPosition(state.getPosition()));
			sb.append("<br />");
	
			sb.append("<b>");
			sb.append("Distance to HIVE: ");
			sb.append("</b>");
			GPSPoint hiveLocation = HiveService.getHiveLocation();
			double calcDistance = CoordinateConversions.calcDistance(hiveLocation, state.getPosition());
			sb.append(calcDistance);
			sb.append("<br />");
	
			
			sb.append("<b>");
			sb.append("Last Update: ");
			sb.append("</b>");
			sb.append(state.getTimestamp());
			sb.append("<br />");
			
			sb.append("</p>");
		} else {
			sb.append("no current state");
		}
		
		return sb.toString();
	}

	private String formatPosition(GPSPoint postition) {
		return postition.toString();
	}

	@Override
	public void attach() {
		super.attach();
		eventBus.register(this);
		GlobalEventManager.registerListener(this);
	}
	
	@Override
	public void detach() {
		super.detach();
		eventBus.unregister(this);
		GlobalEventManager.unregisterListener(this);
	}
	
	private void sendTestMessage() {
		Container container = Container.newBuilder().setType(ContainerType.GH_PING).build();
		GlobalEventManager.fire(new SendDroneMessageEvent(bee.getSerialNumber(), container), null);
	}
	
	private void sendTakeOffCheckMessage() {
		Container container = Container.newBuilder().setType(ContainerType.GH_TAKEOFF_CHECK).build();
		GlobalEventManager.fire(new SendDroneMessageEvent(bee.getSerialNumber(), container), null);
	}
	
	private void sendTakeOffMessage() {
		Container container = Container.newBuilder().setType(ContainerType.GH_TAKEOFF).build();
		GlobalEventManager.fire(new SendDroneMessageEvent(bee.getSerialNumber(), container), null);
	}
	
	private void sendTestRTK() {
		for (String rtk: testRTKs) {
			byte[] data = HexUtil.parseHexBinary(rtk);
			Container rtkMessage = Container.newBuilder()
					.setType(ContainerType.GH_RTK)
					.setRtkCorrection(Rtk.rtk.newBuilder()
										.setMessageBuffer(ByteString.copyFrom(data))
										.build())
					.build();
			GlobalEventManager.fire(new SendDroneMessageEvent(bee.getSerialNumber(), rtkMessage), null);
		}
	}
	
	private void setStateData(DroneState state) {
		String stateTxt = createStateHTML(state);
		stateLbl.setValue(stateTxt);
		
	}
	
	@Subscribe
	void updateDrohneState(DroneNewState event) {
		if (event.getBee().equals(bee)) {
			this.getUI().access(() -> {
				setStateData(bee.getState());
			});
		}
	}

	@Override
	public void notify(IEvent event, IEventSource source) {
		if (event instanceof DroneNewState) {
			updateDrohneState((DroneNewState)event);
		}
	}
}
