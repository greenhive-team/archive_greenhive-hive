package at.greenhive.thingy.cropland;

import java.util.ArrayList;
import java.util.Collection;

import at.greenhive.bl.util.geo.GPSPoint;

public class PointCollector {
	
	Collection<GPSPoint> rowPoint = new ArrayList<>();
	Collection<GPSPoint> boundPoint = new ArrayList<>();
	Collection<GPSPoint> transitPoint = new ArrayList<>();
	
	public void addRowPoint(GPSPoint point) {
		rowPoint.add(point);
	}
	
	public void addTransitPoint(GPSPoint point) {
		transitPoint.add(point);
	}
	
	public void addBoundaryPoint(GPSPoint point) {
		boundPoint.add(point);
	}
}
