package at.greenhive.webapp.messaging;

import java.util.logging.Logger;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import at.greenhive.bl.event.GlobalEventManager;
import me.limespace.appbase.event.ApplicationErrorEvent;

@Component
@Scope("prototype")
public class MessageSubscriberTask extends MessageTask implements Runnable {

	private static Logger log = Logger.getLogger(MessageSubscriberTask.class.getName());

	@Override
	public void run() {

		Context context = ZMQ.context(1);

		// First, connect our subscriber socket
		Socket subscriber = context.socket(ZMQ.SUB);
		subscriber.subscribe(ZMQ.SUBSCRIPTION_ALL);
		subscriber.bind("tcp://*:5561");

		log.info("Message Subscriber bound to Port 5561." );
		
		while (!Thread.currentThread().isInterrupted()) {

			subscriber.recv();
			
			GlobalEventManager.fire(new ApplicationErrorEvent("Subscriber Server is not supported any more."), null);

			//try {
				//Container container = Container.parseFrom(data);
				//handleMessage(container);
								
//			} catch (InvalidProtocolBufferException e) {
//				e.printStackTrace();
//				GlobalEventManager.instance().fire(new ApplicationErrorEvent(e), null);
//			}

		}

		subscriber.close();
		context.term();
	}

}
