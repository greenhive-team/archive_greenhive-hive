
DROP TABLE IF EXISTS users;
create table users (
  	username varchar(256),
  	password varchar(256),
  	enabled boolean
);

DROP TABLE IF EXISTS cropland;
create table cropland (
	
	clid int primary key,
	
	croptype int,
	default_spray_height decimal(5,2),
	max_speed decimal(5,2),
	name varchar(50),
	description varchar(1000)

	);

DROP TABLE IF EXISTS cropland_hive_location;
create table cropland_hive_location (
	
	clid int primary key,
	
	lat  decimal (29,25),
	lon decimal (29,25),
	alt  decimal (29,25),
	
	CONSTRAINT pk_cropland_hive_location PRIMARY KEY (clid)

	);
	
DROP TABLE IF EXISTS cropland_bounds;
create table cropland_bounds (
	
	clid int,
	wpid int,
	
	lat  decimal (29,25),
	lon decimal (29,25),
	alt  decimal (29,25),
	
	CONSTRAINT pk_cropland_bounds PRIMARY KEY (clid,wpid)
);

DROP TABLE IF EXISTS cropland_rows;
create table cropland_rows (
    clid int,
    rowid int,
    wpid int,
    
    lat  decimal (29,25),
    lon decimal (29,25),
    alt  decimal (29,25),
    
    CONSTRAINT pk_cropland_rows PRIMARY KEY (clid,rowid,wpid)
);

DROP TABLE IF EXISTS cropland_paths;
create table cropland_paths (
    clid int,
    pathid int,
    wpid int,
        
    lat  decimal (29,25),
    lon decimal (29,25),
    alt  decimal (29,25),
    
    CONSTRAINT pk_cropland_paths PRIMARY KEY (clid,pathid,wpid)
);

DROP TABLE IF EXISTS operation;
create table operation (
	
	oid int ,
	clid int,
	quantity decimal,
	name varchar(100),

	CONSTRAINT pk_operation PRIMARY KEY (oid,clid)
);

DROP TABLE IF EXISTS operation_path;
create table operation_path (
	
	oid int ,
	clid int,
	pathid int,
	pointid int,
	x int,
	y int,
	z int,
	
	CONSTRAINT pk_operation_path PRIMARY KEY (oid, clid, pathid, pointid)
);

DROP TABLE IF EXISTS bumblebee;
create table bumblebee (
	
	serialid char(32) ,
	
	name varchar(100),
	swarmid char(32),
	
	CONSTRAINT pk_bumblebee PRIMARY KEY (serialid)
);

DROP TABLE IF EXISTS ant;
create table ant (
	
	serialid char(32) ,
	
	name varchar(100),
	swarmid char(32),
	
	CONSTRAINT pk_ant PRIMARY KEY (serialid)
);

DROP TABLE IF EXISTS swarm;
create table swarm (
	
	swarmid char(32) ,
	
	name varchar(100),
	
	CONSTRAINT pk_swarm PRIMARY KEY (swarmid)
);

