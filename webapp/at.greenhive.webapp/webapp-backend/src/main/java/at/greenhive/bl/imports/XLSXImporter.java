package at.greenhive.bl.imports;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import at.greenhive.thingy.cropland.CropType;
import at.greenhive.thingy.cropland.CroplandBoundBean;
import at.greenhive.thingy.cropland.CroplandMetadata;
import at.greenhive.thingy.cropland.CroplandRowBean;
import at.greenhive.thingy.cropland.CroplandService;
import at.greenhive.thingy.missionplaner.mission.OperationMetaData;
import at.greenhive.thingy.operation.OperationService;

public class XLSXImporter {
	
	//private static final Logger logger = LoggerFactory.getLogger(XLSXImporter.class);
	
	enum CroplandCols {
		CLID, CROPTYPE, DEFAULT_SPRAY_HEIGHT, MAX_SPEED, NAME, DESCRIPTION;
	}

	enum CroplandBoundCols {
		CLID, WPID, LAT, LON, ALT;
	}

	enum CroplandRowCols {
		CLID, ROWID, WPID, LAT, LON, ALT;
	}

	enum OperationCols {
		OID, CLID, QUANTITY, NAME;
	}

	private CroplandService croplandService;
	private OperationService operationService;

	public XLSXImporter(CroplandService croplandService, OperationService operationService) {
		this.croplandService = croplandService;
		this.operationService = operationService;
	}

	private void importCropLands(Sheet sheet) {
		Iterator<Row> rowIterator = sheet.iterator();

		List<CroplandMetadata> cmList = new ArrayList<>();

		while (rowIterator.hasNext()) {
			// Cropland cl = new Cropland(metadata, outline, rows);

			Row row = rowIterator.next();

			Cell clidCell = row.getCell(CroplandCols.CLID.ordinal());
			if (clidCell != null && clidCell.getCellType() == CellType.NUMERIC) {

				int clid = (int) clidCell.getNumericCellValue();
				int cropType = (int) row.getCell(CroplandCols.CROPTYPE.ordinal()).getNumericCellValue();
				double defaultSpayHeight = row.getCell(CroplandCols.DEFAULT_SPRAY_HEIGHT.ordinal())
						.getNumericCellValue();
				String name = row.getCell(CroplandCols.NAME.ordinal()).getStringCellValue();
				String desc = row.getCell(CroplandCols.DESCRIPTION.ordinal()).getStringCellValue();

				CroplandMetadata cm = new CroplandMetadata(clid);
				cm.setCroptype(CropType.fromOrdinal(cropType));
				cm.setDefaultSprayHeight(defaultSpayHeight);
				cm.setMaxSpeed(2);
				cm.setName(name);
				cm.setDescription(desc);
				cmList.add(cm);
			}
		}

		if (!cmList.isEmpty()) {
			croplandService.replaceAllCroplands(cmList);
		}

	}

	private void importCropLandBounds(Sheet sheet) {
		Iterator<Row> rowIterator = sheet.iterator();

		List<CroplandBoundBean> bbList = new ArrayList<>();

		while (rowIterator.hasNext()) {

			Row row = rowIterator.next();

			Cell clidCell = row.getCell(CroplandBoundCols.CLID.ordinal());
			if (clidCell != null && clidCell.getCellType() == CellType.NUMERIC) {

				int clid = (int) clidCell.getNumericCellValue();
				int wpid = (int) row.getCell(CroplandBoundCols.WPID.ordinal()).getNumericCellValue();
				double lat = row.getCell(CroplandBoundCols.LAT.ordinal()).getNumericCellValue();
				double lon = row.getCell(CroplandBoundCols.LON.ordinal()).getNumericCellValue();
				double alt = row.getCell(CroplandBoundCols.ALT.ordinal()).getNumericCellValue();

				CroplandBoundBean bb = new CroplandBoundBean();

				bb.setClid(clid);
				bb.setWpid(wpid);
				bb.setLat(lat);
				bb.setLon(lon);
				bb.setAlt(alt);

				bbList.add(bb);
			}
		}

		if (!bbList.isEmpty()) {
			croplandService.replaceAllCroplandBounds(bbList);
		}
	}

	private void importCropLandRows(Sheet sheet) {
		Iterator<Row> rowIterator = sheet.iterator();

		List<CroplandRowBean> crList = new ArrayList<>();

		while (rowIterator.hasNext()) {

			Row row = rowIterator.next();

			Cell clidCell = row.getCell(CroplandRowCols.CLID.ordinal());
			if (clidCell != null && clidCell.getCellType() == CellType.NUMERIC) {

				int clid = (int) clidCell.getNumericCellValue();
				int rowid = (int) row.getCell(CroplandRowCols.ROWID.ordinal()).getNumericCellValue();
				int wpid = (int) row.getCell(CroplandRowCols.WPID.ordinal()).getNumericCellValue();
				double lat = row.getCell(CroplandRowCols.LAT.ordinal()).getNumericCellValue();
				double lon = row.getCell(CroplandRowCols.LON.ordinal()).getNumericCellValue();
				double alt = row.getCell(CroplandRowCols.ALT.ordinal()).getNumericCellValue();

				CroplandRowBean cr = new CroplandRowBean();

				cr.setClid(clid);
				cr.setRowid(rowid);
				cr.setWpid(wpid);
				cr.setLat(lat);
				cr.setLon(lon);
				cr.setAlt(alt);

				crList.add(cr);
			}
		}

		if (!crList.isEmpty()) {
			croplandService.replaceAllCroplandRows(crList);
		}

	}

	private void importOperations(Sheet sheet) {
		Iterator<Row> rowIterator = sheet.iterator();

		List<OperationMetaData> opList = new ArrayList<>();

		while (rowIterator.hasNext()) {

			Row row = rowIterator.next();

			Cell oidCell = row.getCell(OperationCols.OID.ordinal());
			if (oidCell != null && oidCell.getCellType() == CellType.NUMERIC) {

				int oid = (int) oidCell.getNumericCellValue();
				int clid = (int) row.getCell(OperationCols.CLID.ordinal()).getNumericCellValue();
				int quantity = (int)row.getCell(OperationCols.QUANTITY.ordinal()).getNumericCellValue();
				String name  = row.getCell(OperationCols.NAME.ordinal()).getStringCellValue();

				OperationMetaData op = new OperationMetaData(oid);

				op.setCroplandId(clid);
				op.setQuantity(quantity);
				op.setName(name);
				
				opList.add(op);
			}
		}

		if (!opList.isEmpty()) {
			operationService.replaceAllOperations(opList);
		}

	}

	public void importData(InputStream inputStream) throws IOException {
		try (Workbook wb = new XSSFWorkbook(inputStream)) {
			Sheet croplandSheet = wb.getSheet("cropland");
			importCropLands(croplandSheet);

			Sheet croplandBoundsSheet = wb.getSheet("cropland_bounds");
			importCropLandBounds(croplandBoundsSheet);

			Sheet croplandRowsSheet = wb.getSheet("cropland_rows");
			importCropLandRows(croplandRowsSheet);

			Sheet operationSheet = wb.getSheet("operation");
			importOperations(operationSheet);
		}

	}
}
