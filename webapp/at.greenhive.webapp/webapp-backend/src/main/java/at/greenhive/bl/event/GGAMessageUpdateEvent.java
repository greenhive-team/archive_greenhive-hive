package at.greenhive.bl.event;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import me.limespace.appbase.event.IEvent;

/*
0 xxGBS - string $GPGBS GBS Message ID (xx = current Talker ID, see NMEA Talker IDs table)
1 time - hhmmss.ss 235503.00 UTC time to which this RAIM sentence belongs, see note on UTC representation 
2 errLat m numeric 1.6 Expected error in latitude 
3 errLon m numeric 1.4 Expected error in longitude 
4 errAlt m numeric 3.2 Expected error in altitude 
5 svid - numeric 03 Satellite ID of most likely failed satellite 
6 prob - numeric - Probability of missed detection: null (not supported, fixed field) 
7 bias m numeric -21.4 Estimated bias of most likely failed satellite (a priori residual) 
8 stddev m numeric 3.8 Standard deviation of estimated bias 
9 systemId - numeric 1 NMEA defined GNSS System ID, see Signal Identifiers table (only available in NMEA 4.10 and later) 
10 signalId - numeric 0 NMEA defined GNSS Signal ID, see Signal Identifiers table (only available in NMEA 4.10 and later) 
11 cs - hexadecimal *5B Checksum 12 <CR><LF> - character - Carriage return and line feed
*/


/*
0 xxGGA - string $GPGGA GGA Message ID (xx = current Talker ID, see NMEA Talker IDs table) 
1 time - hhmmss.ss 092725.00 UTC time, see note on UTC representation 
2 lat - ddmm. mmmmm 4717.11399 Latitude (degrees & minutes), see format description 

3 NS - character N North/South indicator 
4 lon - dddmm. mmmmm 00833.91590 Longitude (degrees & minutes), see format description 
5 EW - character E East/West indicator 
6 quality - digit 1 Quality indicator for position fix, see position fix flags description 
7 numSV - numeric 08 Number of satellites used (range: 0-12) 
8 HDOP - numeric 1.01 Horizontal Dilution of Precision 
9 alt m numeric 499.6 Altitude above mean sea level 
10 altUnit - character M Altitude units: M (meters, fixed field) 
11 sep m numeric 48.0 Geoid separation: difference between ellipsoid and mean sea level 
12 sepUnit - character M Geoid separation units: M (meters, fixed field) 
13 diffAge s numeric - Age of differential corrections (null when DGPS is not used) 
14 diffStat ion - numeric - ID of station providing differential corrections (null when DGPS is not used) 
15 cs - hexadecimal *5B Checksum 16 <CR><LF> - character - Carriage return and line feed

$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,08,1.01,499.6,M,48.0,M,,*5B

*/

public class GGAMessageUpdateEvent implements IEvent {
	
	private enum MessagePart {
		PRE,TIME,LAT,NS,LON,EW,QUALITY,NUMSV,HDOP,ALT,ALTUNIT,SEP,SEPUNIT,DIFFAGE,DIFFSTAT
	}

	private String[] data;

	public GGAMessageUpdateEvent(String[] messageParts) {
		if (messageParts.length != 15) {
			throw new IllegalArgumentException("Invalid part count:" + messageParts.length + ". GGA needs 15 message parts.");
		}
		if (!messageParts[MessagePart.PRE.ordinal()].endsWith("GGA")) {
			throw new IllegalArgumentException("Invalid Message Type:" + messageParts[MessagePart.PRE.ordinal()]);
		}
		this.data = messageParts;
	}
	
	public String getLat() {
		return data[MessagePart.LAT.ordinal()];
	}
	
	public String getLon() {
		return data[MessagePart.LON.ordinal()];
	}
	
	public LocalTime getTime() {
		DateTimeFormatter timePattern = DateTimeFormatter.ofPattern("HHmmss.SS");
		
		LocalTime time = LocalTime.parse(data[MessagePart.TIME.ordinal()], timePattern);
		return time;
	}
}
