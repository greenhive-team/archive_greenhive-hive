package at.greenhive.thingy.swarm.event;

import java.util.Optional;
import java.util.Set;

import at.greenhive.thingy.missionplaner.event.AbstractSelectionEvent;
import at.greenhive.thingy.swarm.Swarm;

public class SwarmSelectionEvent extends AbstractSelectionEvent<Swarm>{

	public SwarmSelectionEvent(Optional<Swarm> firstSelectedItem, Set<Swarm> allSelectedItems,
			Object source) {
		super(firstSelectedItem, allSelectedItems, source);		
	}
}