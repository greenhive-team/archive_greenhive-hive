package at.greenhive.thingy.missionplaner;

public interface IWorkSegment {

	/**
	 * 
	 * @param rate in ml
	 */
	void setRate(double rate);

	/**
	 * 
	 * @return rate in ml (milli liter)
	 */
	double getRate();

	/**
	 * 
	 * @return spray amount in ml
	 */
	double getSprayAmount();

	WorkCoordinate getStartPoint();

	WorkCoordinate getEndPoint();

	void addPathPoint(WorkCoordinate point);

	double getDistance(WorkCoordinate hiveCoordinate);
	double getDistance(IWorkSegment other);

}