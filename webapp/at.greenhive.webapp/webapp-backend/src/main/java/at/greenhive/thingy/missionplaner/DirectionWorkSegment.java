package at.greenhive.thingy.missionplaner;

public class DirectionWorkSegment implements IWorkSegment {
	
	double rate;
	WorkPath path;
	
	public DirectionWorkSegment() {
		path = new WorkPath(0);
	}
	
	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#setRate(double)
	 */
	@Override
	public void setRate(double rate) {
		this.rate = rate;
	}
	
	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getRate()
	 */
	@Override
	public double getRate() {
		return rate;
	}
	
	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getSprayAmount()
	 */
	@Override
	public double getSprayAmount() {
		return rate * path.getLength();
	}

	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getStartPoint()
	 */
	@Override
	public WorkCoordinate getStartPoint() {
		return path.getStartPoint();
	}

	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getEndPoint()
	 */
	@Override
	public WorkCoordinate getEndPoint() {
		return path.getEndPoint();
	}

	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#addPathPoint(at.greenhive.thingy.missionplaner.WorkCoordinate)
	 */
	@Override
	public void addPathPoint(WorkCoordinate point) {
		path.addPathPoint(point);
	}

	@Override
	public double getDistance(IWorkSegment other) {
		return getDistance(other.getStartPoint());
	}
	
	@Override
	public double getDistance(WorkCoordinate other) {
		return WorkCoordinate.calcLength(path.getEndPoint(), other);
	}
}
