package at.greenhive.bl.util.geo;

import com.peertopark.java.geocalc.Coordinate;

public class GPSPoint extends GeoPoint {

	public GPSPoint(Coordinate latitude, Coordinate longitude, long altitude) {
		super(latitude, longitude, altitude);
	}

	@Override
	public String toString() {
		return "GPSPoint [ latitude=" + latitude + ", longitude=" + longitude + ", altitude=" + altitude + "]";
	}
}