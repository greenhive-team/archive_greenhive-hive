package at.greenhive.bl.hive;

import at.greenhive.bl.util.geo.GPSPoint;
import me.limespace.appbase.event.IEvent;

public class HiveGPSUpdateEvent implements IEvent{
	
	private GPSPoint gpsPoint;

	public HiveGPSUpdateEvent(GPSPoint gpsPoint) {
		this.gpsPoint = gpsPoint;
	}
	
	public GPSPoint getGpsPoint() {
		return gpsPoint;
	}
}
