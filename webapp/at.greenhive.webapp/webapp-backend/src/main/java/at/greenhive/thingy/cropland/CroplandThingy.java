package at.greenhive.thingy.cropland;

import java.util.Locale;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.preferences.PreferencesMetaData;

public class CroplandThingy extends Thingy {

	@Override
	public String getIconPath() {
		return "Feld-25.png";
	}

	@Override
	public String getName() {
		return "cropland";
	}

	@Override
	public String getDisplayName(Locale locale) {
		return "Ackerflächen";
	}

	@Override
	public String getTooltipText(Locale locale) {
		return null;
	}

	@Override
	protected PreferencesMetaData getPreferencesMetaData() {
		// TODO Auto-generated method stub
		return null;
	}

}
