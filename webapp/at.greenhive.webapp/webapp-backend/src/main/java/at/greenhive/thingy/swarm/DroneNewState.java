package at.greenhive.thingy.swarm;

import me.limespace.appbase.event.IEvent;

public class DroneNewState implements IEvent {

	private Drone bee;

	public DroneNewState(Drone bee) {
		this.bee = bee;
	}

	public Drone getBee() {
		return bee;
	}
}
