package at.greenhive.thingy.missionplaner;

import java.util.Locale;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.preferences.PreferencesMetaData;

public class MissionPlanerThingy extends Thingy {

	@Override
	public String getIconPath() {
		return "pflanze_sonne-25.png";
	}

	@Override
	public String getName() {
		return "missionplanner";
	}

	@Override
	public String getDisplayName(Locale locale) {
		return "Saison Planer";
	}

	@Override
	public String getTooltipText(Locale locale) {
		return "";
	}

	@Override
	protected PreferencesMetaData getPreferencesMetaData() {
		// TODO Auto-generated method stub
		return null;
	}

}
