package at.greenhive.thingy.cropland;

import java.util.List;

import at.greenhive.bl.util.geo.GeoPoint;

public class CroplandPath {
    int id;
    List<GeoPoint> path;
	//Gps
	
	public CroplandPath(int id) {
	    this.id = id;
	}
	
	public int getId() {
        return id;
    }
	
	public void addPoint(GeoPoint point) {
	    path.add(point);
	}
	
    public List<GeoPoint> getPath() {
        return path;
    }	
    
    public void setPath(List<GeoPoint> path) {
        this.path = path;
    }

}
