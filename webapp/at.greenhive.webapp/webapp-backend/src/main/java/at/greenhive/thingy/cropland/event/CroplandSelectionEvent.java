package at.greenhive.thingy.cropland.event;

import java.util.Optional;
import java.util.Set;

import at.greenhive.thingy.cropland.CroplandMetadata;
import at.greenhive.thingy.missionplaner.event.AbstractSelectionEvent;

public class CroplandSelectionEvent extends AbstractSelectionEvent<CroplandMetadata>{

	public CroplandSelectionEvent(Optional<CroplandMetadata> firstSelectedItem, Set<CroplandMetadata> allSelectedItems,
			Object source) {
		super(firstSelectedItem, allSelectedItems, source);		
	} 

}
