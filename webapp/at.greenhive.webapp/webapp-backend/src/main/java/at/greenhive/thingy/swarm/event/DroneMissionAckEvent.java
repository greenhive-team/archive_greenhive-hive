package at.greenhive.thingy.swarm.event;

public class DroneMissionAckEvent extends DroneEvent {

	private final int missionId;

	public DroneMissionAckEvent(String droneId, int missionId) {
		super(droneId);
		this.missionId = missionId;
	}

	public int getMissionId() {
		return missionId;
	}
}
