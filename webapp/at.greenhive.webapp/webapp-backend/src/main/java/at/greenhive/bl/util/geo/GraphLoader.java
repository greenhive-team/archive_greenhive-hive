/**
 * @author UCSD Intermediate Programming MOOC team
 *
 * A utility class that reads various kinds of files into different 
 * graph structures.
 */
package at.greenhive.bl.util.geo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.javatuples.Triplet;

public class GraphLoader {

	public static MapGraph createMap(List<Triplet<GeoPoint, GeoPoint, String>> pathList) {

		MapGraph map = new MapGraph();
		Collection<GeoPoint> nodes = new HashSet<>();
		HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> pointMap = buildPointMapOneWay(pathList);
		List<GeoPoint> intersections = findIntersections(pointMap);
		for (GeoPoint pt : intersections) {
			map.addVertex(pt);
			nodes.add(pt);
		}
		addEdgesAndSegments(nodes, pointMap, map);
		return map;
	}

	// Once you have built the pointMap and added the Nodes,
	// add the edges and build the road segments if the segments
	// map is not null.
	private static void addEdgesAndSegments(Collection<GeoPoint> nodes,
			HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> pointMap, MapGraph map) {

		// Now we need to add the edges
		// This is the tricky part
		for (GeoPoint pt : nodes) {
			// Trace the node to its next node, building up the points
			// on the edge as you go.
			List<LinkedList<RoadLineInfo>> inAndOut = pointMap.get(pt);
			LinkedList<RoadLineInfo> outgoing = inAndOut.get(0);
			for (RoadLineInfo info : outgoing) {
				HashSet<GeoPoint> used = new HashSet<GeoPoint>();
				used.add(pt);

				List<GeoPoint> pointsOnEdge = findPointsOnEdge(pointMap, info, nodes);
				GeoPoint end = pointsOnEdge.remove(pointsOnEdge.size() - 1);
				double length = getRoadLength(pt, end, pointsOnEdge);
				map.addEdge(pt, end, info.roadName, info.roadType, length);

			}
		}
	}

	// Calculate the length of this road segment taking into account all of the
	// intermediate geographic points.
	private static double getRoadLength(GeoPoint start, GeoPoint end, List<GeoPoint> path) {
		double dist = 0.0;
		GeoPoint curr = start;
		for (GeoPoint next : path) {
			dist += CoordinateConversions.calcDistance(curr, next);
			curr = next;
		}
		dist += CoordinateConversions.calcDistance(curr, end);
		return dist;
	}

	private static List<GeoPoint> findPointsOnEdge(HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> pointMap,
			RoadLineInfo info, Collection<GeoPoint> nodes) {
		List<GeoPoint> toReturn = new LinkedList<GeoPoint>();
		GeoPoint pt = info.point1;
		GeoPoint end = info.point2;
		List<LinkedList<RoadLineInfo>> nextInAndOut = pointMap.get(end);
		LinkedList<RoadLineInfo> nextLines = nextInAndOut.get(0);
		while (!nodes.contains(end)) {
			toReturn.add(end);
			RoadLineInfo nextInfo = nextLines.get(0);
			if (nextLines.size() == 2) {
				if (nextInfo.point2.equals(pt)) {
					nextInfo = nextLines.get(1);
				}
			} else if (nextLines.size() != 1) {
				System.out.println("Something went wrong building edges");
			}
			pt = end;
			end = nextInfo.point2;
			nextInAndOut = pointMap.get(end);
			nextLines = nextInAndOut.get(0);
		}
		toReturn.add(end);

		return toReturn;
	}

	// Find all the intersections. Intersections are either dead ends
	// (1 road in and 1 road out, which are the reverse of each other)
	// or intersections between two different roads, or where three
	// or more segments of the same road meet.
	private static List<GeoPoint> findIntersections(HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> pointMap) {
		// Now find the intersections. These are roads that do not have
		// Exactly 1 or 2 roads coming in and out, where the roads in
		// match the roads out.
		List<GeoPoint> intersections = new LinkedList<GeoPoint>();
		for (GeoPoint pt : pointMap.keySet()) {
			List<LinkedList<RoadLineInfo>> roadsInAndOut = pointMap.get(pt);
			LinkedList<RoadLineInfo> roadsOut = roadsInAndOut.get(0);
			LinkedList<RoadLineInfo> roadsIn = roadsInAndOut.get(1);

			boolean isNode = true;

			if (roadsIn.size() == 1 && roadsOut.size() == 1) {
				// If these are the reverse of each other, then this is
				// and intersection (dead end)
				if (!(roadsIn.get(0).point1.equals(roadsOut.get(0).point2)
						&& roadsIn.get(0).point2.equals(roadsOut.get(0).point1))
						&& roadsIn.get(0).roadName.equals(roadsOut.get(0).roadName)) {
					isNode = false;
				}
			}
			if (roadsIn.size() == 2 && roadsOut.size() == 2) {
				// If all the road segments have the same name,
				// And there are two pairs of reversed nodes, then
				// this is not an intersection because the roads pass
				// through.

				String name = roadsIn.get(0).roadName;
				boolean sameName = true;
				for (RoadLineInfo info : roadsIn) {
					if (!info.roadName.equals(name)) {
						sameName = false;
					}
				}
				for (RoadLineInfo info : roadsOut) {
					if (!info.roadName.equals(name)) {
						sameName = false;
					}
				}

				RoadLineInfo in1 = roadsIn.get(0);
				RoadLineInfo in2 = roadsIn.get(1);
				RoadLineInfo out1 = roadsOut.get(0);
				RoadLineInfo out2 = roadsOut.get(1);

				boolean passThrough = false;
				if ((in1.isReverse(out1) && in2.isReverse(out2)) || (in1.isReverse(out2) && in2.isReverse(out1))) {

					passThrough = true;
				}

				if (sameName && passThrough) {
					isNode = false;
				}

			}
			if (isNode) {
				intersections.add(pt);
			}
		}
		return intersections;
	}

	private static HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> buildPointMapOneWay(
			List<Triplet<GeoPoint, GeoPoint, String>> pathList) {

		HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> pointMap = new HashMap<>();
		for (Triplet<GeoPoint, GeoPoint, String> segment : pathList) {
			RoadLineInfo line = new RoadLineInfo(segment.getValue0(), segment.getValue1(), segment.getValue2(), "path");
			addToPointsMapOneWay(line, pointMap);
		}

		return pointMap;
	}

	// Add the next line read from the file to the points map.
	private static void addToPointsMapOneWay(RoadLineInfo line, HashMap<GeoPoint, List<LinkedList<RoadLineInfo>>> map) {
		List<LinkedList<RoadLineInfo>> pt1Infos = map.get(line.point1);
		if (pt1Infos == null) {
			pt1Infos = new ArrayList<LinkedList<RoadLineInfo>>();
			pt1Infos.add(new LinkedList<RoadLineInfo>());
			pt1Infos.add(new LinkedList<RoadLineInfo>());
			map.put(line.point1, pt1Infos);
		}
		List<RoadLineInfo> outgoing = pt1Infos.get(0);
		outgoing.add(line);

		List<LinkedList<RoadLineInfo>> pt2Infos = map.get(line.point2);
		if (pt2Infos == null) {
			pt2Infos = new ArrayList<LinkedList<RoadLineInfo>>();
			pt2Infos.add(new LinkedList<RoadLineInfo>());
			pt2Infos.add(new LinkedList<RoadLineInfo>());
			map.put(line.point2, pt2Infos);
		}
		List<RoadLineInfo> incoming = pt2Infos.get(1);
		incoming.add(line);

	}
}
