package at.greenhive.bl.util.math.geo3D;

public class Matrix4 {
	public double X[][]; // first index is row, second index is column

	public Matrix4() {

		X = new double[4][];
		for (int i = 0; i < 4; i++) {
			X[i] = new double[4];
		}
	}

	/** Create a new Matrix4 by explicitly setting its components. */

	public Matrix4(double x11, double x12, double x13, double x14, double x21,
			double x22, double x23, double x24, double x31, double x32,
			double x33, double x34, double x41, double x42, double x43,
			double x44) {
		X = new double[4][];
		for (int i = 0; i < 4; i++) {
			X[i] = new double[4];
		}
		X[0][0] = x11;
		X[0][1] = x12;
		X[0][2] = x13;
		X[0][3] = x14;
		X[1][0] = x21;
		X[1][1] = x22;
		X[1][2] = x23;
		X[1][3] = x24;
		X[2][0] = x31;
		X[2][1] = x32;
		X[2][2] = x33;
		X[2][3] = x34;
		X[3][0] = x41;
		X[3][1] = x42;
		X[3][2] = x43;
		X[3][3] = x44;

	}

	public void clear() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				X[i][j] = 0.0;
			}
		}
	}

	public Matrix4 copy() {
		Matrix4 ret = new Matrix4();

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				ret.X[i][j] = X[i][j];
			}
		}

		return ret;
	}

	public Matrix4 transpose() {
		Matrix4 ret = new Matrix4();

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				ret.X[j][i] = X[i][j];
			}
		}

		return ret;
	}

	/**
	 * returns the subdeterminant composed by the given rows and colums, \f$
	 * X_{r_0,c_0} \left( X_{r_1,c_1} X_{r_2,c_2} - X_{r_2,c_1} X_{r_1,c_2}
	 * \right) - X_{r_0,c_1} \left( X_{r_1,c_0} X_{r_2,c_2} - X_{r_2,c_0}
	 * X_{r_1,c_2} \right) + X_{r_0,c_2} \left( X_{r_1,c_0} X_{r_2,c_1} -
	 * X_{r_2,c_0} X_{r_1,c_1} \right) \f$
	 */
	protected double subDeterminant(int r0, int r1, int r2, int c0, int c1,
			int c2) {
		return X[r0][c0] * (X[r1][c1] * X[r2][c2] - X[r2][c1] * X[r1][c2])
				- X[r0][c1] * (X[r1][c0] * X[r2][c2] - X[r2][c0] * X[r1][c2])
				+ X[r0][c2] * (X[r1][c0] * X[r2][c1] - X[r2][c0] * X[r1][c1]);
	}

	/**
	 * returns the determinant of the matirx, \f$ \det A \f$
	 */
	public double det() {
		return X[0][0] * subDeterminant(1, 2, 3, 1, 2, 3) - X[0][1]
				* subDeterminant(1, 2, 3, 0, 2, 3) + X[0][2]
				* subDeterminant(1, 2, 3, 0, 1, 3) - X[0][3]
				* subDeterminant(1, 2, 3, 0, 1, 2);
	}

	Matrix4 adjoint() {
		Matrix4 a = new Matrix4();

		a.X[0][0] = subDeterminant(1, 2, 3, 1, 2, 3);
		a.X[0][1] = -subDeterminant(0, 2, 3, 1, 2, 3);
		a.X[0][2] = subDeterminant(0, 1, 3, 1, 2, 3);
		a.X[0][3] = -subDeterminant(0, 1, 2, 1, 2, 3);

		a.X[1][0] = -subDeterminant(1, 2, 3, 0, 2, 3);
		a.X[1][1] = subDeterminant(0, 2, 3, 0, 2, 3);
		a.X[1][2] = -subDeterminant(0, 1, 3, 0, 2, 3);
		a.X[1][3] = subDeterminant(0, 1, 2, 0, 2, 3);

		a.X[2][0] = subDeterminant(1, 2, 3, 0, 1, 3);
		a.X[2][1] = -subDeterminant(0, 2, 3, 0, 1, 3);
		a.X[2][2] = subDeterminant(0, 1, 3, 0, 1, 3);
		a.X[2][3] = -subDeterminant(0, 1, 2, 0, 1, 3);

		a.X[3][0] = -subDeterminant(1, 2, 3, 0, 1, 2);
		a.X[3][1] = subDeterminant(0, 2, 3, 0, 1, 2);
		a.X[3][2] = -subDeterminant(0, 1, 3, 0, 1, 2);
		a.X[3][3] = subDeterminant(0, 1, 2, 0, 1, 2);

		return a;
	}

	/**
	 * returns the inverse matrix, \f$ A^{-1} \f$
	 */
	public Matrix4 invert() {
		Matrix4 a;
		double invDet = 1.0 / det();

		a = adjoint();

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				a.X[i][j] *= invDet;
			}
		}

		return a;
	}

	public String[] toStringArray() {
		String[] ret = new String[4];

		for (int i = 0; i < 4; i++) {
			ret[i] = "(" + X[i][0] + ", " + X[i][1] + ", " + X[i][2] + ", "
					+ X[i][3] + ")";
		}

		return ret;
	}

	public double[] getOpenGLMatrix() {
		double[] openGLMatrix = new double[16];
		int index = 0;

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				openGLMatrix[index] = X[j][i];
				index++;
			}
		}

		return openGLMatrix;
	}
	
	public float[] getOpenGLMatrixf() {
		float[] openGLMatrix = new float[16];
		int index = 0;

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				openGLMatrix[index] = (float)X[j][i];
				index++;
			}
		}

		return openGLMatrix;
	}

	public void setFromOpenGLMatrix(double[] openGLMatrix) {
		int index = 0;

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				X[j][i] = openGLMatrix[index];
				index++;
			}
		}
	}

	public boolean equals(Matrix4 a) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (X[i][j] != a.X[i][j]) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Multiplies this matrix with an other 4x4 matrix
	 * @param a the matrix to multiply with
	 * @return the resulting matrix
	 */
	public Matrix4 multiply(Matrix4 a) {
		Matrix4 ret = new Matrix4();

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				ret.X[i][j] = X[i][0] * a.X[0][j] + X[i][1] * a.X[1][j]
						+ X[i][2] * a.X[2][j] + X[i][3] * a.X[3][j];
			}
		}
		return ret;
	}

	/**
	 * Multiplies a 3D vector with this 4x4 matrix by adding 1 as a fourth component.
	 * @param a the vector to multiply with
	 * @return the result vector with the omitted fourth component scaled back to 1
	 */
	public Vector3D multiply(Vector3D a) {
		Vector3D res = new Vector3D();
		double res4;

		res4 = X[3][0] * a.val[0] + X[3][1] * a.val[1] + X[3][2] * a.val[2] + X[3][3];
		for (int i = 0; i < 3; i++) {
			res.val[i] = X[i][0] * a.val[0] + X[i][1] * a.val[1] + X[i][2] * a.val[2]
					+ X[i][3];
			res.val[i] /= res4;
		}

		return res;
	}

	/** This method is identical to times(), except that v is assumed to be a direction vector.
    That is, the implicit fourth element is assumed to be 0. */

	public final Vector3D multiplyDirection(Vector3D v){
		return new Vector3D(X[0][0]*v.val[0] + X[0][1]*v.val[1] + X[0][2]*v.val[2],
			            X[1][0]*v.val[0] + X[1][1]*v.val[1] + X[1][2]*v.val[2],
				    X[2][0]*v.val[0] + X[2][1]*v.val[1] + X[2][2]*v.val[2]);
	}
	
	/**
	 * Multiplies this matrix with a scalar value
	 * @param a the scalar to multiply with
	 * @return the resulting matrix
	 */
	public Matrix4 multiply(double a) {
		Matrix4 ret = new Matrix4();

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				ret.X[i][j] = X[i][j] * a;
			}
		}
		return ret;
	}

	/** Create a matrix for transforming from world coordinates to viewing coordinates.  This
    matrix transforms coordinates such that the position vector orig is translated to
    the origin, the direction vector zdir lies along the positive z axis, and the
    direction vection updir lies the positive y half of the yz plane. */

public static Matrix4 viewTransform(Vector3D orig, Vector3D zdir, Vector3D updir)
{
	Vector3D rx, ry, rz;

  rz = zdir.multiply(1.0/zdir.abs());
  rx = updir.crossMultiply(zdir);
  rx.normalise();
  ry = rz.crossMultiply(rx);
  return new Matrix4(rx.val[0], rx.val[1], rx.val[2], -(rx.val[0]*orig.val[0] + rx.val[1]*orig.val[1] + rx.val[2]*orig.val[2]),
		  			 ry.val[0], ry.val[1], ry.val[2], -(ry.val[0]*orig.val[0] + ry.val[1]*orig.val[1] + ry.val[2]*orig.val[2]),
		  			 rz.val[0], rz.val[1], rz.val[2], -(rz.val[0]*orig.val[0] + rz.val[1]*orig.val[1] + rz.val[2]*orig.val[2]),
		  			 0.0, 0.0, 0.0, 1.0);
}

/** Create a matrix which is the inverse of the viewTransform matrix.  That is, it first
    rotates the rotates the z axis to lie along the direction of zdir and the y axis to
    lie in the updir direction, then translates the origin to the point orig.  This is
    useful for transforming from object coordinates to world coordinates. */

public static Matrix4 objectTransform(Vector3D orig, Vector3D zdir, Vector3D updir)
{
	Vector3D rx, ry, rz;

  rz = zdir.multiply(1.0/zdir.abs());
  rx = updir.crossMultiply(zdir);
  rx.normalise();
  ry = rz.crossMultiply(rx);
  return new Matrix4(rx.val[0], ry.val[0], rz.val[0], orig.val[0],
		     rx.val[1], ry.val[1], rz.val[1], orig.val[1],
		     rx.val[2], ry.val[2], rz.val[2], orig.val[2],
		     0.0, 0.0, 0.0, 1.0);
}

/** Create a matrix to implement a perspective projection.  The center of projection is
    at (0, 0, -d), and the projection plane is given by z=1. */

public static Matrix4 perspective(double d)
{
  double e33 = 1.0/((d+1.0)*(d+1.0));

  return new Matrix4(1.0, 0.0, 0.0, 0.0,
		  			0.0, 1.0, 0.0, 0.0,
		  			0.0, 0.0, e33, 1.0-e33,
		  			0.0, 0.0, e33, 1.0-e33);
}
	
	
	/** Create an identity matrix. */

	public static Matrix4 identity() {
		return new Matrix4( 1.0, 0.0, 0.0, 0.0,
							0.0, 1.0, 0.0, 0.0,
							0.0, 0.0, 1.0, 0.0, 
							0.0, 0.0, 0.0, 1.0);
	}

	/** Create a matrix to scale x, y, and z by sx, sy, and sz respectively. */

	public static Matrix4 scale(double sx, double sy, double sz) {
		return new Matrix4( sx, 0.0, 0.0, 0.0,
							0.0, sy, 0.0, 0.0,
							0.0, 0.0, sz, 0.0,
							0.0, 0.0, 0.0, 1.0);
	}

	/** Create a matrix to translate a vector by (dx, dy, dz). */

	public static Matrix4 translation(double dx, double dy, double dz) {
		return new Matrix4( 1.0, 0.0, 0.0, dx,
							0.0, 1.0, 0.0, dy,
							0.0, 0.0, 1.0, dz,
							0.0, 0.0, 0.0, 1.0);
	}
	
	/** Create a matrix to translate a vector by (dx, dy, dz). */

	public static Matrix4 translation(Vector3D vector) {
		return Matrix4.translation(vector.val[0],vector.val[1],vector.val[2]);
	}

	/** Create a matrix that rotates a vector around the X axis. */

	public static Matrix4 xrotation(double angle) {
		double c = Math.cos(angle), s = Math.sin(angle);

		return new Matrix4( 1.0, 0.0, 0.0, 0.0,
							0.0, c, -s, 0.0,
							0.0, s, c, 0.0,
							0.0, 0.0, 0.0, 1.0);
	}

	/** Create a matrix that rotates a vector around the Y axis. */

	public static Matrix4 yrotation(double angle) {
		double c = Math.cos(angle), s = Math.sin(angle);

		return new Matrix4(c, 0.0, s, 0.0, 0.0, 1.0, 0.0, 0.0, -s, 0.0, c, 0.0,
				0.0, 0.0, 0.0, 1.0);
	}

	/** Create a matrix that rotates a vector around the Z axis. */

	public static Matrix4 zrotation(double angle) {
		double c = Math.cos(angle), s = Math.sin(angle);

		return new Matrix4(c, -s, 0.0, 0.0, s, c, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
				0.0, 0.0, 0.0, 1.0);
	}

	/** This routine creates a matrix to rotate a vector around an arbitrary axis. */

	public static Matrix4 axisRotation(Vector3D axis, double angle) {
		double c = Math.cos(angle), s = Math.sin(angle);
		double t = 1.0 - c;

		return new Matrix4(t * axis.val[0] * axis.val[0]+ c, t * axis.val[0] * axis.val[1] - s
				* axis.val[2], t * axis.val[0] * axis.val[2] + s * axis.val[1], 0.0, t * axis.val[0]
				* axis.val[1] + s * axis.val[2], t * axis.val[1] * axis.val[1] + c, t * axis.val[1]
				* axis.val[2] - s * axis.val[0], 0.0, t * axis.val[0] * axis.val[2] - s * axis.val[1], t
				* axis.val[1] * axis.val[2] + s * axis.val[0], t * axis.val[2] * axis.val[2] + c, 0.0,
				0.0, 0.0, 0.0, 1.0);
	}
}
