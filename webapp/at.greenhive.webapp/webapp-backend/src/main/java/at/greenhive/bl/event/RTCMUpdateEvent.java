package at.greenhive.bl.event;

import me.limespace.appbase.event.IEvent;

public class RTCMUpdateEvent implements IEvent {

	private byte[] data;

	public RTCMUpdateEvent(byte[] data) {
		this.data = data;
	}
	
	public byte[] getData() {
		return data;
	}

}
