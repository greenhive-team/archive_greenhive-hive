package at.greenhive.thingy.operation;

@FunctionalInterface
public interface OperationManagerProvider {
	OperationManager get();
}
