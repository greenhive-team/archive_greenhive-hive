package at.greenhive.bl.airtrafficcontroller;

import at.greenhive.thingy.swarm.event.DroneEvent;

public class DroneTakeoffStatusUpdateEvent extends DroneEvent{

	private boolean ready;

	public DroneTakeoffStatusUpdateEvent(String droneId, boolean ready) {
		super(droneId);
		this.ready = ready;
	}
	
	public boolean isReady() {
		return ready;
	}

}
