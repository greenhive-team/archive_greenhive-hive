package at.greenhive.thingy.operation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import at.greenhive.thingy.missionplaner.WorkCoordinate;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.WorkPath;
import at.greenhive.thingy.missionplaner.mission.OperationData;
import at.greenhive.thingy.missionplaner.mission.OperationMetaData;

@Repository
public class OperationRepo {
	
	private RowMapper<OperationMetaData> croplandMetadataRowMapper = (rs, rownum) -> {
		OperationMetaData missionMeta = new OperationMetaData(rs.getInt("oid"));

		missionMeta.setCroplandId(rs.getInt("clid"));
		missionMeta.setName(rs.getString("name"));
		missionMeta.setQuantity(rs.getInt("quantity"));
		return missionMeta;
	};
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public boolean saveWorkPatch(int operationid, WorkPatch workpatch) {
		boolean rc = true;
		
		return rc;
	}
	
	public OperationData createOperation(OperationData operationData) {
		if (operationData.getOperationID()  > 0 ) {
			throw new IllegalArgumentException("Operation ID:" + operationData.getOperationID() + " is already in repo");
		}
		
		return operationData;
	}
	
	public int deleteAllOperations() {
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		String sql = "DELETE from operation ";
		return jdbcTemplate.update(sql, namedParameters);
	}
	
	
	public WorkPatch loadWorkPatch(int operationid) {

		String sql = "select pathid, x, y, z  from OPERATION_PATH where oid = :operationid order by pathid,pointid";
		Map<String, Integer> namedParameters = Collections.singletonMap("operationid", operationid);
		
		SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, namedParameters);
		
		WorkPatch patch = new WorkPatch();
		
		WorkPath path = null;
		int oldPathId = Integer.MIN_VALUE;

		while (rs.next()) {
						
			int pathid = rs.getInt("pathid");
			//int pointid = rs.getInt("pointid");
			
			int x = rs.getInt("x");
			int y = rs.getInt("y");
			int z = rs.getInt("z");
			
			if (pathid != oldPathId) {
				oldPathId = pathid;
				if (path != null) {
					patch.addPath(path);
				}
				path = new WorkPath(pathid);
			}
			
			path.addPathPoint(new WorkCoordinate(x,y,z));
		}
		
		if (path != null) {
			patch.addPath(path);
		}

		return patch;
	}

	public int[] insertOperationMetaList(List<OperationMetaData> operationMetaList) {
		
		Map<String, ?>[] parms = createParameterMap(operationMetaList);
		
		String sql = "insert into operation (oid ,	clid, 	 quantity,	name ) values (:oid, :clid, :quantity, :name)";
		int[] updateCounts = jdbcTemplate.batchUpdate(sql,parms);
		return updateCounts;
	}
	
	private Map<String, ?>[] createParameterMap(List<OperationMetaData> operationMetaList) {
		@SuppressWarnings("unchecked")
		Map<String,?>[] parms = new HashMap[operationMetaList.size()];
		
		int ix = 0;
		for (OperationMetaData operation :operationMetaList) {
			HashMap<String,Object> param = new  HashMap<String, Object>(); 
			parms[ix++] = param;
			param.put("oid", operation. getOperationId());
			param.put("clid", operation.getCroplandId());
			param.put("quantity", operation.getQantity());
			param.put("name", operation.getName());
		}
		return parms;
	}
	
	public Collection<OperationMetaData> loadOperationMetaList() {
		String sql = "select  oid , clid ,quantity, name  from operation";
		
		List<OperationMetaData> operationMetaList = jdbcTemplate.query(sql, croplandMetadataRowMapper);
		return operationMetaList;
	}
	
	public OperationData loadOperation(String id) {
		return new OperationData(Integer.parseInt(id));
		
	}

	

}