package at.greenhive.thingy.missionplaner.segmentation;

import java.util.Collections;
import java.util.List;

import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;

public class HorizontalRowsAnt extends SegmentationStrategy {

	public HorizontalRowsAnt(OperationCroplandParameter croplandMissionParm, OperationParameter missionParameter) {
		super(croplandMissionParm);
	}
	
	@Override
	public List<IWorkSegment> createTransit(WorkPatch patch) {
		return Collections.emptyList();
	}

	@Override
	public List<IWorkSegment> createSegments(WorkPatch patch) {
		// TODO Auto-generated method stub
		return null;
	}

}
