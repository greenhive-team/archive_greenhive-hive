// arch-tag: b1eef08e-7c4b-4780-9690-abf7396a7013
/*
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at 
 * http://www.mozilla.org/MPL/ 
 *
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specificlanguage governing rights and limitations under the License. 
 *
 * The Initial Developer of the Original Code is Gregor Mueckl. 
 * All Rights Reserved.
 *
 * Created on Jan 5, 2005
 */
package at.greenhive.bl.util.math.geo3D;

public class Matrix3 {
    public double X[][];
  
    public Matrix3()
    {
        X=new double[3][];
        for(int i=0;i<3;i++) {
            X[i]=new double[3];
        }
    }
  
    public void clear()
    {
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                X[i][j]=0.0;
            }
        }
    }
    
    public Matrix3 copy()
    {
        Matrix3 ret=new Matrix3();
        
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                ret.X[i][j]=X[i][j];
            }
        }
        
        return ret;
    }
    
    public Matrix3 transpose()
    {
        Matrix3 ret=new Matrix3();
        
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                ret.X[j][i]=X[i][j];
            }
        }
        
        return ret;        
    }
    
    public Matrix3 invert()
    {
        Matrix3 a=new Matrix3();
        double det, invDet;

        a.X[0][0]=X[1][1]*X[2][2] - X[1][2]*X[2][1];
        a.X[0][1]=X[0][2]*X[2][1] - X[0][1]*X[2][2];
        a.X[0][2]=X[0][1]*X[1][2] - X[0][2]*X[1][1];
        
        a.X[1][0]=X[1][2]*X[2][0] - X[1][0]*X[2][2];
        a.X[1][1]=X[0][0]*X[2][2] - X[0][2]*X[2][0];
        a.X[1][2]=X[0][2]*X[1][0] - X[0][0]*X[1][2];
        
        a.X[2][0]=X[1][0]*X[2][1] - X[1][1]*X[2][0];
        a.X[2][1]=X[0][1]*X[2][0] - X[0][0]*X[2][1];
        a.X[2][2]=X[0][0]*X[1][1] - X[0][1]*X[1][0];
        
        det=X[0][0]*a.X[0][0] + X[0][1]*a.X[1][0] + X[0][2]*a.X[2][0];
        invDet=1.0/det;
        for(int i=0;i<3;i++) {
          for(int j=0;j<3;j++) {
            a.X[i][j]*=invDet;
          }
        }
        
        return a;        
    }
    
    public double det()
    {
        return X[0][0]*X[1][1]*X[2][2] + X[0][1]*X[1][2]*X[2][0] + X[0][2]*X[1][0]*X[2][1]
               - X[0][2]*X[1][1]*X[2][0] - X[0][1]*X[1][0]*X[2][2] - X[0][0]*X[1][2]*X[2][1];       
    }
    
    public boolean equals(Matrix3 a) {
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                if(X[i][j]!=a.X[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public Matrix3 add(Matrix3 a) 
    {
        Matrix3 ret=new Matrix3();
        
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                ret.X[i][j]=X[i][j]+a.X[i][j];
            }
        }
        
        return ret;
    }

    public Matrix3 sub(Matrix3 a) 
    {
        Matrix3 ret=new Matrix3();
        
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                ret.X[i][j]=X[i][j]-a.X[i][j];
            }
        }
        
        return ret;
    }
    
    public Matrix3 multiply(Matrix3 a)
    {
        Matrix3 ret=new Matrix3();
        
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
              ret.X[i][j]=X[i][0]*a.X[0][j]+X[i][1]*a.X[1][j]+X[i][2]*a.X[2][j];
            }
        }
        return ret;
    }

    /**
     * @param a
     * @return
     */
    public Vector3D multiply(Vector3D a) {
        Vector3D res = new Vector3D();
        
        for(int i=0;i<3;i++) {
            res.val[i]=X[i][0]*a.val[0]+X[i][0]*a.val[1]+X[i][2]*a.val[2];
        }
        
        return res;
    }
    
    public Matrix3 multiply(double a) {
        Matrix3 ret=new Matrix3();
        
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                ret.X[i][j]=X[i][j]*a;
            }
        }
        return ret;
    }
}
