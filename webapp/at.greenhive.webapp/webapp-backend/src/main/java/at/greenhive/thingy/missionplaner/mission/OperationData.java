package at.greenhive.thingy.missionplaner.mission;

import java.util.Collection;
import java.util.List;

import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.thingy.missionplaner.WorkPath;
import gnu.trove.map.hash.TIntObjectHashMap;

public class OperationData {
	
	private int operationID = Integer.MIN_VALUE;
	GeoPoint hiveLocation;
	
	TIntObjectHashMap<MissionTask> tasks;
	TIntObjectHashMap<MissionTaskState> taskStates;
	private List<WorkPath> transitPaths; 
	
	public OperationData(int operationID) {
		tasks = new TIntObjectHashMap<>();
		taskStates = new TIntObjectHashMap<>();
	}
	
	public void setOperationID(int operationID) {
		this.operationID = operationID;
	}
	
	public int getOperationID() {
		return operationID;
	}
	
	/**
	 * 
	 * @param missionTask
	 * @return the previous task associated with this taskid or NULL if the task was new
	 */
	public MissionTask addTask(MissionTask missionTask) {
		return tasks.put(missionTask.getTaskId(), missionTask);
	}
	
	/**
	 * 
	 * @param missionTaskState
	 * @return the previous state associated with this task or NULL if the state was new
	 */
	public MissionTaskState addTaskState(MissionTaskState missionTaskState) {
		return taskStates.put(missionTaskState.getTaskId(), missionTaskState);
	}
	
	public Collection<MissionTaskState> getTaskStates() {
		return taskStates.valueCollection();
	}

	public MissionTaskState getTaskState(int taskId) {
		return taskStates.get(taskId);
	}
	
	public Collection<MissionTask> getTasks() {
		return tasks.valueCollection();
	}
	
	public MissionTask getTask(int taskId) {
		return tasks.get(taskId);
	}

	public GeoPoint getHiveLocation() {
		return hiveLocation;
	}

	public void setTransitPaths(List<WorkPath> transitPaths) {
		this.transitPaths = transitPaths;
	}

	public List<WorkPath> getTransitPaths() {
		return transitPaths;
	}
}

