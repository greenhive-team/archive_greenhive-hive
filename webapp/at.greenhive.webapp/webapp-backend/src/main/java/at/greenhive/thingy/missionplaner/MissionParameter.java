package at.greenhive.thingy.missionplaner;

import at.greenhive.bl.util.geo.GeoPoint;

public class MissionParameter {
	
	GeoPoint hiveLocation;
	
	public int timeToRefill;
	public int concurrentRefillCount;
	public int droheAirTimeMax;
	public int sprayDistanceMax;
	public int spraySpeed;
	public int sprayHeight;
	public int travelSpeed;
	public int travelHeight;

	public MissionParameter() {
		
	}
}