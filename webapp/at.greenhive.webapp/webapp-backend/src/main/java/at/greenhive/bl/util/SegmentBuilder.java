package at.greenhive.bl.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;

public class SegmentBuilder<R, T> {

	List<T> entries = new ArrayList<>();

	public void addEntry(T entry) {
		entries.add(entry);
	}

	public List<R> buildSegments(BiFunction<T, T, R> create) {
		List<R> r = new ArrayList<>();
		if (entries.isEmpty()) {
			return Collections.emptyList();
		}
		Iterator<T> iterator = entries.iterator();
		T first = iterator.next();
		while (iterator.hasNext()) {
			T second = iterator.next();
			R ob = create.apply(first,second);
			r.add(ob);
		}
		return r;
	}
}
