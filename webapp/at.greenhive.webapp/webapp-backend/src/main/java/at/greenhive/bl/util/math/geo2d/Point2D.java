package at.greenhive.bl.util.math.geo2d;

import java.util.Locale;

public class Point2D {

	public double x;
	public double y;

	public Point2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return String.format(Locale.ENGLISH,"(%f,%f)", x, y);
	}
	
	public double dot(Point2D other) {
		return x *other.x  + y * other.y;
	}
}
