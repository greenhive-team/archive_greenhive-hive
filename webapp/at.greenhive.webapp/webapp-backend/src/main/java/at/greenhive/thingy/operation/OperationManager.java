package at.greenhive.thingy.operation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import at.greenhive.bl.airtrafficcontroller.DroneTakeoffStatusUpdateEvent;
import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.event.RTCMUpdateEvent;
import at.greenhive.bl.hive.HiveService;
import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.thingy.cropland.Cropland;
import at.greenhive.thingy.missionplaner.HiveCoordinateConverter;
import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.MissionPlannerEngine;
import at.greenhive.thingy.missionplaner.OperationController;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.mission.IllegalTaskStateException;
import at.greenhive.thingy.missionplaner.mission.MissionData;
import at.greenhive.thingy.missionplaner.mission.MissionSprayTask;
import at.greenhive.thingy.missionplaner.mission.MissionTask;
import at.greenhive.thingy.missionplaner.mission.MissionTaskState;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationData;
import at.greenhive.thingy.missionplaner.mission.OperationMetaData;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;
import at.greenhive.thingy.missionplaner.mission.enTaskState;
import at.greenhive.thingy.operation.OperationDataNotCompleteException.Reason;
import at.greenhive.thingy.operation.OperationState.enOperationState;
import at.greenhive.thingy.swarm.Drone;
import at.greenhive.thingy.swarm.DroneNewState;
import at.greenhive.thingy.swarm.Swarm;
import at.greenhive.thingy.swarm.event.DroneGoingToHiveEvent;
import at.greenhive.thingy.swarm.event.DroneMissionAckEvent;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.thingy.swarm.event.DroneTaskCompleteEvent;
import gnu.trove.TIntCollection;
import gnu.trove.list.array.TIntArrayList;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.PolygonArea;
import net.sf.geographiclib.PolygonResult;

public class OperationManager implements IEventListener, IEventSource {

	private enum enDroneState {
		IDLE, WAITING_MISSION_ACK, WAITING_TAKEOFF_CHECK, ON_MISSION;
	}

	private enum enDroneMessage {
		MISSION_ACK, READY_FOR_TAKEOFF;
	}

	private Deque<OperationState> operationStates = new LinkedList<>();

	private OperationParameter operationParameter = new OperationParameter();
	private MissionPlannerEngine plannerEngine = new MissionPlannerEngine();

	private HiveCoordinateConverter converter;

	private Cropland cropland;
	private OperationCroplandParameter operationCroplandParm;
	private OperationData operationData;

	private Collection<MissionData> plannedMissions;

	private Set<MissionData> assignedMissions;
	private Set<MissionData> complededMissions;

	private Map<String, enDroneState> droneStates;

	private Swarm swarm;

	public void addOperation(OperationMetaData operation, Cropland cropland) {

		if (isRunning()) {
			throw new IllegalStateException("Operation is currently in progess");
		}

		Iterator<GeoPoint> iterator = cropland.getOutline().iterator();
		GeoPoint origin = iterator.next();
		GeoPoint zPoint = iterator.next();

		converter = new HiveCoordinateConverter(origin, zPoint);

		this.cropland = cropland;
		this.operationCroplandParm = new OperationCroplandParameter(LocalDate.now(), operation.getQantity(),
				cropland.getMetadata().getDefaultSprayHeight(), cropland.getMetadata().getMaxSpeed()); // TODO: user
																										// should be
																										// able to
																										// overwrite
																										// this per
																										// mission
		this.operationData = createOperationData();
		assignedMissions = new HashSet<>();
		complededMissions = new HashSet<>();

		droneStates = new HashMap<>();
		operationStates = new LinkedList<>();

		plannedMissions = createMissionData();

		GlobalEventManager.fire(new OperationUpdateEvent(), this);
	}

	public void updateSwarm(Swarm swarm) {
		if (isRunning()) {
			throw new IllegalStateException("Operation is currently in progess");
		}

		this.swarm = swarm;
		operationData = createOperationData();
		plannedMissions = createMissionData();
		GlobalEventManager.fire(new OperationUpdateEvent(), this);
	}

	public Swarm getSwarm() {
		return swarm;
	}

	private void setOperationState(enOperationState newState) {
		if (CollectionUtils.isEmpty(operationStates) || operationStates.peek().getState() != newState) {
			operationStates.push(new OperationState(newState));
			GlobalEventManager.fire(new OperationStateChanged(operationStates.peek()), this);
		}
	}

	public boolean isRunning() {
		if (CollectionUtils.isEmpty(operationStates)) {
			return false;
		}
		enOperationState state = operationStates.getLast().getState();

		return state == enOperationState.IDLE || state == enOperationState.WAITING_FOR_SWARM
				|| state == enOperationState.RUNNING;
	}

	public boolean isReadyForStart() {
		if (plannedMissions == null) {
			return false;
		}
		if (swarm == null || CollectionUtils.isEmpty(swarm.getDrones())) {
			return false;
		}
		if (isRunning()) {
			return false;
		}
		return true;
	}

	public boolean startOperation() throws OperationDataNotCompleteException {
		if (isRunning()) {
			return false;
		}
		if (plannedMissions == null) {
			throw new OperationDataNotCompleteException(Reason.MISSING_MISSIONDATA);
		}
		if (swarm == null || CollectionUtils.isEmpty(swarm.getDrones())) {
			throw new OperationDataNotCompleteException(Reason.MISSING_SWARM);
		}
		enterStartedState();

		return true;
	}

	private void enterStartedState() {
		setOperationState(enOperationState.WAITING_FOR_SWARM);
		updateState();
	}

	private void updateState() {
		if (operationStates == null) {
			return;
		}
		switch (operationStates.peek().getState()) {
		case WAITING_FOR_SWARM:
			if (checkSwarmIdle()) {
				enterRunningState();
			}
			break;
		case RUNNING:
			dispatchMissions();
			break;
		default:
			break;
		}
	}

	private void enterRunningState() {
		setOperationState(enOperationState.RUNNING);
		dispatchMissions();
	}

	private void enterPausedState() {
		setOperationState(enOperationState.PAUSED);
		dispatchMissions();
	}

	private void dispatchMissions() {

		if (allMissionsCompleted()) {
			operationStates.add(new OperationState(enOperationState.FINISHED));
			GlobalEventManager.fire(new OperationStateChanged(operationStates.peek()), this);
			return;
		}

		for (MissionData missionData : plannedMissions) {
			if (!complededMissions.contains(missionData) && !assignedMissions.contains(missionData)) {
				Drone freeBee = findFreeDrone();
				if (freeBee == null) {
					return;
				}
				assignMission2Drone(freeBee, missionData);
			}
		}
	}

	private boolean allMissionsCompleted() {
		for (MissionData missionData : plannedMissions) {
			if (!complededMissions.contains(missionData)) {
				return false;
			}
		}
		return true;
	}

	private void assignMission2Drone(Drone freeBee, MissionData mission) {

		mission.updateDroneId(freeBee.getSerialNumber());
		assignedMissions.add(mission);
		OperationController c = new OperationController(HiveService.getHiveLocation());
		setDroneState(freeBee.getSerialNumber(), enDroneState.WAITING_MISSION_ACK);
		c.sendMissionData(mission, converter);

	}

	private void resendMission() {

	}

	private void sendTakeOffCheck(Drone bee) {
		OperationController c = new OperationController(HiveService.getHiveLocation());
		setDroneState(bee.getSerialNumber(), enDroneState.WAITING_TAKEOFF_CHECK);
		c.sendTakeOffCheck(bee.getSerialNumber());
	}

	private void sendTakeOff(Drone bee) {
		OperationController c = new OperationController(HiveService.getHiveLocation());
		setDroneState(bee.getSerialNumber(), enDroneState.ON_MISSION);
		c.sendTakeOff(bee.getSerialNumber());
		// TODO: check if drone switch back to IDLE -> 2 retry -> error status
	}

	private Drone findFreeDrone() {
		for (Drone bee : swarm.getDrones()) {
			if (bee.getState().isReadyForMission()) {
				if (!isDroneInUse(bee)) {
					return bee;
				}
			}
		}
		return null;
	}

	private boolean isDroneInUse(Drone bee) {
		for (MissionData missionData : plannedMissions) {
			if (!complededMissions.contains(missionData)) {
				for (MissionTaskState missionTaskState : missionData.getTaskStates()) {
					if (missionTaskState.getTaskState().isAssigned()
							&& missionTaskState.getDroneId().equals(bee.getSerialNumber())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean checkSwarmIdle() {
		boolean rc = true;
		for (Drone bee : swarm.getDrones()) {
			rc &= bee.getState().isIdle();
		}
		return rc;
	}

	private Collection<MissionData> createMissionData() {

		if (this.swarm == null || operationData == null || this.swarm.getDrones().isEmpty()) {
			return Collections.emptyList();
		}
		plannedMissions = new ArrayList<>();

		boolean tasksLeft = true;
		while (tasksLeft) {
			for (Drone drone : swarm.getDrones()) {
				int missionId = getNewMissionId();
				MissionData missionData = plannerEngine.getMissionData(missionId,
																		drone.getSerialNumber(),
																		drone.getCapacity(), 
																		drone.needsTransit(),
																		operationCroplandParm.getMaxSpeed(), 
																		operationData, 
																		converter,
																		HiveService.getHiveLocation());
				if (!missionData.getTasks().isEmpty()) {
					plannedMissions.add(missionData);
				} else {
					tasksLeft = false;
					continue;
				}
			}
		}
		return plannedMissions;
	}

	private int getNewMissionId() {

		int maxid = 0;

		for (MissionData m : complededMissions) {
			maxid = Math.max(maxid, m.getMissionId());
		}
		for (MissionData m : assignedMissions) {
			maxid = Math.max(maxid, m.getMissionId());
		}
		for (MissionData m : plannedMissions) {
			maxid = Math.max(maxid, m.getMissionId());
		}
		return maxid + 1;
	}

	public Collection<MissionData> getMissionData() {
		return plannedMissions;
	}

	private OperationData createOperationData() {

		if (cropland != null && operationCroplandParm != null) {
			WorkPatch workPatch = plannerEngine.createFromCropland(converter, cropland, operationCroplandParm);
			
			List<IWorkSegment> segments = plannerEngine.getSegments(workPatch, cropland.getMetadata().getCroptype(),
					operationCroplandParm, operationParameter);
			
			OperationData operationData = plannerEngine.createOperationData(converter, segments, workPatch.getTansitPaths(), operationCroplandParm,HiveService.getHiveLocation());
			
			return operationData;
		}
		return null;
	}

	public Deque<OperationState> getOperationStates() {
		return operationStates;
	}

	public Map<String, enDroneState> getDroneStates() {
		return droneStates;
	}

	public OperationData getOperationData() {
		return operationData;
	}

	public void startUp() {
		GlobalEventManager.registerListener(this);
	}

	public void shutDown() {
		GlobalEventManager.unregisterListener(this);
	}

	@Override
	public void notify(IEvent event, IEventSource source) {

		if (source == this) {
			return;
		}

		try {

			if (event instanceof DroneTaskCompleteEvent) {
				DroneTaskCompleteEvent tcE = (DroneTaskCompleteEvent) event;
				int taskId = tcE.getTaskId();
				operationData.getTaskState(taskId).setTaskState(enTaskState.FINISHED);
				updateState();
				updateMissionState();
				GlobalEventManager.fire(new OperationUpdateEvent(), this);

			}
			if (event instanceof DroneGoingToHiveEvent) {

				DroneGoingToHiveEvent dgh = (DroneGoingToHiveEvent) event;
				String droneId = dgh.getDroneId();
				updateMissions(droneId);
				updateMissionState();
				replanMissions();
				updateState();

				GlobalEventManager.fire(new OperationUpdateEvent(), this);

			} else if (event instanceof DroneNewState) {
				if (updateDroneState(((DroneNewState) event).getBee(), null)) {
					updateState();
				}

			} else if (event instanceof DroneMissionAckEvent) {
				String droneId = ((DroneMissionAckEvent) event).getDroneId();
				updateDroneState(swarm.getDrone(droneId), enDroneMessage.MISSION_ACK);

			} else if (event instanceof DroneTakeoffStatusUpdateEvent) {
				DroneTakeoffStatusUpdateEvent e = ((DroneTakeoffStatusUpdateEvent) event);
				if (e.isReady()) {
					String droneId = e.getDroneId();
					updateDroneState(swarm.getDrone(droneId), enDroneMessage.READY_FOR_TAKEOFF);
				}
			} else if (event instanceof DroneStatusUpdateEvent) {

			} else if (event instanceof RTCMUpdateEvent) {
				if (swarm != null) {
					OperationController c = new OperationController(HiveService.getHiveLocation());
					RTCMUpdateEvent rtcm  = (RTCMUpdateEvent)event;
					c.sendRTCMUpdate(swarm, rtcm.getData());
				}
			}

		} catch (IllegalTaskStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void replanMissions() {
		plannedMissions = createMissionData();
	}

	private void updateMissions(String droneId) {

		for (MissionData missionData : plannedMissions) {

			TIntCollection tasksToRemove = new TIntArrayList();
			for (MissionTaskState missionTaskState : missionData.getTaskStates()) {
				int taskId = missionTaskState.getTaskId();
				if (!missionTaskState.getTaskState().isFinishedSucessfull()
						&& missionTaskState.getTaskState().isAssigned()
						&& missionTaskState.getDroneId().equals(droneId)) {
					tasksToRemove.add(taskId);
					operationData.getTaskState(taskId).reset();
				} else {
					if (!missionTaskState.getTaskState().isAssigned()) {
						operationData.getTaskState(taskId).reset();
					}
				}
			}

			tasksToRemove.forEach(taskId -> {
				missionData.removeTask(taskId);
				return true;
			});
		}
	}

	private void updateMissionState() {
		for (MissionData md : plannedMissions) {
			if (isMissionComplete(md)) {
				assignedMissions.remove(md);
				complededMissions.add(md);
			}
		}
	}

	private boolean isMissionComplete(MissionData md) {
		for (MissionTaskState missionTaskState : md.getTaskStates()) {
			if (!missionTaskState.getTaskState().isFinishedSucessfull()) {
				return false;
			}
		}
		return true;
	}

	private void setDroneState(String droneId, enDroneState state) {
		droneStates.put(droneId, state);
	}

	private boolean updateDroneState(Drone bee, enDroneMessage beeMessage) {
		if (droneStates == null) {
			return false;
		}

		enDroneState currentState = droneStates.get(bee.getSerialNumber());
		if (currentState == null) {
			return false;
		}

		switch (currentState) {
		case WAITING_MISSION_ACK:
			if (beeMessage == enDroneMessage.MISSION_ACK) {
				sendTakeOffCheck(bee);
			} else {
				// if (waitCnt)
				// resendMission(bee.getSerialNumber());
			}
			break;
		case WAITING_TAKEOFF_CHECK:
			if (beeMessage == enDroneMessage.READY_FOR_TAKEOFF) {
				sendTakeOff(bee);
			}
		default:
			break;
		}

		return true;

	}

	public HiveCoordinateConverter getConverter() {
		return converter;
	}

	public OperationInfo getOperationInfo() {
		OperationInfo info = new OperationInfo();

		if (!CollectionUtils.isEmpty(plannedMissions)) {
			info.setNumMissions(plannedMissions.size());
		}

		if (operationData != null) {
			info.setNumTasks(operationData.getTasks().size());

			info.setEstTime(-1); // TODO:

			PolygonArea area = new PolygonArea(Geodesic.WGS84, false);
			for (GeoPoint pnt : cropland.getOutline()) {
				area.AddPoint(pnt.getLatitudeDecimalDegrees(), pnt.getLongitudeDecimalDegrees());
			}
			PolygonResult result = area.Compute();

			info.setSprayArea(Math.round(result.area));

			double estSprayAmount = 0;
			for (MissionTask task : operationData.getTasks()) {
				if (task instanceof MissionSprayTask) {
					estSprayAmount += ((MissionSprayTask) task).getSprayAmount();
				}
			}

			info.setEstSprayAmount(Math.round(estSprayAmount));

			int numFinishedTasks = 0;
			for (MissionTaskState state : operationData.getTaskStates()) {
				if (state.getTaskState().isFinishedSucessfull()) {
					numFinishedTasks++;
				}
			}

			info.setNumFinishedTasks(numFinishedTasks);
		}

		return info;
	}

	public void pauseOperation() {
		if (operationStates.peek().getState() == enOperationState.PAUSED) {
			setOperationState(enOperationState.RUNNING);
		} else {
			setOperationState(enOperationState.PAUSED);
		}

		updateState();
	}

	public void stopOperation() {
		OperationController c = new OperationController(HiveService.getHiveLocation());
		for (Drone bee : swarm.getDrones()) {
			c.sendAbortMission(bee.getSerialNumber());
		}

		setOperationState(enOperationState.ABORTED);
	}

	public void resetOperationManager() {
		this.operationData = null;
		this.plannedMissions = null;

		assignedMissions = new HashSet<>();
		complededMissions = new HashSet<>();

		droneStates = new HashMap<>();
		operationStates = new LinkedList<>();

		GlobalEventManager.fire(new OperationUpdateEvent(), this);
		GlobalEventManager.fire(new OperationStateChanged(null), this);
	}
}
