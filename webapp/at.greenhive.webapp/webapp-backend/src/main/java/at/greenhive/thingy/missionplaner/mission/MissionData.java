package at.greenhive.thingy.missionplaner.mission;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import at.greenhive.bl.util.geo.GeoPoint;
import gnu.trove.map.hash.TIntObjectHashMap;

public class MissionData {
	
	private final int missionId;
	private String droneId;
	
	GeoPoint hiveLocation;
	
	LinkedHashMap<Integer, MissionTask> tasks;
	LinkedHashMap<Integer, MissionTask> toHive;
	LinkedHashMap<Integer, MissionTask> toStart;
	
	TIntObjectHashMap<MissionTaskState> taskStates;
	
	private double maxSpeed;
	public MissionData(int missionId, String droneId, double maxSpeed) {
		this.missionId = missionId;
		this.droneId = droneId;
		
		//tasks = new TIntObjectHashMap<>();
		tasks = new LinkedHashMap<>();
		taskStates = new TIntObjectHashMap<>();
		
		toStart = new LinkedHashMap<>();
		toHive = new LinkedHashMap<>();
		this.maxSpeed = maxSpeed;
	}
	
	public int getMissionId() {
		return missionId;
	}
	
	public void updateDroneId(String droneId) {
		for (MissionTaskState state :taskStates.valueCollection()) {
			state.setDroneId(droneId);
		}
		this.droneId = droneId;
	}
	
	public String getDroneId() {
		return droneId;
	}
	
	public void addTask(MissionTask missionTask, MissionTaskState missionTaskState) {
		tasks.put(missionTask.getTaskId(), missionTask);
		taskStates.put(missionTaskState.getTaskId(), missionTaskState);
	}
	
	public Collection<MissionTaskState> getTaskStates() {
		return taskStates.valueCollection();
	}

	public MissionTaskState getTaskState(int taskId) {
		return taskStates.get(taskId);
	}
	
	public Collection<MissionTask> getTasks() {
		return tasks.values();
	}
	
	public Collection<MissionTask> getToStart() {
		return toStart.values();
	}
	
	public Collection<MissionTask> getToHive() {
		return toHive.values();
	}
	
	public MissionTask getFirstTask() {
		if (tasks.isEmpty()) {
			return null;
		}
		return tasks.entrySet().iterator().next().getValue();
	}
	
	public MissionTask getLastTask() {
		
		if (tasks.isEmpty()) {
			return null;
		}
		
		Iterator<Entry<Integer, MissionTask>> iterator = tasks.entrySet().iterator();
		
		Entry<Integer, MissionTask> lastElement = iterator.next(); 
		while (iterator.hasNext()) { 
			lastElement = iterator.next(); 
		}
		
		return lastElement.getValue();
	}
	
	public MissionTask getTask(int taskId) {
		MissionTask t = tasks.get(taskId);
		if (t==null) {
			t = toStart.get(taskId);
			if (t==null) {
				t = toHive.get(taskId);
			}
		}
		return t;
	}

	public GeoPoint getHiveLocation() {
		return hiveLocation;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}

	public void removeTask(int taskId) {
		tasks.remove(taskId);
		toStart.remove(taskId);
		toHive.remove(taskId);
		taskStates.remove(taskId);
	}
	
	@Override
	public int hashCode() {
		return missionId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MissionData other = (MissionData) obj;
		if (missionId != other.missionId)
			return false;
		return true;
	}

	public void addTransitToFirst(MissionTask toStart, MissionTaskState state) {
		this.toStart.put(toStart.getTaskId(),toStart);
		taskStates.put(toStart.getTaskId(), state);
	}
	
	public void addTransitToHive(MissionTask toHive, MissionTaskState state) {
		this.toHive.put(toHive.getTaskId(),toHive);
		taskStates.put(toHive.getTaskId(), state);
	}

}

