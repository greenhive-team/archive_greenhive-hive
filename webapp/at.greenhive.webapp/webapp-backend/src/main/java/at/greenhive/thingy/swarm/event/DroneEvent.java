package at.greenhive.thingy.swarm.event;

import java.time.LocalDateTime;

import me.limespace.appbase.event.IEvent;

public class DroneEvent implements IEvent {
	private final String droneId;
	private final LocalDateTime timestmp;
	
	public DroneEvent(String droneId) {
		this.droneId = droneId;
		this.timestmp = LocalDateTime.now();
	}
	
	public String getDroneId() {
		return droneId;
	}
	
	public LocalDateTime getTimestmp() {
		return timestmp;
	}
}
