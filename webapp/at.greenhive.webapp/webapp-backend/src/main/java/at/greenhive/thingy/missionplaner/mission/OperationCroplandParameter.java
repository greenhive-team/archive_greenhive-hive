package at.greenhive.thingy.missionplaner.mission;

import java.time.LocalDate;

public class OperationCroplandParameter {
	
	LocalDate missionDate;
	int sprayRate;  //  ml/m
	private double sprayHeight;
	private double maxSpeed;
	
	
	public OperationCroplandParameter(LocalDate missionDate, int sprayRate, double sprayHeight, double maxSpeed) {
		super();
		this.missionDate = missionDate;
		this.sprayRate = sprayRate;
		this.sprayHeight = sprayHeight;
		this.maxSpeed = maxSpeed;
	}

	public LocalDate getMissionDate() {
		return missionDate;
	}
	
	public int getSprayRate() {
		return sprayRate;
	}
	
	public double getSprayHeight() {
		return sprayHeight;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}
	
}
