package at.greenhive.thingy.settings;

import java.util.Locale;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.preferences.PreferencesMetaData;

public class SettingsThingy extends Thingy {

	@Override
	public String getIconPath() {
		return "einstellungen-25.png";
	}

	@Override
	public String getName() {
		return "settings";
	}

	@Override
	public String getDisplayName(Locale locale) {
		return "Einstellungen";
	}

	@Override
	public String getTooltipText(Locale locale) {
		return null;
	}

	@Override
	protected PreferencesMetaData getPreferencesMetaData() {
		return null;
	}

}
