package at.greenhive.thingy.cropland;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.peertopark.java.geocalc.DegreeCoordinate;

import at.greenhive.bl.util.geo.GeoPoint;

@Repository
public class CroplandRepo {
	private NamedParameterJdbcTemplate jdbcTemplate;

	private RowMapper<CroplandMetadata> croplandMetadataRowMapper = (rs, rownum) -> {
		CroplandMetadata cropland = new CroplandMetadata(rs.getInt("clid"));

		cropland.setCroptype(CropType.fromOrdinal(rs.getInt("croptype")));
		cropland.setDefaultSprayHeight(rs.getDouble("default_spray_height"));
		cropland.setMaxSpeed(rs.getDouble("max_speed"));
		cropland.setName(rs.getString("name"));
		cropland.setDescription(rs.getString("description"));
		
		return cropland;
	};
	
	private RowMapper<GeoPoint> geoPointRowMapper = (rs, rownum) -> {
		GeoPoint geoPoint = new GeoPoint(new DegreeCoordinate(rs.getDouble("lat")),
				new DegreeCoordinate(rs.getDouble("lon")), rs.getLong("alt"));
		return geoPoint;
	};

	private RowMapper<Integer> rowidRowMapper = (rs, rownum) -> {
		return Integer.valueOf(rs.getInt("rowid"));
	};
	
	private RowMapper<Integer> pathidPathMapper = (rs, pathnum) -> {
		return Integer.valueOf(rs.getInt("pathid"));
	};

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public List<CroplandMetadata> loadCroplandList() {

		String sql = "select clid,croptype, default_spray_height, max_speed, name, description from cropland";
		List<CroplandMetadata> croplandList = jdbcTemplate.query(sql, croplandMetadataRowMapper);

		return croplandList;
	}


	public int[] insertCroplandDetail(List<CroplandMetadata> croplandMeta) {

		Map<String, ?>[] parms = createParameterMap(croplandMeta);
		String sql = "insert into cropland (clid,name, default_spray_height, max_speed, croptype,description ) values (:clid,:name, :default_spray_height, :max_speed, :croptype, :description)";
		int[] updateCounts = jdbcTemplate.batchUpdate(sql,parms);
		return updateCounts;
	}
	
	public int[] insertCroplandRows( List<CroplandRowBean> croplandRowBeans) {

		Map<String, ?>[] parms = createCroplandRowParameterMap(croplandRowBeans);
		String sql = "insert into cropland_rows (clid, rowid, wpid, lat, lon, alt ) values (:clid, :rowid, :wpid, :lat, :lon, :alt)";
		int[] updateCounts = jdbcTemplate.batchUpdate(sql,parms);
		return updateCounts;
	}
	
	public int[] insertCroplandBounds( List<CroplandBoundBean> croplandBoundBeans) {

		Map<String, ?>[] parms = createCroplandBoundParameterMap(croplandBoundBeans);
		String sql = "insert into cropland_bounds (clid, wpid, lat, lon, alt ) values (:clid, :wpid, :lat, :lon, :alt)";
		int[] updateCounts = jdbcTemplate.batchUpdate(sql,parms);
		return updateCounts;
	}

	
	public int[] updateCroplandDetail(List<CroplandMetadata> croplandMeta) {

		Map<String, ?>[] parms = createParameterMap(croplandMeta);
		String sql = "update cropland set croptype = :croptype, name = :name ,description = :description where clid = :clid";
		int[] updateCounts = jdbcTemplate.batchUpdate(sql,parms);
		return updateCounts;
	}
	
	private Map<String, ?>[] createCroplandRowParameterMap(List<CroplandRowBean> croplandRowBeans) {
		@SuppressWarnings("unchecked")
		Map<String,?>[] parms = new HashMap[croplandRowBeans.size()];
		
		int ix = 0;
		for (CroplandRowBean clrow:croplandRowBeans) {
			HashMap<String,Object> param = new  HashMap<String, Object>(); 
			parms[ix++] = param;
			param.put("clid", clrow.getClid());
			param.put("rowid", clrow.getRowid());
			param.put("wpid", clrow.getWpid());
			param.put("lat", clrow.getLat());
			param.put("lon", clrow.getLon());
			param.put("alt", clrow.getAlt());
		}
		return parms;
	}
	
	private Map<String, ?>[] createCroplandBoundParameterMap(List<CroplandBoundBean> croplandBoundBean) {
		@SuppressWarnings("unchecked")
		Map<String,?>[] parms = new HashMap[croplandBoundBean.size()];
		
		int ix = 0;
		for (CroplandBoundBean clBound:croplandBoundBean) {
			HashMap<String,Object> param = new  HashMap<String, Object>(); 
			parms[ix++] = param;
			param.put("clid", clBound.getClid());
			param.put("wpid", clBound.getWpid());
			param.put("lat", clBound.getLat());
			param.put("lon", clBound.getLon());
			param.put("alt", clBound.getAlt());
		}
		return parms;
	}

	private Map<String, ?>[] createParameterMap(List<CroplandMetadata> croplandMeta) {
		@SuppressWarnings("unchecked")
		Map<String,?>[] parms = new HashMap[croplandMeta.size()];
		
		int ix = 0;
		for (CroplandMetadata clmd:croplandMeta) {
			HashMap<String,Object> param = new  HashMap<String, Object>(); 
			parms[ix++] = param;
			param.put("clid", clmd.getClid());
			param.put("name", clmd.getName());
			param.put("default_spray_height", clmd.getDefaultSprayHeight());
			param.put("max_speed", clmd.getMaxSpeed());
			param.put("description", clmd.getDescription());
			param.put("croptype", clmd.getCroptype().ordinal());
			
		}
		return parms;
	}

	public CroplandMetadata loadCroplandDetail(int clid) {

		String sql = "select clid,croptype,default_spray_height, max_speed, name,description from cropland where clid = :clid";
		Map<String, Integer> namedParameters = Collections.singletonMap("clid", clid);
		CroplandMetadata cl = jdbcTemplate.queryForObject(sql, namedParameters, croplandMetadataRowMapper);

		return cl;
	}

	public List<GeoPoint> loadCroplandOutline(int clid) {
		String sql = "select wpid,lat,lon,alt from cropland_bounds where clid = :clid ORDER BY wpid";
		Map<String, Integer> namedParameters = Collections.singletonMap("clid", clid);
		List<GeoPoint> outline = jdbcTemplate.query(sql, namedParameters, geoPointRowMapper);

		return outline;
	}

	public List<CroplandRow> loadCroplandRows(int clid) {
		
		//TODO: improve DB access 
		// should be changed so that all rows are read with one query 
		// row / waypoint split should be done in code
		
		String sql = "SELECT DISTINCT rowid FROM cropland_rows WHERE clid = :clid";
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		namedParameters.put("clid", clid);
		List<Integer> rows = jdbcTemplate.query(sql, namedParameters, rowidRowMapper);

		List<CroplandRow> croplandRows = new LinkedList<CroplandRow>();
		for (Integer row : rows) {
			sql = "SELECT rowid,wpid,lat,lon,alt FROM cropland_rows WHERE clid=:clid AND rowid=:rowid ORDER BY wpid";
			namedParameters.put("rowid", row);
			List<GeoPoint> path = jdbcTemplate.query(sql, namedParameters, geoPointRowMapper);
			CroplandRow croplandRow = new CroplandRow(row);
			croplandRow.setPath(path);
			croplandRows.add(croplandRow);
		}

		return croplandRows;
	}
	
	public List<CroplandPath> loadCroplandPaths(int clid) {
		
		//TODO: improve DB access 
		// should be changed so that all rows are read with one query 
		// row / waypoint split should be done in code
		
		String sql = "SELECT DISTINCT pathid FROM cropland_paths WHERE clid = :clid";
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		namedParameters.put("clid", clid);
		List<Integer> paths = jdbcTemplate.query(sql, namedParameters, pathidPathMapper);

		List<CroplandPath> croplandPaths = new LinkedList<CroplandPath>();
		for (Integer path : paths) {
			sql = "SELECT pathid,wpid,lat,lon,alt FROM cropland_paths WHERE clid=:clid AND pathid=:pathid ORDER BY wpid";
			namedParameters.put("pathid", path);
			List<GeoPoint> pathList = jdbcTemplate.query(sql, namedParameters, geoPointRowMapper);
			CroplandPath croplandPath = new CroplandPath(path);
			croplandPath.setPath(pathList);
			croplandPaths.add(croplandPath);
		}

		return croplandPaths;
	}

	public GeoPoint loadHiveLocation(int clid) {
		String sql = "select lat, lon , alt from cropland_hive_location where clid = :clid";
		Map<String, Integer> namedParameters = Collections.singletonMap("clid", clid);
		GeoPoint hiveLocation = jdbcTemplate.queryForObject(sql, namedParameters, geoPointRowMapper);

		return hiveLocation;
	}
	
	public CroplandDeleteInfo deleteCropland(int clid) {
		
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		namedParameters.put("clid", clid);
		
    	String sql = "DELETE from cropland_rows WHERE clid = :clid";
    	int del_croplandRows = jdbcTemplate.update(sql, namedParameters);
    	
    	sql = "DELETE from cropland_bounds WHERE clid = :clid";
    	int del_croplandBounds = jdbcTemplate.update(sql, namedParameters);
    	
    	sql = "DELETE from cropland WHERE clid = :clid";
    	int del_cropland = jdbcTemplate.update(sql, namedParameters);
    	
    	
		return new CroplandDeleteInfo(del_cropland, del_croplandBounds, del_croplandRows);
	}

	public int deleteAllCroplands() {
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		String sql = "DELETE from cropland ";
    	return jdbcTemplate.update(sql, namedParameters);
	}
	
	public int deleteAllCroplandRows() {
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		String sql = "DELETE from cropland_rows ";
    	return jdbcTemplate.update(sql, namedParameters);
	}
	
	public int deleteAllCroplandBounds() {
		Map<String, Integer> namedParameters = new HashMap<String, Integer>();
		String sql = "DELETE from cropland_bounds ";
		return jdbcTemplate.update(sql, namedParameters);
	}
}
