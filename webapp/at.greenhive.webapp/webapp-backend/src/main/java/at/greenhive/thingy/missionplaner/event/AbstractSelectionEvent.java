package at.greenhive.thingy.missionplaner.event;

import java.util.Optional;
import java.util.Set;

public class AbstractSelectionEvent<T> {
	private Optional<T> firstSelectedItem;
	private Set<T> allSelectedItems;
	
	private Object source;
	
	public AbstractSelectionEvent(Optional<T> firstSelectedItem, Set<T> allSelectedItems, Object source) {
		this.firstSelectedItem = firstSelectedItem;
		this.allSelectedItems = allSelectedItems;
		this.source = source;
	}

	public Optional<T> getFirstSelectedItem() {
		return firstSelectedItem;
	}
	
	public Set<T> getAllSelectedItems() {
		return allSelectedItems;
	}
	
	public Object getSource() {
		return source;
	}
}
