package at.greenhive.thingy.missionplaner.mission;

public class OperationMetaData {
	
	private final int operationId;
	private int croplandId;
	
	private int qantity;
	private String name;

	public OperationMetaData(int oid) {
		this.operationId = oid;
	}

	public int getCroplandId() {
		return croplandId;
	}

	public void setCroplandId(int croplandId) {
		this.croplandId = croplandId;
	}
	
	/**
	 * 
	 * @return Spray quantity in ml/m
	 */
	public int getQantity() {
		return qantity;
	}

	public void setQuantity(int qantity) {
		this.qantity = qantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getOperationId() {
		return operationId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + croplandId;
		result = prime * result + operationId;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + qantity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationMetaData other = (OperationMetaData) obj;
		if (croplandId != other.croplandId)
			return false;
		if (operationId != other.operationId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (qantity != other.qantity)
			return false;
		return true;
	}

	
	
}
