package at.greenhive.bl.imports;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.greenhive.thingy.cropland.CroplandService;
import at.greenhive.thingy.operation.OperationService;

@Component
public class ImportService {

	@Autowired
	CroplandService croplandService;
	
	@Autowired
	OperationService operationService;
	
	public void importXLSX(InputStream inputStream) throws IOException {
		XLSXImporter importer = new XLSXImporter(croplandService, operationService);
		importer.importData(inputStream);
	}
}
