package at.greenhive.thingy.missionplaner;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import at.greenhive.bl.util.math.geo2d.Line2D;
import at.greenhive.bl.util.math.geo2d.Point2D;

public class WorkPath {

	public static class DistanceResult {
		
		final double distance;
		final Point2D projectedPoint;
		
		public DistanceResult(double distance, Point2D projectedPoint) {
			this.distance = distance;
			this.projectedPoint = projectedPoint;
		}
		public double getDistance() {
			return distance;
		}
		
		public Point2D getProjectedPoint() {
			return projectedPoint;
		}
	}
	
	int id;
	LinkedList<WorkCoordinate> path;
	Double length = null;
	
	public WorkPath(int rowId) {
		this.id = rowId;
		path = new LinkedList<>();
	}
	
	public LinkedList<WorkCoordinate> getPath() {
		return path;
	}

	public WorkCoordinate insertSplit(Point2D point) {
		ListIterator<WorkCoordinate> it = path.listIterator();
		WorkCoordinate first = it.next();
		while (it.hasNext()) {
			WorkCoordinate next = it.next();

			Line2D seg = new Line2D(new Point2D(first.getX(), first.getZ()), new Point2D(next.getX(),next.getZ()));
			if (seg.isInside(point)) {
				it.previous();
				WorkCoordinate workCoordinate = new WorkCoordinate(point.x, first.getY(), point.y);
				it.add(workCoordinate);
				return workCoordinate;
			}
			
			first = next;
		}
		return null;
	}
	
	
	public DistanceResult getDistance(WorkCoordinate point) {
		ListIterator<WorkCoordinate> it = path.listIterator();
		WorkCoordinate first = it.next();
		
		double minDistance = Double.MAX_VALUE;
		DistanceResult result = null;
		while (it.hasNext()) {
			WorkCoordinate next = it.next();
			Line2D seg = new Line2D(new Point2D(first.getX(), first.getZ()), new Point2D(next.getX(),next.getZ()));
			Point2D myPoint = new Point2D(point.getX(), point.getZ());
			
			Point2D projectedPointOnLine = seg.getProjectedPointOnLine(myPoint);
			if (seg.isInside(projectedPointOnLine)) {
				double dist = seg.distance(myPoint);
				if (dist < minDistance) {
					minDistance = dist;
					result = new DistanceResult(dist, projectedPointOnLine);
					System.out.println("for "+ this.hashCode() + ",found better - dist: " + dist + ", Seg:" + seg.toString() +", point:" + projectedPointOnLine.toString());
				}
			}
			first = next;
		}
		return result;
	}
	
	public void addPathPoint(WorkCoordinate point) {
		path.add(point);
		length = null;
	}

	public WorkCoordinate getStartPoint() {
		return path.getFirst();
	}

	public WorkCoordinate getEndPoint() {
		return path.getLast();
	}

	public double getLength() {
		if (length == null) {
			double calcLen = 0;
			
			Iterator<WorkCoordinate> it = path.iterator();
			WorkCoordinate first = it.next();
			while (it.hasNext()) {
				WorkCoordinate next = it.next();
				calcLen += WorkCoordinate.calcLength(first, next);
				first = next;
			}
			
			length = Double.valueOf(calcLen);
		}
		return length.doubleValue();
	}

	public double getDistance(WorkPath other) {
		return WorkCoordinate.calcLength(this.getEndPoint(), other.getStartPoint());
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (WorkCoordinate c:path) {
			if (b.length()>0 ) {
				b.append("->");
			}
			b.append(c.toString());
		}
		return b.toString();
	}
	
}
