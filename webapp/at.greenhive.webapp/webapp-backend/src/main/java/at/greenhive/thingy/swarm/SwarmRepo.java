package at.greenhive.thingy.swarm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SwarmRepo {
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private RowMapper<Swarm> swarmRowMapper = (rs, rownum) -> {
		Swarm swarm = new Swarm(rs.getString("swarmid"));
		swarm.setName(rs.getString("name"));
		return swarm;
	};
	
	private ResultSetExtractor<Map<String,List<Bumblebee>>> bumbleBeeExtractor =  (rs) -> {
		Map<String,List<Bumblebee>> bumblebees = new HashMap<>();
		while (rs.next()) {
			Bumblebee bee = new Bumblebee(rs.getString("serialid"));
			bee.setName(rs.getString("name"));
			
			String swarmId = rs.getString("swarmId");
			
			List<Bumblebee> beeList = bumblebees.get(swarmId);
			if  (beeList == null) {
				beeList = new ArrayList<>();
				bumblebees.put(swarmId, beeList);
			}
			beeList.add(bee);
		}
		return bumblebees;
	};
	
	private ResultSetExtractor<Map<String,List<Ant>>> antExtractor =  (rs) -> {
		Map<String,List<Ant>> ants = new HashMap<>();
		while (rs.next()) {
			Ant ant = new Ant(rs.getString("serialid"));
			ant.setName(rs.getString("name"));
			
			String swarmId = rs.getString("swarmId");
			
			List<Ant> antList = ants.get(swarmId);
			if  (antList == null) {
				antList = new ArrayList<>();
				ants.put(swarmId, antList);
			}
			antList.add(ant);
		}
		return ants;
	};
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
		
	public Swarm createSwarm(String id, List<Bumblebee> bees) {
		Swarm swarm = new Swarm(id);
		swarm.addDrones(bees);
		return swarm;
	}

	public Swarm loadSwarm(String swarmid) {

		String sql = "select swarmid,name from swarm where swarmid = :swarmid";
		Map<String, String> namedParameters = Collections.singletonMap("swarmid", swarmid);
		Swarm swarm = jdbcTemplate.queryForObject(sql, namedParameters, swarmRowMapper);

		return swarm;
	}


	public List<Swarm> loadSwarm() {
		
		String sql = "select serialid,name,swarmid from bumblebee";
		Map<String,List<Bumblebee>> bumblebees = jdbcTemplate.query(sql, bumbleBeeExtractor);
		
		sql = "select serialid,name,swarmid from ant";
		Map<String,List<Ant>> ants = jdbcTemplate.query(sql, antExtractor);

		sql = "select swarmid,name from swarm";
		List<Swarm> swarmList = jdbcTemplate.query(sql, swarmRowMapper);
		
		for (Swarm swarm:swarmList) {
			List<Bumblebee> bees = bumblebees.get(swarm.getId());
			if (bees != null) {
				for (Bumblebee bee:bees) {
					swarm.addDrone(bee);
				}
			}
			List<Ant> antList = ants.get(swarm.getId());
			if (antList != null) {
				for (Ant ant:antList) {
					swarm.addDrone(ant);
				}
			}
		}

		return swarmList;
	}
}
