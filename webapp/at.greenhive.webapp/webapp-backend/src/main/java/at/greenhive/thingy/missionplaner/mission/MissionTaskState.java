package at.greenhive.thingy.missionplaner.mission;

public class MissionTaskState {
	
	private final int taskId;
	private String droneId;
	private enTaskState taskState;
	
	public MissionTaskState( int taskId) {
		this.taskId = taskId;
		this.taskState = enTaskState.UNASSIGNED;
	}
	
	public int getTaskId() {
		return taskId;
	}
	
	public void reset() {
		droneId = null;
		taskState = enTaskState.UNASSIGNED;
	}
	
	public void setPlannedDroneId(String droneId) {
		this.droneId = droneId;
		taskState = enTaskState.PLANNED;
	}
	
	public void setDroneId(String droneId) {
		this.droneId = droneId;
		taskState = enTaskState.ASSIGNED;
	}
	
	public String getDroneId() {
		return droneId;
	}
	
	public enTaskState getTaskState() {
		return taskState;
	}
	
	public void setTaskState(enTaskState taskState) throws IllegalTaskStateException {
		checkState(taskState);
		this.taskState = taskState;
	}
	
	private void checkState(enTaskState taskState) throws IllegalTaskStateException {
		if (droneId == null && taskState != enTaskState.UNASSIGNED) {
			throw new IllegalTaskStateException();
		}
	}
}
