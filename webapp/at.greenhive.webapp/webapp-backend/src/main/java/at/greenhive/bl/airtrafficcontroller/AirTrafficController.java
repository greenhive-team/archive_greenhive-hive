package at.greenhive.bl.airtrafficcontroller;

import com.google.protobuf.ByteString;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.protobuf.Message.Container;
import at.greenhive.protobuf.Rtk;
import at.greenhive.protobuf.Types.ContainerType;
import me.limespace.appbase.event.IEventSource;

public class AirTrafficController implements IEventSource {
	
	public void sendReset(String droneId) {
		Container reset = Container.newBuilder().setType(ContainerType.GH_RESET).build();
		sendMessageToDrone(droneId, reset);
	}
	
	public void abortMission(String droneId) {
		Container reset = Container.newBuilder().setType(ContainerType.GH_ABORT_MISSION).build();
		sendMessageToDrone(droneId, reset);
	}
	
	public void sendTakeOffCheck(String droneId) {
		Container takeOffCheck = Container.newBuilder().setType(ContainerType.GH_TAKEOFF_CHECK).build();
		sendMessageToDrone(droneId, takeOffCheck);
	}
	
	public void sendTakeOff(String droneId) {
		Container takeOff = Container.newBuilder().setType(ContainerType.GH_TAKEOFF).build();
		sendMessageToDrone(droneId, takeOff);
	}
	
	public void sendRTKInfo(String droneId, byte[] rtkInfo) {
		Container rtk = Container.newBuilder()
				.setType(ContainerType.GH_RTK)
				.setRtkCorrection(Rtk.rtk.newBuilder()
									.setMessageBuffer(ByteString.copyFrom(rtkInfo))
									.build())
				.build();
		sendMessageToDrone(droneId, rtk);
	}
	
	public void sendMessageToDrone(String droneId, Container message) {
		SendDroneMessageEvent sme = new SendDroneMessageEvent(droneId, message);
		GlobalEventManager.fire(sme, this);
	}
}
