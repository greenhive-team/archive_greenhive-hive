package at.greenhive.thingy.missionplaner;

import com.peertopark.java.geocalc.DegreeCoordinate;

import at.greenhive.bl.util.geo.CoordinateConversions;
import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.bl.util.math.geo3D.CoordinateSystem;
import at.greenhive.bl.util.math.geo3D.Vector3D;

public class HiveCoordinateConverter {
	
	private final GeoPoint rootLocation;
	private final double[] xyzRootLocation;
	private CoordinateSystem localCS;
	
	public HiveCoordinateConverter(GeoPoint rootLocation, GeoPoint zPoint) {
		this.rootLocation = rootLocation;
		this.xyzRootLocation = CoordinateConversions.getXYZfromLatLonDegrees(rootLocation.getLatitudeDecimalDegrees(), rootLocation.getLongitudeDecimalDegrees(),rootLocation.getAltitude());
		if (zPoint != null) {
			double[] xyz_zPoint = CoordinateConversions.getXYZfromLatLonDegrees(zPoint.getLatitudeDecimalDegrees(), zPoint.getLongitudeDecimalDegrees(),zPoint.getAltitude());
			
			Vector3D origin = new Vector3D(xyzRootLocation);
			Vector3D zDir = new Vector3D(xyz_zPoint).sub(origin);
			
			localCS = new CoordinateSystem(origin, zDir, new Vector3D(xyzRootLocation).norm());
		}
	}
	
	public WorkCoordinate toRelative(GeoPoint point) {
		double[] xyzPoint = CoordinateConversions.getXYZfromLatLonDegrees(point.getLatitudeDecimalDegrees(), point.getLongitudeDecimalDegrees(),point.getAltitude());
		if (localCS != null) {
			Vector3D localPoint = localCS.toLocal().multiply(new Vector3D(xyzPoint));
			return new WorkCoordinate(  localPoint.val[0] ,
										localPoint.val[1] ,
										localPoint.val[2] );
		}
		
		
		WorkCoordinate workCo = new WorkCoordinate( xyzPoint[0] - xyzRootLocation[0],
													xyzPoint[1] - xyzRootLocation[1],
													xyzPoint[2] - xyzRootLocation[2]);
		return workCo;
	}
	
	public GeoPoint toAbsolut(WorkCoordinate workCoordinate) {
		
		double[] xyz = new double[3];
		
		if (localCS != null) {
			
			xyz[0] = workCoordinate.getX() ;
			xyz[1] = workCoordinate.getY() ;
			xyz[2] = workCoordinate.getZ() ;
			
			Vector3D globalPoint = localCS.fromLocal().multiply(new Vector3D(xyz));
			xyz = globalPoint.val;
					
		} else {
		
			xyz[0] = workCoordinate.getX() +  xyzRootLocation[0] ;
			xyz[1] = workCoordinate.getY() +  xyzRootLocation[1] ;
			xyz[2] = workCoordinate.getZ() +  xyzRootLocation[2] ;
		}
		
		double[] xyzToLatLonDegrees = CoordinateConversions.xyzToLatLonDegrees(xyz);
		
		return new GeoPoint(new DegreeCoordinate(xyzToLatLonDegrees[0]),
							new DegreeCoordinate(xyzToLatLonDegrees[1]),
							Math.round(xyzToLatLonDegrees[2]));
		
	}
	
	public GeoPoint getRootLocation() {
		return rootLocation;
	}
}
