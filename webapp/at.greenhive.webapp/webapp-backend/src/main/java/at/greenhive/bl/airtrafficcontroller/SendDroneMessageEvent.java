package at.greenhive.bl.airtrafficcontroller;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import at.greenhive.protobuf.Message.Container;
import me.limespace.appbase.event.IEvent;

public class SendDroneMessageEvent implements IEvent {

	private String id;
	private Container message;

	public SendDroneMessageEvent(String id, Container message) {
		this.id = id;
		this.message = message;
	}

	public String getId() {
		return id;
	}
	
	public Container getMessage() {
		return message;
	}

	@Override
	public String toString() {
		try {
			return "SendDroneMessageEvent [id=" + id + ", message=" + JsonFormat.printer().print(message) + "]";
		} catch (InvalidProtocolBufferException e) {
			return "Error during JSON foramt" + e.getMessage();
		}
	}
	
}
