package at.greenhive.thingy.missionplaner;

import at.greenhive.bl.airtrafficcontroller.AirTrafficController;
import at.greenhive.protobuf.Message.Container;
import at.greenhive.protobuf.MissionOuterClass.Mission;
import at.greenhive.protobuf.MissionOuterClass.Mission.Builder;
import at.greenhive.protobuf.MissionOuterClass.Task;
import at.greenhive.protobuf.MissionOuterClass.TaskType;
import at.greenhive.protobuf.Positioning.GeoPoint;
import at.greenhive.protobuf.Types.ContainerType;
import at.greenhive.thingy.missionplaner.mission.MissionData;
import at.greenhive.thingy.missionplaner.mission.MissionSprayTask;
import at.greenhive.thingy.missionplaner.mission.MissionTask;
import at.greenhive.thingy.swarm.Swarm;

public class OperationController {

	static enum OperationStates {
		SWAM_ONLINE, SWARM_READY_FOR_TAKEOF, MISSIONS_IN_PROGRESS
	}

	AirTrafficController airTrafficController = new AirTrafficController();

	private final at.greenhive.bl.util.geo.GeoPoint hiveLocation;

	public OperationController(at.greenhive.bl.util.geo.GeoPoint hiveLocation) {
		this.hiveLocation = hiveLocation;
	}

	public void sendMissionData(MissionData missionData, HiveCoordinateConverter converter) {

		Builder missionBuilder = Mission.newBuilder();
		missionBuilder.setMissionId(missionData.getMissionId());
		missionBuilder.setHiveLocation(GeoPoint.newBuilder()
				.setLatitude(hiveLocation.getLatitudeDecimalDegrees())
				.setLongitude(hiveLocation.getLongitudeDecimalDegrees())
				.setAltitude(hiveLocation.getAltitude())
				.build());
		
		for (MissionTask t : missionData.getToStart()) {
			addMissionTask(missionBuilder,t, converter, missionData.getMaxSpeed());
			
		}
		for (MissionTask t : missionData.getTasks()) {
			if (t instanceof MissionSprayTask) {
				addMissionSprayTask(missionBuilder,(MissionSprayTask) t, converter, missionData.getMaxSpeed());
			}
		}
		for (MissionTask t : missionData.getToHive()) {
			addMissionTask(missionBuilder,t, converter, missionData.getMaxSpeed());
		}
		
		Container message = Container.newBuilder()
				.setType(ContainerType.GH_MISSION)
				.setMission(missionBuilder.build())
				.build();

		airTrafficController.sendMessageToDrone(missionData.getDroneId(), message);
	}
	
	
	private void addMissionSprayTask(Builder missionBuilder, MissionSprayTask t, HiveCoordinateConverter converter, double maxSpeed) {
		missionBuilder.addTaskList(Task.newBuilder()
				.setTaskId(t.getTaskId())
				.setTaskType(TaskType.SPRAY)
				.setSprayRate(t.getRate())
				.setSpeed(maxSpeed)
				.setStart(GeoPoint.newBuilder()
						.setLatitude(t.getStart(converter).getLatitudeDecimalDegrees())
						.setLongitude(t.getStart(converter).getLongitudeDecimalDegrees())
						.setAltitude(t.getStart(converter).getAltitude())
						.build())
				.setEnd(GeoPoint.newBuilder()
						.setLatitude(t.getEnd(converter).getLatitudeDecimalDegrees())
						.setLongitude(t.getEnd(converter).getLongitudeDecimalDegrees())
						.setAltitude(t.getEnd(converter).getAltitude())
						.build())
				.setSprayHeight(t.getHeight())
				.build());
	}
	
	private void addMissionTask(Builder missionBuilder, MissionTask t, HiveCoordinateConverter converter, double maxSpeed) {
		missionBuilder.addTaskList(Task.newBuilder()
				.setTaskId(t.getTaskId())
				.setTaskType(TaskType.TRANSIT)
				.setSpeed(maxSpeed)
				.setStart(GeoPoint.newBuilder()
						.setLatitude(t.getStart(converter).getLatitudeDecimalDegrees())
						.setLongitude(t.getStart(converter).getLongitudeDecimalDegrees())
						.setAltitude(t.getStart(converter).getAltitude())
						.build())
				.setEnd(GeoPoint.newBuilder()
						.setLatitude(t.getEnd(converter).getLatitudeDecimalDegrees())
						.setLongitude(t.getEnd(converter).getLongitudeDecimalDegrees())
						.setAltitude(t.getEnd(converter).getAltitude())
						.build())
				.build());
	}
	
	public void sendRTCMUpdate(Swarm swarm,byte[] rtkInfo) {
		swarm.getDrones().forEach(bee -> sendRTCMUpdate(bee.getSerialNumber(),rtkInfo ));
	}

	public void sendTakeOffChecks(Swarm swarm) {
		swarm.getDrones().forEach(bee -> sendTakeOffCheck(bee.getSerialNumber()));
	}

	public void sendTakeOff(Swarm swarm) {
		swarm.getDrones().forEach(bee -> sendTakeOff(bee.getSerialNumber()));
	}

	public void sendTakeOffCheck(String droneId) {
		airTrafficController.sendTakeOffCheck(droneId);
	}

	public void sendTakeOff(String droneId) {
		airTrafficController.sendTakeOff(droneId);
	}

	public void sendReset(String droneId) {
		airTrafficController.sendReset(droneId);
	}

	public void sendAbortMission(String droneId) {
		airTrafficController.abortMission(droneId);
	}

	private void sendRTCMUpdate(String droneId, byte[] rtkInfo) {
		airTrafficController.sendRTKInfo(droneId, rtkInfo);
	}
	
	
}
