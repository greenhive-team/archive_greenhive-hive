package at.greenhive.thingy.missionplaner.mission;

import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.thingy.missionplaner.HiveCoordinateConverter;
import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.WorkCoordinate;

public abstract class MissionTask {
	
	protected final int taskId;
	boolean forward = true;
	protected IWorkSegment workSegement;
	
	abstract public String getType();
	
	public MissionTask( int taskId, IWorkSegment workSegement) {
		this.taskId = taskId;
		this.workSegement = workSegement;
		//this.start = start;
		//this.end = end;
	}
	
	public int getTaskId() {
		return taskId;
	}
	
	public WorkCoordinate getEndCoordiante() {
		if (forward) {
			return workSegement.getEndPoint();
		}
		return workSegement.getStartPoint();
	}
	
	public WorkCoordinate getStartCoordiante() {
		if (forward) {
			return workSegement.getStartPoint();
		}
		return workSegement.getEndPoint();
	}
	
	public GeoPoint getStart(HiveCoordinateConverter converter) {
		return converter.toAbsolut(getStartCoordiante());
	}
	
	public GeoPoint getEnd(HiveCoordinateConverter converter) {
		return converter.toAbsolut(getEndCoordiante());

	}
	
	public void setStartRelatedTo(WorkCoordinate coordinate) {
		forward = WorkCoordinate.calcLength(workSegement.getStartPoint(), coordinate) < WorkCoordinate.calcLength(workSegement.getEndPoint(), coordinate);
		//forward = (coordinate.getDistance(workSegement.getStartPoint()) < workSegement.getDistance(workSegement.getEndPoint() ));
	}
	
	@Override
	public String toString() {
		return "MissionTask [taskId=" + taskId 
				//+ ", start="+ start 
				//+ ", end=" + end 
				+ "]";
	}
}
