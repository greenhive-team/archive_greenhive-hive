package at.greenhive.thingy.missionplaner;

import java.util.LinkedList;
import java.util.List;

public class WorkPatch {

	WorkPath bounds;
	List<WorkPath> tansitPaths;

	List<WorkPath> pathList = new LinkedList<>();

	public void addPath(WorkPath path) {
		pathList.add(path);

	}

	public void setBoundPath(WorkPath path) {
		bounds = path;
	}

	public List<WorkPath> getPathList() {
		return pathList;
	}

	public WorkPath getBounds() {
		return bounds;
	}

	public void setTransitPath(List<WorkPath> transitPaths) {
		tansitPaths = transitPaths;
	}
	
	public List<WorkPath> getTansitPaths() {
		return tansitPaths;
	}

}
