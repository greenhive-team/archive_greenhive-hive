package at.greenhive.bl;

import me.limespace.appbase.environment.IUser;

public class User implements IUser {

	private String userid;
	private String password;

	public User(String userid, String password) {
		this.userid = userid;
		this.password = password;
	}

	public String getUserid() {
		return userid;
	}
	
	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userid;
	}

	@Override
	public String getID() {
		return getUserid();
	}
}
