package at.greenhive.thingy.missionplaner.segmentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.greenhive.bl.util.math.geo3D.Vector3D;
import at.greenhive.thingy.missionplaner.DirectionWorkSegment;
import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.WorkCoordinate;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.WorkPath;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;

public class HorizontalRows extends SegmentationStrategy {

	public HorizontalRows(OperationCroplandParameter croplandMissionParm, OperationParameter missionParameter) {
		super(croplandMissionParm);
	}
	
	@Override
	public List<IWorkSegment> createSegments(WorkPatch patch) {
		
		List<IWorkSegment> result = new ArrayList<>();

		List<WorkPath> pathList = patch.getPathList();
		for (WorkPath path:pathList) {
			WorkCoordinate startPoint = null;
			WorkCoordinate endPoint = null;
			for (WorkCoordinate coordinate:path.getPath()) {
				if (endPoint != null) {
					startPoint = endPoint;
					endPoint = coordinate;
					calculate(startPoint,endPoint, result);
				} else {
					endPoint = coordinate;
				}
			}
		}
		
		return result;
	}

	private long calculate(WorkCoordinate startPoint, WorkCoordinate endPoint, List<IWorkSegment> result) {
		Vector3D line = getVector(startPoint, endPoint);
		long pathLen = Math.round(line.length());

		long maxLen = getSegmentLen();
		long segCount = Math.round(pathLen / maxLen);
				
		Vector3D dir = line.norm();
		Vector3D start = new Vector3D(startPoint.getX(), startPoint.getY(), startPoint.getZ() );
		Vector3D move = dir.multiply(maxLen);
		
		for (int i = 0; i < segCount; i++) {
			
			Vector3D end = start.add(move);
			
			DirectionWorkSegment s = new DirectionWorkSegment(); // Forward
			s.setRate(croplandMissionParm.getSprayRate()); //TODO: calc user defined spray rate by bounds 
			
			s.addPathPoint(new WorkCoordinate(start.x(), start.y(), start.z()));
			s.addPathPoint(new WorkCoordinate(end.x(), end.y(), end.z()));
			result.add(s);
			
			start = end;
		}
		
		return result.size();
	}

	@Override
	public List<IWorkSegment> createTransit(WorkPatch patch) {
		return Collections.emptyList();
	}


}
