package at.greenhive.thingy.swarm;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.peertopark.java.geocalc.DegreeCoordinate;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.thingy.swarm.event.DroneStatusUpdateEvent;
import at.greenhive.thingy.swarm.event.SwarmChangeEvent;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

@Component
@Scope("singleton")
public class SwarmManager implements ISwarmManager, IEventListener, IEventSource {

	private final String UNREGISTERED = "__unregistered__";
	
	@Autowired
	SwarmRepo swarmRepo;
	
	GlobalEventManager eventManager;
	
	private Map<String, Swarm> swarmMap = new HashMap<>();
	
	private SwarmManager() {
		
	}
	
	@PostConstruct
	public void initIt() throws Exception {
		GlobalEventManager.registerListener(this);
		getSwarmList();
	}

	@PreDestroy
	public void cleanUp() throws Exception {
		GlobalEventManager.unregisterListener(this);
	}
	
	public Swarm getSwarm(String swarmid) {
		Swarm swarm = swarmMap.get(swarmid);
		if (swarm == null) {
			swarm = swarmRepo.loadSwarm(swarmid);
			if (swarm != null) {
				swarmMap.put(swarm.getId(), swarm);
			}
		}
		return swarm;
	}

	public Collection<Swarm> getSwarmList() {
		
		List<Swarm> swarmList =  swarmRepo.loadSwarm();
		for (Swarm sw: swarmList) {
			swarmMap.put(sw.getId(), sw);
		}
		return swarmMap.values();
	}

	@Override
	public void notify(IEvent event, IEventSource source) {
		if (event instanceof DroneStatusUpdateEvent) {
			DroneStatusUpdateEvent ue = (DroneStatusUpdateEvent) event;
			
			String serial = ue.getDroneId();
			Drone bee = findBumbleBee(serial);
			if (bee == null) {
				bee = createUnregisteredBee(serial);
				GlobalEventManager.fire(new SwarmChangeEvent(),this);
			}
			
			if (bee != null) {
				DroneState state = createStateFromEvent(ue);
				bee.setState(state);
				GlobalEventManager.fire(new DroneNewState(bee), this);
			}
		} 
	}

	private Drone createUnregisteredBee(String serial) {
		
		Bumblebee bee = new Bumblebee(serial);
		
		Swarm swarm = swarmMap.get(UNREGISTERED);
		if (swarm == null) {
			swarm = new Swarm(UNREGISTERED);
			swarm.setName("Unknown Bees");
			swarmMap.put(UNREGISTERED, swarm);
		}
		
		swarm.addDrone(bee);
		
		return bee;
	}

	private DroneState createStateFromEvent(DroneStatusUpdateEvent ue) {
		
		DroneState state = new DroneState();
		
		state.setDroneState(ue.getStatus());
		state.setPowerLevel( ue.getBattery());
		state.setFillLevel(ue.getFillLevel());
		state.setTimestamp(LocalDateTime.now());
		state.setPosition(new GPSPoint( new DegreeCoordinate(ue.getLatitude()) , new DegreeCoordinate(ue.getLongitute()), (long) ue.getAltitute()   ));
		return state;
	}

	private Drone findBumbleBee(String serial) {
		for (Swarm swarm : swarmMap.values()) {
			Drone bumblebee = swarm.getDrone(serial);
			if (bumblebee != null) {
				return bumblebee;
			}
		}
		return null;
	}

}
