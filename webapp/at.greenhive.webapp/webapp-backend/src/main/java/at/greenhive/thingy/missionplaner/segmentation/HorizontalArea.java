package at.greenhive.thingy.missionplaner.segmentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import at.greenhive.bl.util.math.geo2d.Point2D;
import at.greenhive.bl.util.math.geo2d.Polygon2D;
import at.greenhive.bl.util.math.geo2d.Polygon2D.Builder;
import at.greenhive.bl.util.math.geo3D.Vector3D;
import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.NonDirectionWorkSegment;
import at.greenhive.thingy.missionplaner.WorkCoordinate;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.WorkPath;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;

public class HorizontalArea extends SegmentationStrategy {

	private OperationParameter operationParameter;

	public HorizontalArea(OperationCroplandParameter croplandMissionParm, OperationParameter operationParameter) {
		super(croplandMissionParm);
		this.operationParameter = operationParameter;
	}

	@Override
	public List<IWorkSegment> createTransit(WorkPatch patch) {
		return Collections.emptyList();
	}
	
	@Override
	public List<IWorkSegment> createSegments(WorkPatch patch) {

		WorkPath longest = findLongestSide(patch);

		Vector3D direction = getVector(longest.getStartPoint(), longest.getEndPoint());
		Vector3D offset = direction.crossMultiply(Vector3D.vy());
		offset.normalise();
		offset = offset.multiply(operationParameter.getSprayWidth());

		Polygon2D outlinePoly = createPolygon(patch);
		Vector3D move = direction.norm().multiply(getSegmentLen());

		Vector3D newStart = getVector(longest.getStartPoint()).sub(direction);
		Vector3D newEnd = getVector(longest.getStartPoint()).add(direction);

		List<IWorkSegment> result = new ArrayList<>();

		Vector3D nextStart = newStart.add(offset);
		Vector3D nextEnd = newEnd.add(offset);

		while (findSegments(move, nextStart, nextEnd,outlinePoly, result)) {
			nextStart = nextStart.add(offset);
			nextEnd = nextEnd.add(offset);
		}

		nextStart = newStart.sub(offset);
		nextEnd = newEnd.sub(offset);

		while (findSegments(move, nextStart, nextEnd,outlinePoly, result)) {
			nextStart = nextStart.sub(offset);
			nextEnd = nextEnd.sub(offset);
		}

		return result;
	}

	private boolean findSegments(Vector3D move, Vector3D start, Vector3D end, Polygon2D outlinePoly, List<IWorkSegment> result) {

		boolean found = false;
		long segCount = Math.round(end.sub(start).length() / getSegmentLen());
		
		Vector3D segStart = start;
		Vector3D segEnd = start;
		
		for (int i = 0; i < segCount; i++) {

			segEnd = segEnd.add(move);
			Point2D pStart = new Point2D(segStart.x(), segStart.z());
			Point2D pEnd = new Point2D(segEnd.x(), segEnd.z());
			
			if (outlinePoly.contains(pStart) || outlinePoly.contains(pEnd)) {
				NonDirectionWorkSegment s = new NonDirectionWorkSegment();
				s.setRate(croplandMissionParm.getSprayRate()); //TODO: calc user defined spray rate by bounds 
				
				s.addPathPoint(new WorkCoordinate(segStart.x(), segStart.y(), segStart.z()));
				s.addPathPoint(new WorkCoordinate(segEnd.x(), segEnd.y(), segEnd.z()));
				result.add(s);
				found = true;
			}
			
			segStart = segEnd;
		}

		return found;
	}

	private Polygon2D createPolygon(WorkPatch patch) {
		Builder builder = Polygon2D.Builder();
		for (WorkCoordinate c : patch.getBounds().getPath()) {
			builder.addVertex(new Point2D(c.getX(), c.getZ()));

		}
		return builder.build();
	}

	private WorkPath findLongestSide(WorkPatch patch) {
		double maxLen = 0;
		WorkPath longest = null;
		
		LinkedList<WorkCoordinate> path = patch.getBounds().getPath();
		
		for (int i = 1; i <= path.size(); i++) {
			
			WorkCoordinate wc0;
			WorkCoordinate wc1;
			
			if (i == path.size()) {
				wc0 = path.get(i-1);
				wc1 = path.get(0);
			} else {
				wc0 = path.get(i-1);
				wc1 = path.get(i);
			}
					
			double len = WorkCoordinate.calcLength(wc0, wc1);
		
		
			if (len > maxLen) {
				WorkPath workPath = new WorkPath(i);
				workPath.addPathPoint(wc0);
				workPath.addPathPoint(wc1);

				maxLen = len;
				longest = workPath;
			}
		}

		return longest;
	}

}
