package at.greenhive.bl;

import java.util.Collection;

public interface IReadOnlyService<T> {
	Collection<T> getAll();
}
