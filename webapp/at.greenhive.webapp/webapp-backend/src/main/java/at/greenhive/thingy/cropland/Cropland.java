package at.greenhive.thingy.cropland;

import java.util.List;

import at.greenhive.bl.util.geo.GeoPoint;

public class Cropland {
	
	CroplandMetadata metadata;
	
	private final List<GeoPoint> outline;
	private final List<CroplandRow> rows;
	private final List<CroplandPath> paths;
	private final GeoPoint hiveLocation;
	
	public Cropland(CroplandMetadata metadata, List<GeoPoint> outline, List<CroplandRow> rows,  List<CroplandPath> paths, GeoPoint hiveLocation) {
		this.metadata = metadata;
		this.outline = outline;
		this.rows = rows;
		this.paths = paths;
		this.hiveLocation = hiveLocation;
	}
	
	public GeoPoint getHiveLocation() {
		return hiveLocation;
	}
	
	public CroplandMetadata getMetadata() {
		return metadata;
	}
	
	public List<GeoPoint> getOutline() {
		return outline;
	}
	
	public List<CroplandRow> getRows() {
		return rows;
	}

	public List<CroplandPath> getPaths() {
		return paths;
	}
}
