package at.greenhive.thingy.hivecontrol;

import java.util.Locale;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.preferences.PreferencesMetaData;

public class HiveControllerThingy extends Thingy {

	@Override
	public String getIconPath() {
		return "controller-25.png";
	}

	@Override
	public String getName() {
		return "hivecontroller";
	}

	@Override
	public String getDisplayName(Locale locale) {
		return "Hive Controller";
	}

	@Override
	public String getTooltipText(Locale locale) {
		return null;
	}

	@Override
	protected PreferencesMetaData getPreferencesMetaData() {
		// TODO Auto-generated method stub
		return null;
	}

}
