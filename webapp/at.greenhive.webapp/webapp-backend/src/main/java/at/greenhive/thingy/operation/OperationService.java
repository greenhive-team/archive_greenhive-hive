package at.greenhive.thingy.operation;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.greenhive.bl.IReadOnlyService;
import at.greenhive.thingy.missionplaner.mission.OperationData;
import at.greenhive.thingy.missionplaner.mission.OperationMetaData;

@Component
public class OperationService implements IReadOnlyService<OperationMetaData> {
	
	@Autowired
	OperationRepo repository;
	
	@Override
	public Collection<OperationMetaData> getAll() {
		return repository.loadOperationMetaList();
	}

	public OperationData loadOperation(String id) {
		return repository.loadOperation(id);
	}
	
	public OperationData createOperation(OperationData operationData) {
		return repository.createOperation(operationData);
	}
	
	public void  replaceAllOperations(List<OperationMetaData> operationMetaData) {
		repository.deleteAllOperations();
		repository.insertOperationMetaList(operationMetaData);
	}

}
