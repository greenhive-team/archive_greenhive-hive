package at.greenhive.thingy.missionplaner;

public class NonDirectionWorkSegment implements IWorkSegment {
	
	double rate;
	WorkPath path;
	
	public NonDirectionWorkSegment() {
		path = new WorkPath(0);
	}
	
	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#setRate(double)
	 */
	@Override
	public void setRate(double rate) {
		this.rate = rate;
	}
	
	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getRate()
	 */
	@Override
	public double getRate() {
		return rate;
	}
	
	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getSprayAmount()
	 */
	@Override
	public double getSprayAmount() {
		return rate * path.getLength();
	}

	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getStartPoint()
	 */
	@Override
	public WorkCoordinate getStartPoint() {
		return path.getStartPoint();
	}

	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#getEndPoint()
	 */
	@Override
	public WorkCoordinate getEndPoint() {
		return path.getEndPoint();
	}

	/* (non-Javadoc)
	 * @see at.greenhive.thingy.missionplaner.IWorkSegment#addPathPoint(at.greenhive.thingy.missionplaner.WorkCoordinate)
	 */
	@Override
	public void addPathPoint(WorkCoordinate point) {
		path.addPathPoint(point);
	}

	@Override
	public double getDistance(IWorkSegment other) {
		
		return Math.min(WorkCoordinate.calcLength(path.getStartPoint(), other.getEndPoint()), 
				Math.min(WorkCoordinate.calcLength(path.getStartPoint(), other.getStartPoint()),
						Math.min(WorkCoordinate.calcLength(path.getEndPoint(), other.getStartPoint()),
								 WorkCoordinate.calcLength(path.getEndPoint(), other.getEndPoint()))));
		
	}

	@Override
	public double getDistance(WorkCoordinate coordinate) {
		return Math.min(WorkCoordinate.calcLength(path.getStartPoint(), coordinate), 
					    WorkCoordinate.calcLength(path.getEndPoint(), coordinate));
	}
}
