package at.greenhive.bl;

import java.util.Locale;

import me.limespace.appbase.environment.IEnvironment;
import me.limespace.appbase.preferences.IPreferences;
import me.limespace.appbase.preferences.PreferencesMetaData;

public abstract class Thingy {
	
	User user;
	private IEnvironment environment;
	
	public abstract String getIconPath();
	public abstract String getName();
	public abstract String getDisplayName(Locale locale);
	public abstract String getTooltipText(Locale locale);
	
	
	protected abstract PreferencesMetaData getPreferencesMetaData();
	
	public void startup(IEnvironment environment, User user) {
		this.user = user;
		this.environment = environment;
	}

	public void shutdown() {
		this.user = null;
	}
	
	public IPreferences getPreferences() {
		return environment.getUserPreferences(IPreferences.Context.THINGY, this.getName(), user);
	}
	
	public ScreenLayout getLayout() {
		return ScreenLayout.FULLSCREEN;
	}
	
}
