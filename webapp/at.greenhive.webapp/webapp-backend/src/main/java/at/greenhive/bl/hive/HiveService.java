package at.greenhive.bl.hive;

import java.time.LocalDateTime;

import com.peertopark.java.geocalc.DegreeCoordinate;

import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.thingy.operation.OperationManager;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class HiveService implements IEventListener {

	private static HiveService instance;
	
	private String hiveId;
//	private GPSPoint hiveLocation = new GPSPoint(new DegreeCoordinate(47.067537306672314),
//												 new DegreeCoordinate(16.145094232558222),
//												 318);
//	
//	private GPSPoint hiveLocation = new GPSPoint(new DegreeCoordinate(47.06727970629792),
//												 new DegreeCoordinate(16.144949393271418),
//												 300);

	
	private GPSPoint hiveLocation = new GPSPoint(new DegreeCoordinate(46.6884121),
			 									 new DegreeCoordinate(15.5581291),
			 									 300);

	
	IHiveController hiveController;
	
	private LocalDateTime hiveLocationTimestamp;
	
	private OperationManager operationManager;
	
	public static void initInstance(String hiveId) {
		
		instance = new HiveService();
		instance.hiveId = hiveId;
		instance.hiveController = new HiveController();
		
		instance.operationManager = new OperationManager();
		instance.operationManager.startUp();
		
		GlobalEventManager.registerListener(instance);
	}
	
	public static void shutdown() {
		
		GlobalEventManager.unregisterListener(instance);
		instance.operationManager.shutDown();
		instance = null;
	}
	
	private static HiveService get() {
		if ( instance == null) {
			throw new IllegalStateException("HiveService is not initialized.");
		}
		return instance;
	}
	
	public static String getHiveId() {
		return get().hiveId;
	}
	
	public static IHiveController getHiveController() {
		return get().hiveController;
	}
	
	public static boolean isInitialized() {
		return instance != null;
	}
	
	public static boolean isGPSInitialized() {
		return true;
	}
	
	public static GPSPoint getHiveLocation() {
		return get().hiveLocation;
	}
	
	public static OperationManager getOperationManager() {
		return get().operationManager;
	}
	
	public static LocalDateTime getHiveLocationTimestamp() {
		return get().hiveLocationTimestamp;
	}
	
	private void setGPS(GPSPoint hiveLocation) {
		get().hiveLocation = hiveLocation;
		get().hiveLocationTimestamp = LocalDateTime.now();
	}
	
	@Override
	public void notify(IEvent event, IEventSource source) {
		if (source != instance) {
			if (event instanceof HiveGPSUpdateEvent) {
				setGPS(((HiveGPSUpdateEvent)event).getGpsPoint());
			}
		}
	}
}
