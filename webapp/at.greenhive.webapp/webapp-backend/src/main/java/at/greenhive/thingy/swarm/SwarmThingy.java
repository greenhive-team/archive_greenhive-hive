package at.greenhive.thingy.swarm;

import java.util.Locale;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.preferences.PreferencesMetaData;

public class SwarmThingy extends Thingy {

	@Override
	public String getIconPath() {
		return "Bumblebee-25.png";
	}

	@Override
	public String getName() {
		return "Swarm";
	}

	@Override
	public String getDisplayName(Locale locale) {
		return "Swarm";
	}

	@Override
	public String getTooltipText(Locale locale) {
		return "Swarm";
	}

	@Override
	protected PreferencesMetaData getPreferencesMetaData() {
		// TODO Auto-generated method stub
		return null;
	}

}