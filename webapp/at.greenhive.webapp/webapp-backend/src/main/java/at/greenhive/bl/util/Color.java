package at.greenhive.bl.util;

public class Color {

	private float r;
	private float g;
	private float b;
	private float a; 
	
	public Color(float r, float g, float b, float alpha) {
		this.r = r; 
		this.g = g;
		this.b = b;
		this.a = alpha;
	}

	public float[] getRGBColorComponents(Object object) {
		float[] c = new float[4];
		c[0] = r;
		c[1] = g;
		c[2] = b;
		c[3] = a;
		return c;
	}

	public float getRed() {
		return r;
	}
	
	public float getGreen() {
		return g;
	}
	
	public float getBlue() {
		return b;
	}
	
	public float getAlpha() {
		return a;
	}
	
	public String getHexRGB() {
		return String.format("#%02x%02x%02x", (byte)(r * 255), (byte)(g * 255), (byte)(b * 255));
	}
	
	public String getHexRGBA() {
		return String.format("#%02x%02x%02x%02x", (byte)(r * 255), (byte)(g * 255), (byte)(b * 255), (byte)(a * 255));
	}
}
