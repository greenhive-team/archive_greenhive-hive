package at.greenhive.thingy.operation;

import java.time.LocalDateTime;

public class OperationState {
	
	public enum enOperationState {
		DEFINED, IDLE, WAITING_FOR_SWARM,  WAITING_FOR_TAKEOF, RUNNING, PAUSED, FINISHED, ABORTED, ERROR;
	}
	
	private final enOperationState state;
	private final LocalDateTime timestamp;

	public OperationState(enOperationState state) {
		this.state = state;
		this.timestamp = LocalDateTime.now();
	}

	public enOperationState getState() {
		return state;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}
}