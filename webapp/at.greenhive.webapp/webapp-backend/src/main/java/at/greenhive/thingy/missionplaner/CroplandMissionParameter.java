package at.greenhive.thingy.missionplaner;

import java.util.Date;

public class CroplandMissionParameter {
	
	Date missionDate;
	int sprayRate;  //  l/m?
	
	
	public CroplandMissionParameter(Date missionDate, int sprayRate) {
		super();
		this.missionDate = missionDate;
		this.sprayRate = sprayRate;
	}

	public Date getMissionDate() {
		return missionDate;
	}
	
	public int getSprayRate() {
		return sprayRate;
	}
	
	
}
