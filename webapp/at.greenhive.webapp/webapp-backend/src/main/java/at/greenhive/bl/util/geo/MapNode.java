package at.greenhive.bl.util.geo;

import java.util.HashSet;
import java.util.Set;

/**
 * MapNode class to be used in MapGraph
 */
public class MapNode {

	int nodeId;
	
	GeoPoint location;
	Set<MapEdge> edges;

	public MapNode(int nodeId, GeoPoint location) {
		this.nodeId = nodeId;
		this.location = location;
		edges = new HashSet<>();
	}

	public int getNodeId() {
		return nodeId;
	}
	/**
	 * Add an edge which is connected to node
	 * 
	 * @param edge MapEdge connected to node
	 * @return boolean
	 */
	public boolean addEdge(MapEdge edge) {
		return this.edges.add(edge);
	}

	/**
	 * Check whether a node contains an edge
	 * 
	 * @param edge MapEdge to check
	 * @return boolean
	 */
	public boolean containsEdge(MapEdge edge) {
		return edges.contains(edge);
	}

	/**
	 * Return all edges connected to node
	 * 
	 * @return boolean
	 */
	public Set<MapEdge> getEdges() {
		return this.edges;
	}

	/**
	 * Getter for this nodes location
	 * 
	 * @return GeographicPoint
	 */
	public GeoPoint getLocation() {
		return this.location;
	}
	
	@Override
	public String toString() {
		return "Loc:" + location.toString() + "Num.Edges" + edges.size();
	}
}
