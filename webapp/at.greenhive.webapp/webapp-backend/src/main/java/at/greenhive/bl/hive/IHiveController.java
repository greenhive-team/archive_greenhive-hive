package at.greenhive.bl.hive;

import java.util.Collection;

public interface IHiveController {
	
	Collection<String> listHardware();
	boolean startRTKListening(String id);
	boolean connectToHardware(String id);
	
	void moveX(int steps);
	void moveY(int steps);
	void moveZ(int steps);
}