package at.greenhive.thingy.missionplaner.segmentation;

import java.util.List;

import at.greenhive.bl.util.math.geo3D.Vector3D;
import at.greenhive.thingy.cropland.CropType;
import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.WorkCoordinate;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;

public abstract class SegmentationStrategy {

	public abstract List<IWorkSegment> createTransit(WorkPatch patch);
	public abstract List<IWorkSegment> createSegments(WorkPatch patch);
	
	public static SegmentationStrategy fromCropType(CropType croptype, OperationCroplandParameter croplandMissionParm, OperationParameter missionParameter) {

		switch (croptype) {
		case WINE:
			return new VerticalLeftRight(croplandMissionParm,missionParameter);
		case PUMPKIN:
			return new HorizontalRows(croplandMissionParm, missionParameter);
		case STRAWBERRY:
			return new HorizontalArea(croplandMissionParm, missionParameter);
			
		default:
			throw new IllegalArgumentException("Croptype" + croptype.name() +  " not supported.");
		}
	}
	
	protected OperationCroplandParameter croplandMissionParm;
	
	protected SegmentationStrategy(OperationCroplandParameter croplandMissionParm) {
		this.croplandMissionParm = croplandMissionParm;
	}
	
	protected Vector3D getVector(WorkCoordinate startPoint, WorkCoordinate endPoint) {
		double vx = endPoint.getX() - startPoint.getX();
		double vy = endPoint.getY() - startPoint.getY();
		double vz = endPoint.getZ() - startPoint.getZ();
		
		Vector3D line = new Vector3D(vx, vy, vz);
		return line;
	}
	
	protected Vector3D getVector(WorkCoordinate point) {
		double vx = point.getX();
		double vy = point.getY();
		double vz = point.getZ();
		
		return new Vector3D(vx, vy, vz);
	}
	
	protected long getSegmentLen() {
		return 2;
	}

}
