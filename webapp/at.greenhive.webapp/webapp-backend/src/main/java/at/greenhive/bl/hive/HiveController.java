package at.greenhive.bl.hive;

import java.util.ArrayList;
import java.util.Collection;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import at.greenhive.bl.event.GGAMessageUpdateEvent;
import at.greenhive.bl.event.GlobalEventManager;
import at.greenhive.bl.event.RTCMUpdateEvent;
import at.greenhive.bl.util.HexUtil;
import me.limespace.appbase.event.IEventSource;

public class HiveController implements IHiveController, IEventSource {

	final private String X_MOTOR_1 = "X";
	final private String Y_MOTOR_1 = "Y";

	final private String Z_MOTOR_1 = "Z";

	final private String MOVE = "";

	SerialPort[] commPorts;

	private SerialPort comPortChanger;
	private SerialPort comPortRTK;

	public HiveController() {
	}

	public Collection<String> listHardware() {

		commPorts = SerialPort.getCommPorts();
		ArrayList<String> rs = new ArrayList<>(commPorts.length);
		for (SerialPort port : commPorts) {
			rs.add(port.getDescriptivePortName());
		}
		return rs;
	}

	public boolean startRTKListening(String id) {
		if (comPortRTK != null && comPortRTK.isOpen()) {
			comPortRTK.closePort();

		}
		for (SerialPort port : commPorts) {
			if (id.equals(port.getDescriptivePortName())) {
				comPortRTK = port;
			}
		}

		if (!comPortRTK.openPort()) {
			return false;
		}

		comPortRTK.addDataListener(new SerialPortDataListener() {

			@Override
			public void serialEvent(SerialPortEvent event) {
				if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
					return;
				byte[] newData = new byte[comPortRTK.bytesAvailable()];
				comPortRTK.readBytes(newData, newData.length);
				if (newData.length > 2 && (newData[0] == (byte) 0xD3 && newData[1] == (byte) 0x00)) { // RTCM Message
					GlobalEventManager.fire(new RTCMUpdateEvent(newData), HiveController.this);
					//System.out.println(HexUtil.printHexBinary(newData));
				} else {
					String message = new String(newData);
					if (message.startsWith("$GNGGA")) {
						handleGGAMessage(message);
					}

				}
			}

			@Override
			public int getListeningEvents() {
				return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
			}
		});

		return true;

	}
	
	private void handleGGAMessage(String message) {
		
		System.out.println("GOT GGA: " + message);
		String[] parts = message.split(",");
		GlobalEventManager.fire(new GGAMessageUpdateEvent(parts), this);
		
	}

	public boolean connectToHardware(String id) {

		if (comPortChanger != null && comPortChanger.isOpen()) {
			comPortChanger.closePort();

		}
		for (SerialPort port : commPorts) {
			if (id.equals(port.getDescriptivePortName())) {
				comPortChanger = port;
			}
		}

		if (!comPortChanger.openPort()) {
			return false;
		}
		comPortChanger.setBaudRate(115200);

		comPortChanger.addDataListener(new SerialPortDataListener() {

			@Override
			public void serialEvent(SerialPortEvent event) {
				if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
					return;
				byte[] newData = new byte[comPortChanger.bytesAvailable()];
				comPortChanger.readBytes(newData, newData.length);
				System.out.print(new String(newData));
			}

			@Override
			public int getListeningEvents() {
				return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
			}
		});

		return true;
	}

	public void moveX(int steps) {
		String cmd = X_MOTOR_1 + MOVE + String.format("%09d", steps);
		byte[] cmdBytes = cmd.getBytes();
		comPortChanger.writeBytes(cmdBytes, cmdBytes.length);
	}

	public void moveY(int steps) {
		String cmd = Y_MOTOR_1 + MOVE + String.format("%09d", steps);
		byte[] cmdBytes = cmd.getBytes();
		comPortChanger.writeBytes(cmdBytes, cmdBytes.length);
	}

	public void moveZ(int steps) {
		String cmd = Z_MOTOR_1 + MOVE + String.format("%09d", steps);
		;
		byte[] cmdBytes = cmd.getBytes();
		comPortChanger.writeBytes(cmdBytes, cmdBytes.length);
	}



	

	
}
