package at.greenhive.bl.util.math.geo3D;

public class Vector3D {

	private static final int ID_X = 0;
	private static final int ID_Y = 1;
	private static final int ID_Z = 2;

	public double val[];

	public Vector3D() {
		this.val = new double[3];
		this.val[ID_X] = 0;
		this.val[ID_Y] = 0;
		this.val[ID_Z] = 0;
	}

	public Vector3D(Vector3D other) {
		this.val = new double[3];
		set(other);
	}
	
	public Vector3D(double[] val) {
		this.val = new double[3];
		this.val[ID_X] = val[ID_X];
		this.val[ID_Y] = val[ID_Y];
		this.val[ID_Z] = val[ID_Z];
	}
	
	public Vector3D(double x, double y, double z) {
		this.val = new double[3];
		this.val[ID_X] = x;
		this.val[ID_Y] = y;
		this.val[ID_Z] = z;
	}
	
	public double x() {
		return val[ID_X];
	}
	
	public double y() {
		return val[ID_Y];
	}
	
	public double z() {
		return val[ID_Z];
	}

	public void set(Vector3D rh) {
		val[ID_X] = rh.val[ID_X];
		val[ID_Y] = rh.val[ID_Y];
		val[ID_Z] = rh.val[ID_Z];
	}

	public void set(double x, double y, double z) {
		val[ID_X] = x;
		val[ID_Y] = y;
		val[ID_Z] = z;
	}

	public void clear() {
		for (int i = 0; i < 3; i++) {
			val[i] = 0.0;
		}
	}

	public boolean equals(Vector3D a) {
		for (int i = 0; i < 3; i++) {
			if (val[i] != a.val[i]) {
				return false;
			}
		}
		return true;
	}

	public double abs() {
		double abs = Math.sqrt(val[0] * val[0] + val[1] * val[1] + val[2] * val[2]);
		return abs;
	}

	public final double length() {
		return Math.sqrt(val[0] * val[0] + val[1] * val[1] + val[2] * val[2]);
	}

	public final double length2() {
		return val[0] * val[0] + val[1] * val[1] + val[2] * val[2];
	}

	/**
	 * 
	 * @return normalised vector 
	 */
	public Vector3D norm() {
		Vector3D ret = new Vector3D();
		double length = abs();

		for (int i = 0; i < 3; i++) {
			ret.val[i] = val[i] / length;
		}
		return ret;
	}

	/**
	 * Normalise this vector
	 */
	public void normalise() {
		double length = abs();
		for (int i = 0; i < 3; i++) {
			val[i] = val[i] / length;
		}
	}

	@Override
	public String toString() {
		return "(" + val[0] + ", " + val[1] + ", " + val[2] + ")";
	}

	public void setAdd(Vector3D a) {
		setAdd(a.val[ID_X], a.val[ID_Y], a.val[ID_Z]);
	}

	public void setAdd(double x, double y, double z) {
		val[ID_X] += x;
		val[ID_Y] += y;
		val[ID_Z] += z;
	}

	public Vector3D add(Vector3D a) {
		Vector3D ret = new Vector3D();
		for (int i = 0; i < 3; i++) {
			ret.val[i] = val[i] + a.val[i];
		}
		return ret;
	}

	public void setSub(Vector3D a) {
		for (int i = 0; i < 3; i++) {
			val[i] -= a.val[i];
		}
	}

	public Vector3D sub(Vector3D a) {
		Vector3D ret = new Vector3D();

		for (int i = 0; i < 3; i++) {
			ret.val[i] = val[i] - a.val[i];
		}

		return ret;
	}

	public void setMultiply(double a) {
		for (int i = 0; i < 3; i++) {
			val[i] *= a;
		}
	}

	public Vector3D multiply(double a) {
		Vector3D ret = new Vector3D();

		for (int i = 0; i < 3; i++) {
			ret.val[i] = val[i] * a;
		}

		return ret;
	}

	public double multiply(Vector3D a) {
		double ret = 0;

		for (int i = 0; i < 3; i++) {
			ret += val[i] * a.val[i];
		}

		return ret;
	}

	public Vector3D crossMultiply(Vector3D a) {
		Vector3D ret = new Vector3D();

		ret.val[0] = val[1] * a.val[2] - val[2] * a.val[1];
		ret.val[1] = val[2] * a.val[0] - val[0] * a.val[2];
		ret.val[2] = val[0] * a.val[1] - val[1] * a.val[0];

		return ret;
	}

	public double angleTo(Vector3D b) {
		if (abs() == 0 || b.abs() == 0) {
			throw new NullPointerException("cannot calculate angle to null vector");
		}

		double cosAngle = multiply(b) / (abs() * b.abs());
		double angle = Math.acos(cosAngle);

		return angle;
	}

	public static Vector3D vx() {
		return new Vector3D(1.0, 0.0, 0.0);
	}

	public static Vector3D vy() {
		return new Vector3D(0.0, 1.0, 0.0);
	}

	public static Vector3D vz() {
		return new Vector3D(0.0, 0.0, 1.0);
	}

}
