package at.greenhive.bl.event;

import me.limespace.appbase.event.IEvent;

public class WaypointReceivedEvent implements IEvent {

	private final double lat;
	private final double lon;
	private final double alt;

	public WaypointReceivedEvent(double lat, double lon, double alt) {
		this.lat = lat;
		this.lon = lon;
		this.alt = alt;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public double getAlt() {
		return alt;
	}

}
