package at.greenhive.bl.util.math.geo2d;

import java.util.ArrayList;
import java.util.List;

public class Polygon2D {

	 public static class BoundingBox {
	    	
	        public double xMax = Double.MIN_VALUE;
	        public double xMin = Double.MAX_VALUE;
	        public double yMax = Double.MIN_NORMAL;
	        public double yMin = Double.MAX_VALUE;
	        
	        /**
	         * Update the bounds if the given point is outside the bounding box
	         *
	         * @param point
	         * @return <code>true</code> if the point was outside the bounding box, otherwise return <code>false</code>
	         */
	        public boolean updateBounds(Point2D point) {
	        	boolean updated = false;
	        	if (point.x > xMax) {
	                xMax = point.x;
	                updated = true;
	            } else if (point.x < xMin) {
	                xMin = point.x;
	                updated = true;
	            }
	            if (point.y > yMax) {
	                yMax = point.y;
	                updated = true;
	            } else if (point.y < yMin) {
	                yMin = point.y;
	                updated = true;
	            }
	            return updated;
	        }
	        
	        /**
	         * Check if the given point is inside the bounding box
	         *
	         * @param point
	         * @return <code>true</code> if the point in bounding box, otherwise return <code>false</code>
	         */
	       
	        public boolean isInside(Point2D point) {
	            if (point.x < xMin || point.x > xMax || point.y < yMin || point.y > yMax) {
	                return false;
	            }
	            return true;
	        }

	    }
	
     /**
     * Builder of the polygon
     */
    public static class Builder {
    	
        private List<Point2D> vertexList = new ArrayList<Point2D>();
        private List<Line2D> sideList = new ArrayList<Line2D>();
        private BoundingBox boundingBox = new BoundingBox();
        private boolean closed = false;

        /**
         * Add vertex points of the polygon.<br>
         * It is very important to add the vertexes by order, like you were drawing them one by one.
         *
         * @param point The vertex point
         * @return The builder
         */
        public Builder addVertex(Point2D point) {
            if (closed) {
                // each hole we start with the new array of vertex points
                vertexList = new ArrayList<Point2D>();
                closed = false;
            }

            boundingBox.updateBounds(point);
            vertexList.add(point);

            // add line (edge) to the polygon
            if (vertexList.size() > 1) {
                Line2D Line = new Line2D(vertexList.get(vertexList.size() - 2), point);
                sideList.add(Line);
            }

            return this;
        }

        /**
         * Close the polygon shape. This will create a new side (edge) from the <b>last</b> vertex point to the <b>first</b> vertex point.
         *
         * @return The builder
         */
        public Builder close() {
            validate();

            // add last Line
            sideList.add(new Line2D(vertexList.get(vertexList.size() - 1), vertexList.get(0)));
            closed = true;

            return this;
        }

        /**
         * Build the instance of the polygon shape.
         *
         * @return The polygon
         */
        public Polygon2D build() {
            validate();

            // in case you forgot to close
            if (!closed) {
                // add last Line
                sideList.add(new Line2D(vertexList.get(vertexList.size() - 1), vertexList.get(0)));
            }

            Polygon2D polygon = new Polygon2D(sideList, boundingBox);
            return polygon;
        }

        private void validate() {
            if (vertexList.size() < 3) {
                throw new IllegalStateException("Polygon must have at least 3 points");
            }
        }
    }

    /**
     * Check if the the given point is inside of the polygon.<br>
     *
     * @param point The point to check
     * @return <code>True</code> if the point is inside the polygon, otherwise return <code>False</code>
     */
    public boolean contains(Point2D point) {
        if (boundingBox.isInside(point)) {
            Line2D ray = createRay(point);
            int intersection = 0;
            for (Line2D side : sides) {
                if (intersect(ray, side)) {
                    // System.out.println("intersection++");
                    intersection++;
                }
            }

			/*
             * If the number of intersections is odd, then the point is inside the polygon
			 */
            if (intersection % 2 != 0) {
                return true;
            }
        }
        return false;
    }

    public List<Line2D> getSides() {
        return sides;
    }

    /**
     * By given ray and one side of the polygon, check if both lines intersect.
     *
     * @param ray
     * @param side
     * @return <code>True</code> if both lines intersect, otherwise return <code>False</code>
     */
    private boolean intersect(Line2D ray, Line2D side) {
    	Point2D intersectPoint = null;

        // if both vectors aren't from the kind of x=1 lines then go into
        if (!ray.isVertical() && !side.isVertical()) {
            // check if both vectors are parallel. If they are parallel then no intersection point will exist
            if (ray.getA() - side.getA() == 0) {
                return false;
            }

            double x = ((side.getB() - ray.getB()) / (ray.getA() - side.getA())); // x = (b2-b1)/(a1-a2)
            double y = side.getA() * x + side.getB(); // y = a2*x+b2
            intersectPoint = new Point2D(x, y);
        } else if (ray.isVertical() && !side.isVertical()) {
            double x = ray.getStart().x;
            double y = side.getA() * x + side.getB();
            intersectPoint = new Point2D(x, y);
        } else if (!ray.isVertical() && side.isVertical()) {
            double x = side.getStart().x;
            double y = ray.getA() * x + ray.getB();
            intersectPoint = new Point2D(x, y);
        } else {
            return false;
        }

        if (side.isInside(intersectPoint) && ray.isInside(intersectPoint)) {
            return true;
        }

        return false;
    }
    
	public Point2D getIntersectPoint(Line2D ray, Line2D side) {
		Point2D intersectPoint = null;

		// if both vectors aren't from the kind of x=1 lines then go into
		if (!ray.isVertical() && !side.isVertical()) {
			// check if both vectors are parallel. If they are parallel then no intersection
			// point will exist
			if (ray.getA() - side.getA() == 0) {
				return null;
			}

			double x = ((side.getB() - ray.getB()) / (ray.getA() - side.getA())); // x = (b2-b1)/(a1-a2)
			double y = side.getA() * x + side.getB(); // y = a2*x+b2
			intersectPoint = new Point2D(x, y);
		} else if (ray.isVertical() && !side.isVertical()) {
			double x = ray.getStart().x;
			double y = side.getA() * x + side.getB();
			intersectPoint = new Point2D(x, y);
		} else if (!ray.isVertical() && side.isVertical()) {
			double x = side.getStart().x;
			double y = ray.getA() * x + ray.getB();
			intersectPoint = new Point2D(x, y);
		} else {
			return null;
		}

		if (side.isInside(intersectPoint) && ray.isInside(intersectPoint)) {
			return intersectPoint;
		}

		return null;
	}

    /**
     * Create a ray. The ray will be created by given point and on point outside of the polygon.<br>
     * The outside point is calculated automatically.
     *
     * @param point
     * @return
     */
    private Line2D createRay(Point2D point) {
        // create outside point
        double epsilon = (boundingBox.xMax - boundingBox.xMin) / 10e6;
        Point2D outsidePoint = new Point2D(boundingBox.xMin - epsilon, boundingBox.yMin);

        Line2D vector = new Line2D(outsidePoint, point);
        return vector;
    }
    
    private final BoundingBox boundingBox;
    private final List<Line2D> sides;

    private Polygon2D(List<Line2D> sides, BoundingBox boundingBox) {
        this.sides = sides;
        this.boundingBox = boundingBox;
    }

    /**
     * Get the builder of the polygon
     *
     * @return The builder
     */
    public static Builder Builder() {
        return new Builder();
    }

    
   
}
