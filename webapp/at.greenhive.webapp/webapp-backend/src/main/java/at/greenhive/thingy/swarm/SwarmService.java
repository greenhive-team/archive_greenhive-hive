package at.greenhive.thingy.swarm;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import at.greenhive.bl.IReadOnlyService;

@Component
public class SwarmService implements IReadOnlyService<Swarm> {

	@Autowired
	SwarmManager swarmManager;
	
	@Override
	public Collection<Swarm> getAll() {
		return swarmManager.getSwarmList();
	}
	
	public Swarm getSwarm(String swarmid) {
		return swarmManager.getSwarm(swarmid);
	}
 	
	public Collection<Drone> getBumblebees(String swarmId) {
		
		Swarm swarm = swarmManager.getSwarm(swarmId);
		if (swarm == null) {
			return Collections.emptyList();
		}

		return swarm.getDrones();
	}

	public Drone getBumblebee(String swarmId, String beeId) {

		Swarm swarm = swarmManager.getSwarm(swarmId);
		if (swarm == null) {
			return null;
		}

		return swarm.getDrone(beeId);
	}
	
}
