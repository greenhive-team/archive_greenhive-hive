package at.greenhive.thingy.missionplaner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.javatuples.Triplet;

import at.greenhive.bl.hive.HiveService;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.bl.util.geo.GeoPoint;
import at.greenhive.bl.util.geo.GraphLoader;
import at.greenhive.bl.util.geo.MapGraph;
import at.greenhive.bl.util.math.geo2d.Point2D;
import at.greenhive.thingy.cropland.CropType;
import at.greenhive.thingy.cropland.Cropland;
import at.greenhive.thingy.cropland.CroplandPath;
import at.greenhive.thingy.cropland.CroplandRow;
import at.greenhive.thingy.missionplaner.WorkPath.DistanceResult;
import at.greenhive.thingy.missionplaner.mission.MissionData;
import at.greenhive.thingy.missionplaner.mission.MissionSprayTask;
import at.greenhive.thingy.missionplaner.mission.MissionTask;
import at.greenhive.thingy.missionplaner.mission.MissionTaskState;
import at.greenhive.thingy.missionplaner.mission.MissionTransitTask;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationData;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;
import at.greenhive.thingy.missionplaner.segmentation.SegmentationStrategy;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TObjectIntProcedure;

public class MissionPlannerEngine {

	private final static double EPSILON = 0.000000001;

	private static class SprayTaskResult {
		MissionTaskState state;
		MissionSprayTask sprayTask;

		public SprayTaskResult(MissionTaskState state, MissionSprayTask sprayTask) {
			super();
			this.state = state;
			this.sprayTask = sprayTask;
		}

		public MissionTaskState getState() {
			return state;
		}

		public MissionSprayTask getSprayTask() {
			return sprayTask;
		}
	}

	public WorkPatch createFromCropland(HiveCoordinateConverter converter, Cropland cropland,
			OperationCroplandParameter croplandMissionParm) {

		WorkPatch wp = new WorkPatch();

		WorkPath outline = new WorkPath(0);
		for (GeoPoint geoPoint : cropland.getOutline()) {
			outline.addPathPoint(converter.toRelative(geoPoint));
		}
		wp.setBoundPath(outline);

		List<WorkPath> transitPaths = new ArrayList<>(cropland.getPaths().size());

		int transId = 0;
		for (CroplandPath path : cropland.getPaths()) {
			WorkPath transit = new WorkPath(transId++);
			transitPaths.add(transit);
			for (GeoPoint geoPoint : path.getPath()) {
				WorkCoordinate transitPoint = converter.toRelative(geoPoint);
				transit.addPathPoint(transitPoint);
			}
		}

		wp.setTransitPath(transitPaths);

		for (CroplandRow row : cropland.getRows()) {
			int rowId = row.getId();
			List<GeoPoint> path = row.getPath();

			WorkPath workPath = new WorkPath(rowId);
			wp.addPath(workPath);

			for (GeoPoint point : path) {
				workPath.addPathPoint(converter.toRelative(point));
			}
		}

		return wp;
	}

//	private Pair<Double, Line2D> getMinDist(WorkCoordinate pathPoint, List<Line2D> transitSegments) {
//		Point2D p = new Point2D(pathPoint.x, pathPoint.y);
//		double min = Double.MAX_VALUE;
//		Line2D minDistSeg = null;
//		for (Line2D seg : transitSegments) {
//			double distance = seg.distance(p);
//			if (distance < min) {
//				min = distance;
//				minDistSeg = seg;
//			}
//		}
//		return Pair.with(min, minDistSeg);
//	}

	public List<IWorkSegment> getSegments(WorkPatch patch, CropType croptype,
			OperationCroplandParameter croplandMissionParm, OperationParameter operationParameter) {
		List<IWorkSegment> segments = new LinkedList<>();

		SegmentationStrategy ss = SegmentationStrategy.fromCropType(croptype, croplandMissionParm, operationParameter);
		segments = ss.createSegments(patch);

		return segments;
	}

	public OperationData createOperationData(HiveCoordinateConverter converter, List<IWorkSegment> segments,
			List<WorkPath> transitPaths, OperationCroplandParameter operationCroplandParm, GPSPoint hiveLocation) {

		OperationData operationData = new OperationData(-1);
		int taskId = 0;

		for (IWorkSegment ws : segments) {
			taskId++;

			MissionTask t = new MissionSprayTask(taskId, operationCroplandParm.getSprayHeight(), ws);
			operationData.addTask(t);
		}
		
		List<WorkCoordinate> endPoints = findEndPoints(segments);
		endPoints.add( converter.toRelative(hiveLocation));
		
		List<WorkPath> transitPathsSplit = splitTransitPath(transitPaths,endPoints);
		
		operationData.setTransitPaths(transitPathsSplit);

		return operationData;
	}

	private List<WorkPath> splitTransitPath(List<WorkPath> transitPaths, List<WorkCoordinate> endPoints) {

		List<WorkPath> tmpPaths = new ArrayList<>();
		for (WorkCoordinate c: endPoints) {
			double min = Double.MAX_VALUE;
			WorkPath minPath = null;
			Point2D projectedPoint = null;
			
			for (WorkPath path: transitPaths) {
				DistanceResult distance = path.getDistance(c);
				if(distance != null) {
					if (distance.getDistance() < min) {
						min = distance.getDistance();
						projectedPoint = distance.getProjectedPoint();
						minPath = path;
					}
				}
			}
			if (minPath != null) {
				WorkCoordinate insert = minPath.insertSplit(projectedPoint);
				if (insert != null) {
					WorkPath newPath = new WorkPath(4711);
					newPath.addPathPoint(c);
					newPath.addPathPoint(insert);
					tmpPaths.add(newPath);
				}
			}
		}
		
		tmpPaths.addAll(transitPaths);
		return tmpPaths;
	}

	private List<WorkCoordinate> findEndPoints(List<IWorkSegment> segments) {
		
		TObjectIntHashMap<WorkCoordinate> cooCount = new TObjectIntHashMap<>();
		for (IWorkSegment ws : segments) {
			cooCount.adjustOrPutValue(ws.getStartPoint(),1,1);
			cooCount.adjustOrPutValue(ws.getEndPoint(),1,1);
		}
		
		List<WorkCoordinate> endPoints = new ArrayList<>();
		cooCount.forEachEntry( new TObjectIntProcedure<WorkCoordinate>() {
			@Override
			public boolean execute(WorkCoordinate coordiante, int count) {
				if (count == 1 ) {
					endPoints.add(coordiante);
				}
				return true;
			}
		}); 
			
		return endPoints;
		
	}

	public MissionData getMissionData(int missionId, String droneID, double capacity, boolean needsTransit,
			double maxSpeed, OperationData operationData, HiveCoordinateConverter converter, GPSPoint hiveLocation) {

		MissionData missionData = new MissionData(missionId, droneID, maxSpeed);

		SprayTaskResult result = findFirstTaskState(capacity, operationData, converter);

		double remainingCapacity = capacity;

		while (result != null) {

			result.getState().setPlannedDroneId(droneID);
			missionData.addTask(result.getSprayTask(), result.getState());

			operationData.addTaskState(result.getState());

			remainingCapacity -= result.getSprayTask().getSprayAmount();
			result = findNextBestTask(remainingCapacity, operationData, result.getSprayTask());
		}

		if (missionData.getFirstTask() != null && needsTransit) {
			List<MissionTask> toStart = findTransitToFirstTask(missionData.getFirstTask(), operationData, converter, hiveLocation);
			List<MissionTask> toHive = findTransitToHive(missionData.getLastTask(), operationData, converter, hiveLocation);

			for (MissionTask t:toStart) {
				MissionTaskState state = new MissionTaskState(t.getTaskId());
				state.setPlannedDroneId(droneID);
				missionData.addTransitToFirst(t, state);
			}
			
			for (MissionTask t:toHive) {
				MissionTaskState state = new MissionTaskState(t.getTaskId());
				state.setPlannedDroneId(droneID);
				missionData.addTransitToHive(t, state);
			}
		}

		return missionData;
	}

	private List<MissionTask> findTransitToHive(MissionTask lastTask, OperationData operationData,
			HiveCoordinateConverter converter,GPSPoint hiveLocation) {
		MapGraph map = createMap(operationData, converter);

		int taskId = lastTask.getTaskId() + 1;
		List<GeoPoint> path = map.aStarSearch(lastTask.getEnd(converter), hiveLocation);
		if (path != null && !path.isEmpty()) {
			List<MissionTask> result = ceateTransitTasks(converter, taskId, path);
			return result;
		}

		return Collections.emptyList();
	}

	private List<MissionTask> findTransitToFirstTask(MissionTask firstTask, OperationData operationData,
			HiveCoordinateConverter converter, GPSPoint hiveLocation) {

		MapGraph map = createMap(operationData, converter);
		List<GeoPoint> path = map.aStarSearch(hiveLocation, firstTask.getStart(converter));
		if (path != null && !path.isEmpty()) {

			
			int taskId = operationData.getTasks()
										.stream()
										.mapToInt(t -> t.getTaskId())
										.max()
										.orElseThrow(NoSuchElementException::new) + 1;

			List<MissionTask> result = ceateTransitTasks(converter, taskId, path);

			return result;
		}
		return Collections.emptyList();
	}

	private List<MissionTask> ceateTransitTasks(HiveCoordinateConverter converter, int taskId, List<GeoPoint> path) {
		List<MissionTask> result = new ArrayList<>(path.size());
		Iterator<GeoPoint> iPath = path.iterator();
		GeoPoint start = iPath.next();
		while (iPath.hasNext()) {
			GeoPoint end = iPath.next();
			DirectionWorkSegment ws = new DirectionWorkSegment();
			ws.addPathPoint(converter.toRelative(start));
			ws.addPathPoint(converter.toRelative(end));
			MissionTask t = new MissionTransitTask(taskId++, ws);
			result.add(t);
			start = end;
		}
		return result;
	}

	private MapGraph createMap(OperationData operationData, HiveCoordinateConverter converter) {

		List<Triplet<GeoPoint, GeoPoint, String>> pathList = new ArrayList<>();

		for (MissionTask t : operationData.getTasks()) {
			pathList.add(Triplet.with(t.getStart(converter), t.getEnd(converter), "TSK-F" + t.getTaskId()));
			pathList.add(Triplet.with(t.getEnd(converter), t.getStart(converter), "TSK-R" + t.getTaskId()));
		}

		int transID = 1;
		for (WorkPath path : operationData.getTransitPaths()) {

			Iterator<WorkCoordinate> iPath = path.getPath().iterator();
			WorkCoordinate start = iPath.next();
			while (iPath.hasNext()) {
				WorkCoordinate end = iPath.next();
				pathList.add(Triplet.with(converter.toAbsolut(start), converter.toAbsolut(end), "TRA-F" + transID));
				pathList.add(Triplet.with(converter.toAbsolut(end), converter.toAbsolut(start), "TRA-R" + transID));
				start = end;
			}
			transID++;
		}

		return GraphLoader.createMap(pathList);
	}

	private SprayTaskResult findFirstTaskState(double capacity, OperationData operationData,
			HiveCoordinateConverter converter) {

		GeoPoint hiveLocation = HiveService.getHiveLocation();

		WorkCoordinate relativeHiveLocation = converter.toRelative(hiveLocation);

		double distanceToHive = Double.MAX_VALUE;
		SprayTaskResult candidate = null;

		for (MissionTask task : operationData.getTasks()) {
			if (task instanceof MissionSprayTask) {
				MissionSprayTask sprayTask = (MissionSprayTask) task;
				int taskId = sprayTask.getTaskId();
				MissionTaskState state = operationData.getTaskState(taskId);
				if (state == null) {
					state = new MissionTaskState(taskId);
				}

				if (!state.getTaskState().isPlanned()) {
					if (sprayTask.getSprayAmount() < capacity) {

						double taskDistanceToHive = getDistanceToHive(relativeHiveLocation, sprayTask);
						if (taskDistanceToHive < distanceToHive) {
							candidate = new SprayTaskResult(state, sprayTask);
							distanceToHive = taskDistanceToHive;
						}
					}
				}
			}
		}
		if (candidate != null) {
			candidate.getSprayTask().setStartRelatedTo(relativeHiveLocation);
		}
		return candidate;
	}

	private SprayTaskResult findNextBestTask(double capacity, OperationData operationData,
			MissionSprayTask previousTask) {

		SprayTaskResult candidate = null;
		double distanceToCandidate = Double.MAX_VALUE;

		for (MissionTask task : operationData.getTasks()) {
			if (task instanceof MissionSprayTask) {
				MissionSprayTask sprayTask = (MissionSprayTask) task;
				int taskId = sprayTask.getTaskId();
				MissionTaskState state = operationData.getTaskState(taskId);
				if (state == null) {
					state = new MissionTaskState(taskId);
				}

				if (!state.getTaskState().isPlanned()) {
					if (sprayTask.getSprayAmount() < capacity) {

						double dist = getDistance(previousTask, sprayTask);
						if (Math.abs(dist) < EPSILON) {
							sprayTask.setStartRelatedTo(previousTask.getEndCoordiante());
							return new SprayTaskResult(state, sprayTask);
						} else if (dist < distanceToCandidate) {
							candidate = new SprayTaskResult(state, sprayTask);
							distanceToCandidate = dist;
						}
					}
				}
			}
		}
		if (candidate != null) {
			candidate.getSprayTask().setStartRelatedTo(previousTask.getEndCoordiante());
		}
		return candidate;
	}

	private double getDistance(MissionSprayTask previousTask, MissionSprayTask sprayTask) {
		return previousTask.getWorkSegement().getDistance(sprayTask.getWorkSegement());
	}

	private double getDistanceToHive(WorkCoordinate hiveCoordinate, MissionSprayTask sprayTask) {
		return sprayTask.getWorkSegement().getDistance(hiveCoordinate);
	}

}
