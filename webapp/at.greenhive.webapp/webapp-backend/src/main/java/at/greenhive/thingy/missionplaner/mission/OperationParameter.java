  package at.greenhive.thingy.missionplaner.mission;

import at.greenhive.bl.util.geo.GeoPoint;

public class OperationParameter {
	
	GeoPoint hiveLocation;
	
	public int timeToRefill = 60;  //sec
	public int concurrentRefillCount = 2;
	public int droheAirTimeMax = 30 * 60; //sec
	public int spraySpeed = 2; // m/s
	public int travelSpeed = 20; // m/s
	public int travelHeight = 10; //m
	public double sprayWidth = 2; // m
	
	//public int maxLoad = 10000; // ml

	public int fillReserve = 500; //ml 

	public OperationParameter() {
		
	}
	
	public GeoPoint getHiveLocation() {
		return hiveLocation;
	}

	public double getFillReserve() {
		return fillReserve;
	}
	
	public double getSpraySpeed() {
		return spraySpeed;
	}
	
	public double getSprayWidth() {
		return sprayWidth;
	}
}