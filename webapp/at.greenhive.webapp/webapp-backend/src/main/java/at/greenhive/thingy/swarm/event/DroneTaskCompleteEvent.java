package at.greenhive.thingy.swarm.event;

public class DroneTaskCompleteEvent extends DroneEvent {

	private final int taskId;
	private double sprayAmount;

	public DroneTaskCompleteEvent(String droneId, int taskId, double sprayAmount) {
		super(droneId);
		this.taskId = taskId;
		this.sprayAmount = sprayAmount;
	}

	public int getTaskId() {
		return taskId;
	}
	
	public double getSprayAmount() {
		return sprayAmount;
	}
}
