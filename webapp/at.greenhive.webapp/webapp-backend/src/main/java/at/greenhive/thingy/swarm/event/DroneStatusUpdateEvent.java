package at.greenhive.thingy.swarm.event;

import at.greenhive.protobuf.Types.DroneStatus;

public class DroneStatusUpdateEvent extends DroneEvent {

	
	private double altitute;
	private double longitute;
	private double latitude;
	
	private double battery;
	private DroneStatus status;
	private long fillLevel;

	public DroneStatusUpdateEvent(String droneId) {
		super(droneId);
		
	}
	
	public void setBattery(double battery) {
		this.battery = battery;
	}
	
	public double getBattery() {
		return battery;
	}
	
	public void setStatus(DroneStatus status) {
		this.status = status;
	}
	
	public DroneStatus getStatus() {
		return status;
	}

	public void setPositon(double latitude, double longitude, double altitude) {
		this.latitude = latitude;
		this.longitute = longitude;
		this.altitute = altitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitute() {
		return longitute;
	}
	
	public double getAltitute() {
		return altitute;
	}
	
	public long getFillLevel() {
		return fillLevel;
	}
	
	public void setFillLevel(long fillLevel) {
		this.fillLevel = fillLevel;
	}
}
