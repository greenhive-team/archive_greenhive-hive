package at.greenhive.thingy.missionplaner.mission;

import at.greenhive.thingy.missionplaner.IWorkSegment;

public class MissionSprayTask extends MissionTask {

	private double height;
	
	public MissionSprayTask(int taskId, double height, IWorkSegment ws) {
		super( taskId, ws);
		this.height = height;
	}

	public double getHeight() {
		return height;
	}

	public double getRate() {
		return workSegement.getRate();
	}
	
	public IWorkSegment getWorkSegement() {
		return workSegement;
	}
	
	public double getSprayAmount() {
		return workSegement.getSprayAmount();
	}
	
	@Override
	public String toString() {
		return "MissionSprayTask [taskId=" + taskId 
				+ ", height=" + height 
				+ "]";
	}

	@Override
	public String getType() {
		return "Spray";
	}
}