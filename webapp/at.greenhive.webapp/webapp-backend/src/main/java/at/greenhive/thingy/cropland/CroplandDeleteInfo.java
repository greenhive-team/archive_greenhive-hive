package at.greenhive.thingy.cropland;

public class CroplandDeleteInfo {

	private final int deletedCropland;
	private final int deletedCroplandBounds;
	private final int deletedCroplandRows;

	public CroplandDeleteInfo(int del_cropland, int del_croplandBounds, int del_croplandRows) {
		this.deletedCropland = del_cropland;
		this.deletedCroplandBounds = del_croplandBounds;
		this.deletedCroplandRows = del_croplandRows;
	}
	
	public int getDeletedCropland() {
		return deletedCropland;
	}
	
	public int getDeletedCroplandBounds() {
		return deletedCroplandBounds;
	}
	
	public int getDeletedCroplandRows() {
		return deletedCroplandRows;
	}
	

}
