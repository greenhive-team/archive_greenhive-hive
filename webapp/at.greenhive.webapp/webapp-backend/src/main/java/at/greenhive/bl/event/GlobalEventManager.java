package at.greenhive.bl.event;

import me.limespace.appbase.event.EventManager;
import me.limespace.appbase.event.IEvent;
import me.limespace.appbase.event.IEventListener;
import me.limespace.appbase.event.IEventSource;

public class GlobalEventManager {

	private static EventManager instance;

	private static EventManager instance() {

		if (instance == null) {
			instance = new EventManager();
		}

		return instance;
	}

	public static void registerListener(IEventListener listener) {
		instance().registerListener(listener);
	}

	public static void unregisterListener(IEventListener listener) {
		instance().unregisterListener(listener);
	}

	public static void fire(IEvent event, IEventSource source) {
		instance().fire(event, source);
	}
	
	

}
