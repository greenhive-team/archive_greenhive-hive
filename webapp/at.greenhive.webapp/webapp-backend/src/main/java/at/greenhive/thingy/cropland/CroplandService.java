package at.greenhive.thingy.cropland;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.greenhive.bl.IReadOnlyService;
import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.bl.util.geo.GeoPoint;

@Service
public class CroplandService implements IReadOnlyService<CroplandMetadata> {
	
	@Autowired
	CroplandRepo repo;
	
	@Override
	public List<CroplandMetadata> getAll() {
		return repo.loadCroplandList();
	}
	
	public List<Cropland> getDetailedCroplandList() {
	    List<Cropland> croplands = new LinkedList<Cropland>();
	    
	    for (CroplandMetadata clm: repo.loadCroplandList()) {
	        List<GeoPoint> clOutline = repo.loadCroplandOutline(clm.getClid());
	        List<CroplandRow> clRows = repo.loadCroplandRows(clm.getClid());
	        List<CroplandPath> clPath = repo.loadCroplandPaths(clm.getClid());
	        GeoPoint hiveLocation = repo.loadHiveLocation(clm.getClid());
	        croplands.add(new Cropland(clm, clOutline, clRows,clPath,hiveLocation));
	    }
	    
	    return croplands;
	}

	public Cropland getCroplandDetail(int id) {
		
		CroplandMetadata clm = repo.loadCroplandDetail(id);
		List<GeoPoint> clOutline = repo.loadCroplandOutline(id);
		List<CroplandRow> clRows = repo.loadCroplandRows(id);
		List<CroplandPath> clPath = repo.loadCroplandPaths(id);
		GeoPoint hiveLocation = repo.loadHiveLocation(id);
		return new Cropland(clm, clOutline, clRows,clPath, hiveLocation);
	}

	public List<GeoPoint> getCroplandOutline(int id) {
		return  repo.loadCroplandOutline(id);
	}
	
	public List<CroplandRow> getCroplandRows(int id) {
		return repo.loadCroplandRows(id);
	}

	public int[] updateCroplands(List<CroplandMetadata> input) {
		return repo.updateCroplandDetail(input);
	}

	public int[] insertCroplands(List<CroplandMetadata> input) {
		return repo.insertCroplandDetail(input);
	}
	
	public int[] replaceAllCroplands(List<CroplandMetadata> input) {
		repo.deleteAllCroplands();
		return repo.insertCroplandDetail(input);
	}
	
	public int[] replaceAllCroplandRows(List<CroplandRowBean> croplandRowBeans) {
		repo.deleteAllCroplandRows();
		return repo.insertCroplandRows(croplandRowBeans);
	}
	
	public int[] replaceAllCroplandBounds(List<CroplandBoundBean> input) {
		repo.deleteAllCroplandBounds();
		return repo.insertCroplandBounds(input);
	}

	public CroplandDeleteInfo deleteCropland(int clid) {
		return repo.deleteCropland(clid);
	}

	

	
	
}
