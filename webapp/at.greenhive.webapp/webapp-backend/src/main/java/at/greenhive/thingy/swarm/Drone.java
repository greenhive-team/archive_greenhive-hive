package at.greenhive.thingy.swarm;

public abstract class Drone {

	enum DroneType { ANT, BUMBLEBEE}
	
	protected String serialNumber;
	String name;
	protected DroneState state;

	public Drone() {
		super();
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setState(DroneState state) {
		this.state = state;
	}

	public DroneState getState() {
		return state;
	}

	public abstract double getCapacity();
	public abstract DroneType getType();

	public abstract boolean needsTransit();
}