package at.greenhive.thingy.operation;

public class OperationStateChanged extends OperationEvent {

	
	private OperationState state;

	public OperationStateChanged(OperationState state) {
		this.state = state;
	}

	public OperationState getState() {
		return state;
	}
}
