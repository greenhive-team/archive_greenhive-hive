package at.greenhive.bl.util.math.geo2d;

public class Line2D {
    
    private final Point2D s;
    private final Point2D e;
    
    private double a;
    private double b;
    private boolean vertical = false;

    public Line2D(Point2D start, Point2D end) {
        this.s = start;
        this.e = end;

        if (e.x - s.x != 0) {
            a = ((e.y - s.y) / (e.x - s.x));
            b = s.y - a * s.x;
        } else {
            vertical = true;
        }
    }

    /**
     * Indicate whereas the point lays on the line.
     *
     * @param point - The point to check
     * @return {@code true} if the point lays on the line, otherwise return {@code false}
     */
    public boolean isInside(Point2D point) {
        double maxX = s.x > e.x ? s.x : e.x;
        double minX = s.x < e.x ? s.x : e.x;
        double maxY = s.y > e.y ? s.y : e.y;
        double minY = s.y < e.y ? s.y : e.y;

        if ((point.x >= minX && point.x <= maxX) && (point.y >= minY && point.y <= maxY)) {
            return true;
        }
        return false;
    }

    /** 
     * Calculates the distance from the given point to the line
     * @see https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
     */
    public double distance(Point2D point) {
    	return Math.abs((e.y - s.y)*point.x - (e.x-s.x)*point.y + e.x*s.y - e.y*s.x) / Math.sqrt( Math.pow((e.y-s.y),2) + Math.pow(e.x-s.x,2)); 
    }

    /**
     * Calculates perpendicular projection coordinates of p onto this line
     * @param p - point to be projected
     * @return Point on the line
     * @see http://www.sunshine2k.de/coding/java/PointOnLine/PointOnLine.html

     */
    
    public Point2D getProjectedPointOnLine(Point2D p)
    {
      // get dot product of e1, e2
      Point2D e1 = new Point2D(e.x - s.x, e.y - s.y);
      Point2D e2 = new Point2D(p.x - s.x, p.y - s.y);
      double valDp = e1.dot(e2);
      // get length of vectors
      double len1 = Math.sqrt(e1.x * e1.x + e1.y * e1.y);
      double len2 = Math.sqrt(e2.x * e2.x + e2.y * e2.y);
      double cos = valDp / (len1 * len2);
      // length of v1P'
      double projLenOfLine = cos * len2;
      Point2D p2 = new Point2D(s.x + (projLenOfLine * e1.x) / len1,
                              s.y + (projLenOfLine * e1.y) / len1);
      return p2;
    }


    
    
    /**
     * Indicate whereas the line is vertical. <br>
     * For example, line like x=1 is vertical, in other words parallel to axis Y. <br>
     * In this case the A is (+/-)infinite.
     *
     * @return {@code true} if the line is vertical, otherwise return {@code false}
     */
    public boolean isVertical() {
        return vertical;
    }

    /**
     * y = <b>A</b>x + B
     *
     * @return The <b>A</b>
     */
    public double getA() {
        return a;
    }

    /**
     * y = Ax + <b>B</b>
     *
     * @return The <b>B</b>
     */
    public double getB() {
        return b;
    }

    /**
     * Get start point
     *
     * @return The start point
     */
    public Point2D getStart() {
        return s;
    }

    /**
     * Get end point
     *
     * @return The end point
     */
    public Point2D getEnd() {
        return e;
    }

    @Override
    public String toString() {
        return String.format("%s-%s", s.toString(), e.toString());
    }
}