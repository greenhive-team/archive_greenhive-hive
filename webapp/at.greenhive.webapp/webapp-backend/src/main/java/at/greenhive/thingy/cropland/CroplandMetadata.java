package at.greenhive.thingy.cropland;

public class CroplandMetadata {

	private int clid;

	private CropType croptype;
	private String name;
	private String description;

	private double defaultSprayHeight;

	private double maxSpeed;

	public CroplandMetadata(int id) {
		this.clid = id;
	}

	public int getClid() {
		return clid;
	}

	public void setCroptype(CropType croptype) {
		this.croptype = croptype;
	}

	public CropType getCroptype() {
		return croptype;
	}

	public void setDefaultSprayHeight(double defaultSprayHeight) {
		this.defaultSprayHeight = defaultSprayHeight;
	}
	
	public double getDefaultSprayHeight() {
		return defaultSprayHeight;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
}
