package at.greenhive.bl.util.geo;

import com.peertopark.java.geocalc.Coordinate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "longitude", "latitude", "altitude" })
public class GeoPoint {
	Coordinate longitude;
	Coordinate latitude;
	long altitude;

	public GeoPoint(Coordinate latitude, Coordinate longitude, long altitude) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
	}

	@JsonIgnore
	public Coordinate getLongitude() {
		return longitude;
	}

	@JsonIgnore
	public Coordinate getLatitude() {
		return latitude;
	}

	@JsonGetter("altitude")
	public long getAltitude() {
		return altitude;
	}

	@JsonGetter("longitude")
	public double getLongitudeDecimalDegrees() {
		return this.longitude.getDecimalDegrees();
	}

	@JsonGetter("latitude")
	public double getLatitudeDecimalDegrees() {
		return this.latitude.getDecimalDegrees();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (altitude ^ (altitude >>> 32));
		result = prime * result + ((latitude == null) ? 0 : Double.hashCode(latitude.getDecimalDegrees()));
		result = prime * result + ((longitude == null) ? 0 : Double.hashCode(longitude.getDecimalDegrees()));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeoPoint other = (GeoPoint) obj;
		if (altitude != other.altitude)
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (latitude.getDecimalDegrees() != other.latitude.getDecimalDegrees())
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (longitude.getDecimalDegrees() != other.longitude.getDecimalDegrees())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GeoPoint (lon=" + longitude.getDecimalDegrees() + ", lat=" + latitude.getDecimalDegrees() + ", alt=" + altitude + ")";
	}
	
	
	
}
