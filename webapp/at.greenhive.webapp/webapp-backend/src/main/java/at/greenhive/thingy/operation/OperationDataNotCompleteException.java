package at.greenhive.thingy.operation;

public class OperationDataNotCompleteException extends Exception {
	private static final long serialVersionUID = 1L;

	public enum Reason {
		MISSING_MISSIONDATA, MISSING_SWARM
	}

	private Reason reason;
	
	public OperationDataNotCompleteException(Reason reason) {
		this.reason = reason;
	}
	
	public Reason getReason() {
		return reason;
	}
}
