package at.greenhive.thingy.swarm;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Swarm {
	
	String id;
	String name;
	
	private Map<String, Drone> drone = new HashMap<String, Drone>();
	
	public Swarm(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public Collection<Drone> getDrones() {
		return drone.values();
	}

	public void addDrone(Drone bumblebee) {
		drone.put(bumblebee.getSerialNumber(), bumblebee);
	}
	
	public void addDrones(List<? extends Drone> bees) {
		for (Drone bee:bees) {
			drone.put(bee.getSerialNumber(), bee);
		}
	}
	
	public Drone getDrone(String serialNo) {
		return drone.get(serialNo);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
