package at.greenhive.thingy.cropland;

public enum CropType {
	 WINE ,STRAWBERRY ,PUMPKIN ;
	 
	 private static CropType[] allValues = values();
	 public static CropType fromOrdinal(int n) {return allValues[n];}
}
