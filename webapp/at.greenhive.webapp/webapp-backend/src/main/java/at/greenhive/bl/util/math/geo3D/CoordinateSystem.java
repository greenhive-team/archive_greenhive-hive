/* Copyright (C) 1999-2003 by Peter Eastman

 This program is free software; you can redistribute it and/or modify it under the
 terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY 
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 PARTICULAR PURPOSE.  See the GNU General Public License for more details. 
 
 adapted for Nammu by Christian Semmelrath
 */

package at.greenhive.bl.util.math.geo3D;

/** The CoordinateSystem class describes the position and orientation of one coordinate
 system relative to another one.  It is defined by three vectors.  orig defines the position
 of the origin of the new coordinate system, while zdir and updir define the directions
 of the z and y axes, respectively.  (Note that the y axis will only be parallel to
 updir in the special case that zdir and updir are perpendicular to each other.) 
 Alternatively, the orientation can be represented by three rotation angles about
 the (global) x, y, and z axes.  Both representations are maintained internally. */

public class CoordinateSystem  {
	final Vector3D orig;
	final Vector3D rot;
	Vector3D zdir, updir;
	Matrix4 transformTo, transformFrom;
	
		/** Create a new CoordinateSystem which represents an identity transformation (i.e. no
	 translation or rotation). */

	public CoordinateSystem() {
		this(new Vector3D(), Vector3D.vz(), Vector3D.vy());
	}

	/** Create a new coordinate system.
	 @param orig     the origin of the new coordinate system
	 @param zdir     the direction of the new coordinate system's z axis
	 @param updir    defines the "up" direction.  If this is perpendicular to zdir, this will be
	 the y axis direction of the new coordinate system.
	 */

	public CoordinateSystem(Vector3D orig, Vector3D zdir, Vector3D updir) {
		this.orig = orig;
		this.zdir = zdir;
		this.updir = updir;
		zdir.normalise();
		updir.normalise();
		rot = new Vector3D();
		findRotationAngles();
	}

	/** Create a new coordinate system.
	 @param orig     the origin of the new coordinate system
	 @param x        the rotation angle around the x axis
	 @param y        the rotation angle around the y axis
	 @param z        the rotation angle around the z axis
	 */

	public CoordinateSystem(Vector3D orig, double x, double y, double z) {
		this.orig = orig;
		rot = new Vector3D();
		setOrientation(x, y, z);
	}

	

	/** Create an exact duplicate of this CoordinateSystem. */

	public final CoordinateSystem duplicate() {
		CoordinateSystem coords = new CoordinateSystem(new Vector3D(orig), new Vector3D(zdir), new Vector3D(updir));
		coords.rot.val[0] = rot.val[0];
		coords.rot.val[1] = rot.val[1];
		coords.rot.val[2] = rot.val[2];
		return coords;
	}

	/** Make this CoordianteSystem identical to another one. */

	public  void copyCoords(CoordinateSystem c) {
		setOrigin(new Vector3D(c.orig));
		setOrientation(new Vector3D(c.zdir), new Vector3D(c.updir));
		rot.val[0] = c.rot.val[0];
		rot.val[1] = c.rot.val[1];
		rot.val[2] = c.rot.val[2];
	}

	/** Determine whether this coordinate system is identical to another one. */

	public  boolean equals(Object coords) {
		CoordinateSystem c = (CoordinateSystem) coords;
		if (!orig.equals(c.orig))
			return false;
		if (!zdir.equals(c.zdir))
			return false;
		if (!updir.equals(c.updir))
			return false;
		return true;
	}

	/** Set the position of this CoordinateSystem's origin. */

	public  void setOrigin(Vector3D orig) {
		this.orig.set(orig);
		transformTo = transformFrom = null;
	}

	/** Set the orientation of this CoordinateSystem.
	 @param zdir     the direction of this coordinate system's z axis
	 @param updir    defines the "up" direction.  If this is perpendicular to zdir, this will become
	 the y axis direction.
	 */

	public  void setOrientation(Vector3D zdir, Vector3D updir) {
		this.zdir = zdir;
		this.updir = updir;
		zdir.normalise();
		updir.normalise();
		findRotationAngles();
		transformTo = transformFrom = null;
	}

	/** Set the orientation of this CoordinateSystem.
	 @param x        the rotation angle around the x axis
	 @param y        the rotation angle around the y axis
	 @param z        the rotation angle around the z axis
	 */

	public  void setOrientation(double x, double y, double z) {
		Matrix4 m;

		rot.val[0] = x * Math.PI / 180.0;
		rot.val[1] = y * Math.PI / 180.0;
		rot.val[2] = z * Math.PI / 180.0;
		
		m = Matrix4.yrotation(-rot.val[1]).multiply(Matrix4.xrotation(-rot.val[0])).multiply(Matrix4.zrotation(-rot.val[2]));
		zdir = m.multiply(Vector3D.vz());
		updir = m.multiply(Vector3D.vy());
		transformTo = transformFrom = null;
	}
	
	public  void updateOrientation() {
		Matrix4 m;

		//rot.X[0] = x ;
		//rot.X[1] = y ;
		//rot.X[2] = z ;
		
		m = Matrix4.yrotation(-rot.val[1]).multiply(Matrix4.xrotation(-rot.val[0])).multiply(Matrix4.zrotation(-rot.val[2]));
		zdir = m.multiply(Vector3D.vz());
		updir = m.multiply(Vector3D.vy());
		transformTo = transformFrom = null;
	}

	/** Get the origin of this CoordinateSystem. */

	public final Vector3D getOrigin() {
		return orig;
	}

	/** Get this CoordinateSystem's z axis direction. */

	public final Vector3D getZDirection() {
		return zdir;
	}

	/** Get the vector used to define "up" in this CoordinateSystem (usually but not always the y axis direction). */

	public final Vector3D getUpDirection() {
		return updir;
	}

	/** Return the x, y, and z rotation angles.  */

	public final double[] getRotationAngles() {
		return new double[] { rot.val[0] * 180.0 / Math.PI, rot.val[1] * 180.0 / Math.PI, rot.val[2] * 180.0 / Math.PI };
	}

	/** Transform this CoordinateSystem's orientation by applying a matrix to its axis directions. */

	public final void transformAxes(Matrix4 m) {
		zdir = m.multiplyDirection(zdir);
		updir = m.multiplyDirection(updir);
		findRotationAngles();
		transformTo = transformFrom = null;
	}

	/** Transform this CoordinateSystem's position by applying a matrix to its origin. */

	public final void transformOrigin(Matrix4 m) {
		orig.set(m.multiply(orig));
		transformTo = transformFrom = null;
	}

	/** Transform this CoordinateSystem's position and orientation by applying a matrix to its origin
	 and axis directions. */

	public final void transformCoordinates(Matrix4 m) {
		orig.set(m.multiply(orig));
		zdir = m.multiplyDirection(zdir);
		updir = m.multiplyDirection(updir);
		findRotationAngles();
		transformTo = transformFrom = null;
	}

	/** Return a matrix which will transform points from this coordinate system to the outside
	 coordinate system with respect to which it is defined. */

	public final Matrix4 fromLocal() {
		if (transformFrom == null)
			transformFrom = Matrix4.objectTransform(orig, zdir, updir);
		return transformFrom;
	}

	/** Return a matrix which will transform points from the outside coordinate system to 
	 this local coordinate system. */

	public final Matrix4 toLocal() {
		if (transformTo == null)
			transformTo = Matrix4.viewTransform(orig, zdir, updir);
		return transformTo;
	}

	/** Calculate the x, y, and z rotation angles given the current values for zdir and updir. 
	 * @param notify */

	private void findRotationAngles() {
		Vector3D v;
		double d;
		Matrix4 m;
		
		if (zdir.val[0] == 0.0 && zdir.val[2] == 0.0) {
			d = Math.sqrt(updir.val[0] * updir.val[0] + updir.val[2] * updir.val[2]);
			rot.val[1] = Math.acos(updir.val[2] / d);
			if (Double.isNaN(rot.val[1]))
				rot.val[1] = Math.acos(updir.val[2] > 0.0 ? 1.0 : -1.0);
			if (zdir.val[1] > 0.0)
				rot.val[1] *= -1.0;
		} else {
			d = Math.sqrt(zdir.val[0] * zdir.val[0] + zdir.val[2] * zdir.val[2]);
			rot.val[1] = Math.acos(zdir.val[2] / d);
			if (Double.isNaN(rot.val[1]))
				rot.val[1] = Math.acos(zdir.val[2] > 0.0 ? 1.0 : -1.0);
			if (zdir.val[0] > 0.0)
				rot.val[1] *= -1.0;
		}
		m = Matrix4.yrotation(rot.val[0]);
		v = m.multiply(zdir);
		d = zdir.abs();
		rot.val[0] = Math.acos(v.val[2] / d);
		if (Double.isNaN(rot.val[0]))
			rot.val[0] = Math.acos(v.val[2] > 0.0 ? 1.0 : -1.0);
		if (v.val[1] < 0.0)
			rot.val[0] *= -1.0;
		m = Matrix4.xrotation(rot.val[0]).multiply(m);
		v = m.multiply(updir);
		d = Math.sqrt(v.val[0] * v.val[0] + v.val[1] * v.val[1]);
		rot.val[2] = Math.acos(v.val[1] / d);
		if (Double.isNaN(rot.val[2]))
			rot.val[2] = Math.acos(v.val[1] > 0.0 ? 1.0 : -1.0);
		if (v.val[0] < 0.0)
			rot.val[2] *= -1.0;
			
	}

	/** A rotation can also be described by specifying a rotation axis, and a rotation angle
	 about that axis.  This method calculates this alternate representation of the to-local
	 rotation.  The from-local transformation is found by simply reversing the sign of the
	 rotation angle.  It returns the rotation angle, and overwrites axis with a unit vector
	 along the rotation axis. */

	public final double getAxisAngleRotation(Vector3D axis) {
		//double a[][] = new double [4][];
		//double b[] = new double [4];
		double ctheta2, cphi, sphi;
		//double phi;
		Vector3D v, xdir;
		//Matrix4 m = toLocal();

		if (zdir.val[2] == 1.0) {
			// If the z-axis is unchanged, it must be the rotation axis.

			axis.set(0.0, 0.0, 1.0);
			if (updir.val[1] == 1.0)
				return 0.0;
		} else if (updir.val[1] == 1.0)
			axis.set(0.0, 1.0, 0.0); // Same for the y-axis;
		else {
			// The rotation axis is the cross product of (newy-oldy) and (newz-oldz).

			axis.set((updir.val[1] - 1.0) * (zdir.val[2] - 1.0) - updir.val[2] * zdir.val[1], updir.val[2] * zdir.val[0] - updir.val[0] * (zdir.val[2] - 1.0), updir.val[0] * zdir.val[1] - (updir.val[1] - 1.0) * zdir.val[0]);
			if (axis.length2() < 1e-6) {
				xdir = updir.crossMultiply(zdir);
				axis.set(xdir.val[1] * (zdir.val[2] - 1.0) - xdir.val[2] * zdir.val[1], xdir.val[2] * zdir.val[0] - (xdir.val[0] - 1.0) * (zdir.val[2] - 1.0), (xdir.val[0] - 1.0) * zdir.val[1] - xdir.val[1] * zdir.val[0]);
			}
			axis.normalise();
		}

		// Now calculate the rotation angle about the axis.

		if (Math.abs(axis.val[2]) < Math.abs(axis.val[1])) {
			v = axis.crossMultiply(zdir);
			ctheta2 = axis.val[2] * axis.val[2];
			sphi = v.val[2] / (ctheta2 - 1.0);
			cphi = (zdir.val[2] - ctheta2) / (1.0 - ctheta2);
		} else {
			v = axis.crossMultiply(updir);
			ctheta2 = axis.val[1] * axis.val[1];
			sphi = v.val[1] / (ctheta2 - 1.0);
			cphi = (updir.val[1] - ctheta2) / (1.0 - ctheta2);
		}
		if (cphi > 0.0)
			return Math.asin(sphi);
		else if (sphi > 0.0)
			return Math.PI - Math.asin(sphi);
		else
			return -(Math.PI + Math.asin(sphi));
	}

	public String toString() {
		String rc = orig.toString();

		rc += " up[" + updir.toString() + "]";
		rc += " zdir[" + zdir.toString() + "]";

		return rc;
	}
}