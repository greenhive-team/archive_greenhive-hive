package at.greenhive.thingy.missionplaner.mission;

public enum enTaskState {
	
	UNASSIGNED, PLANNED, ASSIGNED, INPROGRESS, FINISHED, ABORTED;
	
	public boolean isFinishedSucessfull() {
		return this == FINISHED;
	}
	
	public boolean isFinishedErroneous() {
		return this == ABORTED;
	}
	
	public boolean isStarted() {
		return !(this == UNASSIGNED || this == ASSIGNED) ;
	}

	public boolean isAssigned() {
		return this != UNASSIGNED && this != PLANNED;
	}
	
	public boolean isPlanned() {
		return this != UNASSIGNED;
	}
	
}
