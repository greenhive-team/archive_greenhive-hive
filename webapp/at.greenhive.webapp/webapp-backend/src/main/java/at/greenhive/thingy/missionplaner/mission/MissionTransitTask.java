package at.greenhive.thingy.missionplaner.mission;

import at.greenhive.thingy.missionplaner.IWorkSegment;

public class MissionTransitTask extends MissionTask {
	
	static int id=-1;
	
	public MissionTransitTask(int taskId,  IWorkSegment ws) {
		super( id--, ws);
	}

	public double getRate() {
		return workSegement.getRate();
	}
	
	public IWorkSegment getWorkSegement() {
		return workSegement;
	}
	
	public double getSprayAmount() {
		return workSegement.getSprayAmount();
	}
	
	@Override
	public String toString() {
		return "MissionTransitTask [taskId=" + taskId 
				+ "]";
	}

	@Override
	public String getType() {
		return "Transit";
	}
}