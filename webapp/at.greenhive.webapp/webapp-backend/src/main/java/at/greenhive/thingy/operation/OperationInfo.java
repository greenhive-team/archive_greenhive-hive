package at.greenhive.thingy.operation;

public class OperationInfo {

	private int numMissions;

	private int numTasks;
	private int numFinishedTasks;
	
	private Object estTime;
	private long sprayArea;
	private long estSprayAmount;
	private long sprayAmount;

	public void setNumMissions(int numMissions) {
		this.numMissions = numMissions;
	}

	public void setNumTasks(int numTasks) {
		this.numTasks = numTasks;
	}		

	public void setEstTime(int estTime) {
		this.estTime = estTime;
	}

	public void setSprayArea(long sprayArea) {
		this.sprayArea = sprayArea;
	}

	public void setSprayAmount(long sprayAmount) {
		this.sprayAmount = sprayAmount;
	}
	
	public int getNumMissions() {
		return numMissions;
	}
	
	public int getNumTasks() {
		return numTasks;
	}
	
	public void setNumFinishedTasks(int numFinishedTasks) {
		this.numFinishedTasks = numFinishedTasks;
	}
	
	public int getNumFinishedTasks() {
		return numFinishedTasks;
	}
	
	public Object getEstTime() {
		return estTime;
	}
	
	public void setEstSprayAmount(long estSprayAmount) {
		this.estSprayAmount = estSprayAmount;
	}
	
	public long getEstSprayAmount() {
		return estSprayAmount;
	}
	
	public long getSprayAmount() {
		return sprayAmount;
	}
	
	public long getSprayArea() {
		return sprayArea;
	}

}
