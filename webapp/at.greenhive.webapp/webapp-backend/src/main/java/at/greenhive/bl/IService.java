package at.greenhive.bl;

public interface IService<T> extends IReadOnlyService<T> {

	void create(T entity);
	void save(T entity);
	void delete(T entity);
	
	

}
