package at.greenhive.thingy.swarm;

public class Bumblebee extends Drone {

	public Bumblebee(String serialNumber) {
		this.serialNumber = serialNumber;
		this.state = new DroneState();
	}

	/**
	 * 
	 * @return spray tank capacity milli liter (ml)
	 */
	public double getCapacity() {
		return 8000;
	}

	@Override
	public DroneType getType() {
		return DroneType.BUMBLEBEE;
	}

	@Override
	public boolean needsTransit() {
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bumblebee other = (Bumblebee) obj;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		return true;
	}

}
