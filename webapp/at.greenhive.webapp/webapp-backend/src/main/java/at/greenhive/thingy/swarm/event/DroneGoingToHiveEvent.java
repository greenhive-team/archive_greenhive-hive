package at.greenhive.thingy.swarm.event;

import at.greenhive.protobuf.Airtrafficcontroller.GoingToHiveReason;

public class DroneGoingToHiveEvent extends DroneEvent {

	private GoingToHiveReason reason;

	public DroneGoingToHiveEvent(String droneId, GoingToHiveReason goingToHiveReason) {
		super(droneId);
		this.reason = goingToHiveReason;
	}

	public GoingToHiveReason getReason() {
		return reason;
	}
}
