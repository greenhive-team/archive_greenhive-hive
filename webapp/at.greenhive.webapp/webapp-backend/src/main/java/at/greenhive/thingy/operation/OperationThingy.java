package at.greenhive.thingy.operation;

import java.util.Locale;

import at.greenhive.bl.Thingy;
import me.limespace.appbase.preferences.PreferencesMetaData;

public class OperationThingy extends Thingy {

	@Override
	public String getIconPath() {
		return "Drohne-25.png";
	}

	@Override
	public String getName() {
		return "operation";
	}

	@Override
	public String getDisplayName(Locale locale) {
		return "Operation";
	}

	@Override
	public String getTooltipText(Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PreferencesMetaData getPreferencesMetaData() {
		// TODO Auto-generated method stub
		return null;
	}

}
