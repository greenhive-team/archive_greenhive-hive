package at.greenhive.thingy.missionplaner.event;

import java.util.Optional;
import java.util.Set;

import at.greenhive.thingy.missionplaner.mission.OperationMetaData;

public class MissionSelectionEvent extends AbstractSelectionEvent<OperationMetaData>{

	public MissionSelectionEvent(Optional<OperationMetaData> firstSelectedItem, Set<OperationMetaData> allSelectedItems,
			Object source) {
		super(firstSelectedItem, allSelectedItems, source);		
	}
}
