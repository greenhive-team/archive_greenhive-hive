package at.greenhive.thingy.missionplaner.segmentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.greenhive.bl.util.math.geo3D.Vector3D;
import at.greenhive.thingy.missionplaner.DirectionWorkSegment;
import at.greenhive.thingy.missionplaner.IWorkSegment;
import at.greenhive.thingy.missionplaner.WorkCoordinate;
import at.greenhive.thingy.missionplaner.WorkPatch;
import at.greenhive.thingy.missionplaner.WorkPath;
import at.greenhive.thingy.missionplaner.mission.OperationCroplandParameter;
import at.greenhive.thingy.missionplaner.mission.OperationParameter;

public class VerticalLeftRight extends SegmentationStrategy {
	
	private int rowWith = 3;
	public VerticalLeftRight(OperationCroplandParameter croplandMissionParm, OperationParameter missionParameter) {
		super(croplandMissionParm);
	}

	@Override
	public List<IWorkSegment> createSegments(WorkPatch patch) {
		
		List<IWorkSegment> result = new ArrayList<>();

		List<WorkPath> pathList = patch.getPathList();
		for (WorkPath path:pathList) {
			WorkCoordinate startPoint = null;
			WorkCoordinate endPoint = null;
			for (WorkCoordinate coordinate:path.getPath()) {
				if (endPoint != null) {
					startPoint = endPoint;
					endPoint = coordinate;
					calculate(startPoint,endPoint, result);
				} else {
					endPoint = coordinate;
				}
			}
		}
		
		return result;
	}

	private long calculate(WorkCoordinate startPoint, WorkCoordinate endPoint, List<IWorkSegment> result) {
		Vector3D line = getVector(startPoint, endPoint);
		long pathLen = Math.round(line.length());

		long maxLen = getSegmentLen();
		long segCount = Math.round(pathLen / maxLen);
				
		Vector3D dir = line.norm();
		
		Vector3D offset =  line.crossMultiply(Vector3D.vy());
		offset = offset.norm();
		offset = offset.multiply(rowWith / 2.0);
		
		Vector3D start = new Vector3D(startPoint.getX(), startPoint.getY(), startPoint.getZ() );
		Vector3D newStart1 = start.add(offset);
		Vector3D newStart2 = start.sub(offset);

		Vector3D move = dir.multiply(maxLen);
		
		List<DirectionWorkSegment> backSegments = new ArrayList<>();
		for (int i = 0; i < segCount; i++) {
			
			Vector3D end1 = newStart1.add(move);
			Vector3D end2 = newStart2.add(move);
			
			DirectionWorkSegment s = new DirectionWorkSegment(); // Forward
			s.setRate(croplandMissionParm.getSprayRate()); //TODO: calc user defined spray rate by bounds 
			
			s.addPathPoint(new WorkCoordinate(newStart1.x(), newStart1.y(), newStart1.z()));
			s.addPathPoint(new WorkCoordinate(end1.x(), end1.y(), end1.z()));
			result.add(s);
			
			s = new DirectionWorkSegment(); //Backward
			s.setRate(croplandMissionParm.getSprayRate()); //TODO: calc user defined spray rate by bounds
			s.addPathPoint(new WorkCoordinate(end2.x(), end2.y(), end2.z()));
			s.addPathPoint(new WorkCoordinate(newStart2.x(), newStart2.y(), newStart2.z()));
			
			backSegments.add(s);
			
			newStart1 = end1;
			newStart2 = end2;
		}
		
		result.addAll(backSegments);
		
		return result.size();
	}

	@Override
	public List<IWorkSegment> createTransit(WorkPatch patch) {
		return Collections.emptyList();
	}
	
}
