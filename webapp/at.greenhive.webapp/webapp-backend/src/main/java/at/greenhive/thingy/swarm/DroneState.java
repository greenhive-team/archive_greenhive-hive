package at.greenhive.thingy.swarm;

import java.time.LocalDateTime;

import com.peertopark.java.geocalc.DegreeCoordinate;

import at.greenhive.bl.util.geo.GPSPoint;
import at.greenhive.protobuf.Types.DroneStatus;

public class DroneState {

	double powerLevel;
	double fillLevel;
	
	GPSPoint position;
	LocalDateTime timestamp;
	private DroneStatus status;
	
	public DroneState() {
		status = DroneStatus.UNKNOWN_STATUS;
		timestamp = LocalDateTime.now();
		powerLevel = -1;
		fillLevel = -1;
		position = new GPSPoint(new DegreeCoordinate(0.0),new DegreeCoordinate(0.0),0);
	}
	
	public boolean isIdle() {
		return      status == DroneStatus.IDLE_AIRBORNE_FIELD
				 || status == DroneStatus.IDLE_AIRBORNE_HIVE
				 || status == DroneStatus.IDLE_LANDED_HIVE
				 || status == DroneStatus.IDLE_LANDED_PARKING;
	}
	
	public boolean isReadyForMission() {
		return isIdle() && powerLevel > 12000 && fillLevel > 7500;
	}
	
	public DroneStatus getStatus() {
		return status;
	}
	
	public void setFillLevel(float fillLevel) {
		this.fillLevel = fillLevel;
	}
	
	public void setPowerLevel(double powerLevel) {
		this.powerLevel = powerLevel;
	}
	
	public void setPosition(GPSPoint postition) {
		this.position = postition;
	}
	
	public void setTimestamp(LocalDateTime localDate) {
		this.timestamp = localDate;
	}
	
	public double getFillLevel() {
		return fillLevel;
	}
	
	public GPSPoint getPosition() {
		return position;
	}
	public double getPowerLevel() {
		return powerLevel;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setDroneState(DroneStatus status) {
		this.status = status;
	}

	
}
