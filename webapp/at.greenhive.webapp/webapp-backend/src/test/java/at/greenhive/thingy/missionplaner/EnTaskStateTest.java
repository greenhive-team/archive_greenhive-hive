package at.greenhive.thingy.missionplaner;

import static org.junit.Assert.*;

import org.junit.Test;

import at.greenhive.thingy.missionplaner.mission.enTaskState;

public class EnTaskStateTest {

	@Test
	public void testStartedStates() {
		
		assertFalse( enTaskState.UNASSIGNED.isStarted());
		assertFalse( enTaskState.ASSIGNED.isStarted());
		
		assertTrue( enTaskState.INPROGRESS.isStarted());
		assertTrue( enTaskState.FINISHED.isStarted());
		assertTrue( enTaskState.ABORTED.isStarted());
	}
	
	@Test
	public void testStatesFinishedErroneous() {
		
		assertFalse( enTaskState.UNASSIGNED.isFinishedErroneous());
		assertFalse( enTaskState.ASSIGNED.isFinishedErroneous());
		
		assertFalse( enTaskState.INPROGRESS.isFinishedErroneous());
		assertFalse( enTaskState.FINISHED.isFinishedErroneous());
		assertTrue( enTaskState.ABORTED.isFinishedErroneous());
	}
	
	@Test
	public void testStatesFinishedSucessfull() {
		
		assertFalse( enTaskState.UNASSIGNED.isFinishedSucessfull());
		assertFalse( enTaskState.ASSIGNED.isFinishedSucessfull());
		
		assertFalse( enTaskState.INPROGRESS.isFinishedSucessfull());
		assertTrue( enTaskState.FINISHED.isFinishedSucessfull());
		assertFalse( enTaskState.ABORTED.isFinishedSucessfull());
	}

}
