package at.greenhive.bl.imports;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Ignore;
import org.junit.Test;

public class XLSXImporterTest {

	@Test
	@Ignore
	public void test() throws IOException {
		
		InputStream is = XLSXImporterTest.class.getResourceAsStream("/hive_upload_test.xlsx");
		XLSXImporter importer = new XLSXImporter(null, null);
		
		importer.importData(is);
	}

}
