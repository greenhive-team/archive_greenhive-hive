package at.greenhive.bl.util.geo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import at.greenhive.bl.util.math.geo2d.Line2D;
import at.greenhive.bl.util.math.geo2d.Point2D;

public class Line2DTest {

	@Test
	public void testDistance() {
		
		Point2D p1 = new Point2D(0,0);
		Point2D p2 = new Point2D(1,1);
		Point2D p3 = new Point2D(1,0);
		
		Line2D l = new Line2D(p1,p2);
		
		assertEquals(Math.sqrt(2)/2, l.distance(p3), 10e-7);
		
		l.getProjectedPointOnLine(p3);
	}
	
	@Test
	public void testDistance2() {
		
		Point2D p1 = new Point2D(0,0);
		Point2D p2 = new Point2D(0,1);
		Point2D p3 = new Point2D(.6,.6);
		
		Line2D l = new Line2D(p1,p2);
		
		assertEquals(.6, l.distance(p3), 10e-7);
		
		l.getProjectedPointOnLine(p3);
	}
	
	@Test
	public void testDistance3() {
		
		Point2D p1 = new Point2D(0,0);
		Point2D p2 = new Point2D(1,0);
		Point2D p3 = new Point2D(.7,.7);
		
		Line2D l = new Line2D(p1,p2);
		
		assertEquals(0.7, l.distance(p3), 10e-7);
		
		l.getProjectedPointOnLine(p3);
	}
	
	@Test
	public void testProject1() {
		
		Point2D p1 = new Point2D(0,0);
		Point2D p2 = new Point2D(1,1);
		Point2D p3 = new Point2D(1,0);
		
		Line2D l = new Line2D(p1,p2);
		Point2D pointOnLine = l.getProjectedPointOnLine(p3);
		
		assertEquals(pointOnLine.x, 0.5 , 10e-7);
		assertEquals(pointOnLine.y, 0.5 , 10e-7);
		assertTrue(l.isInside(pointOnLine));
	}

	@Test
	public void testProject2() {
		
		Point2D p1 = new Point2D(0,0);
		Point2D p2 = new Point2D(1,0);
		Point2D p3 = new Point2D(0.9,10);
		
		Line2D l = new Line2D(p1,p2);
		Point2D pointOnLine = l.getProjectedPointOnLine(p3);
		
		assertEquals(pointOnLine.x, 0.9 , 10e-7);
		assertEquals(pointOnLine.y, 0 , 10e-7);
		assertTrue(l.isInside(pointOnLine));
	}
	
	@Test
	public void testProject3() {
		
		Point2D p1 = new Point2D(0,0);
		Point2D p2 = new Point2D(1,2);
		Point2D p3 = new Point2D(0.9,10);
		
		Line2D l = new Line2D(p1,p2);
		Point2D pointOnLine = l.getProjectedPointOnLine(p3);
		
		assertEquals(pointOnLine.x, 4.18 , 10e-7);
		assertEquals(pointOnLine.y, 8.36 , 10e-7);
		assertFalse(l.isInside(pointOnLine));
	}

}
