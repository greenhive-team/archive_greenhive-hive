
package at.greenhive.bl.util.geo;

import org.junit.Assert;
import org.junit.Test;

import com.peertopark.java.geocalc.DMSCoordinate;
import com.peertopark.java.geocalc.DegreeCoordinate;
import com.peertopark.java.geocalc.EarthCalc;
import com.peertopark.java.geocalc.Point;

public class CoordinateConversionsTest extends Assert {
	
	public static final double DEGREES_TO_RADIANS = Math.PI / 180.0;
	
	@Test
	public void calcBearing() {

		GPSPoint p1 = new GPSPoint(new DegreeCoordinate(46.777630), new DegreeCoordinate(15.592464), 300);
		GPSPoint p2 = new GPSPoint(new DegreeCoordinate(46.777671), new DegreeCoordinate(15.592831), 300);
		
		double b = CoordinateConversions.calcBearing(p1, p2);
		System.out.println(b);
		
		b = CoordinateConversions.calcBearing2(p1, p2);
		System.out.println(b);

		p2 = new GPSPoint(new DegreeCoordinate(46.77762881), new DegreeCoordinate(15.59257568), 300);
		p1 = new GPSPoint(new DegreeCoordinate(46.77762448), new DegreeCoordinate(15.59255057), 300);
		
		b = CoordinateConversions.calcBearing2(p1, p2);
		System.out.println(b);
		System.out.println(b * CoordinateConversions.DEGREES_TO_RADIANS);
	}
	
	
	@Test
	public void calcDistance() {

		DMSCoordinate lat1 = new DMSCoordinate(47, 4, 1.617); //
		DMSCoordinate long1 = new DMSCoordinate(16, 8, 47.413); //

		DMSCoordinate lat2 = new DMSCoordinate(47, 4, 1.135); //
		DMSCoordinate long2 = new DMSCoordinate(16, 8, 47.129); //

		double[] c1 = CoordinateConversions.getXYZfromLatLonDegrees(lat1.getDecimalDegrees(), long1.getDecimalDegrees(),
				248.0);
		double[] c2 = CoordinateConversions.getXYZfromLatLonDegrees(lat2.getDecimalDegrees(), long2.getDecimalDegrees(),
				248.0);

		double[] c1a = CoordinateConversions.getXYZfromLatLonDegrees(lat1.getDecimalDegrees(),
				long1.getDecimalDegrees(), 0);
		double[] c2a = CoordinateConversions.getXYZfromLatLonDegrees(lat2.getDecimalDegrees(),
				long2.getDecimalDegrees(), 0);

		double[] c1b = CoordinateConversions.getXYZfromLatLonDegrees(lat1.getDecimalDegrees(),
				long1.getDecimalDegrees(), 6000);
		double[] c2b = CoordinateConversions.getXYZfromLatLonDegrees(lat2.getDecimalDegrees(),
				long2.getDecimalDegrees(), 6000);

		Point p1 = new Point(lat1, long1);
		Point p2 = new Point(lat2, long2);

		double dist_ec = EarthCalc.getDistance(p1, p2);

		double dist_rel = Math.pow(c2[0] - c1[0], 2) + Math.pow(c2[1] - c1[1], 2);
		double dist_rela = Math.pow(c2a[0] - c1a[0], 2) + Math.pow(c2a[1] - c1a[1], 2);
		double dist_relb = Math.pow(c2b[0] - c1b[0], 2) + Math.pow(c2b[1] - c1[1], 2);

		dist_rel = Math.sqrt(dist_rel);
		dist_rela = Math.sqrt(dist_rela);
		dist_relb = Math.sqrt(dist_relb);

		// assertEquals(dist_ec, dist_rel, 0.000000001);

	}
	
	@Test
	public void calcDistance2() {
		DMSCoordinate tlat = new DMSCoordinate(47, 4, 1.617); //
		DMSCoordinate tlon = new DMSCoordinate(16, 8, 47.413); //
		double talt = 300;

		DMSCoordinate olat = new DMSCoordinate(47, 4, 1.135); //
		DMSCoordinate olon = new DMSCoordinate(16, 8, 47.129); //
		double oalt = 300;
		
		double tLatRad = tlat.getDecimalDegrees()* DEGREES_TO_RADIANS;
		double tLonRad = tlon.getDecimalDegrees()* DEGREES_TO_RADIANS;
		
		double oLatRad = olat.getDecimalDegrees()* DEGREES_TO_RADIANS;
		double oLonRad = olon.getDecimalDegrees()* DEGREES_TO_RADIANS;
		
		double deltaLon = tLonRad - oLonRad;
		double deltaLat = tLatRad - oLatRad;  
		
		double dx = deltaLat * 6378137.0; 
		double dy = deltaLon * 6378137.0 * Math.cos(tLatRad);  
		double dz = talt - oalt;
		
		double tr = 6371000 + talt;
		double tx = tr* Math.cos(tLatRad)* Math.cos(tLonRad);
		double ty = tr* Math.cos(tLatRad)* Math.sin(tLonRad);
		double tz = tr* Math.sin(tLatRad);
		
		double or = 6371000 + oalt;
		double ox = or* Math.cos(oLatRad)* Math.cos(oLonRad);
		double oy = or* Math.cos(oLatRad)* Math.sin(oLonRad);
		double oz = or* Math.sin(oLatRad);
		
		
		double dx2 = tx-ox; 
		double dy2 = ty-oy;  
		double dz2 = tz-oz;
		
		System.out.println("V1");
		System.out.println("dx: " + dx);
		System.out.println("dy: " + dy);
		System.out.println("dz: " + dz);
		
		System.out.println("V2");
		System.out.println("dx: " + dx2);
		System.out.println("dy: " + dy2);
		System.out.println("dz: " + dz2);
		
	}

	public void testGPS() throws Exception {
		DMSCoordinate lat1 = new DMSCoordinate(47, 4, 1.523); //
		DMSCoordinate long1 = new DMSCoordinate(16, 8, 43.564); //

		DMSCoordinate lat2 = new DMSCoordinate(47, 4, 1.274); //
		DMSCoordinate long2 = new DMSCoordinate(16, 8, 42.660); //

		Point p1 = new Point(lat1, long1);
		Point p2 = new Point(lat2, long2);

		double dist_ec = EarthCalc.getDistance(p1, p2);
		System.out.println("EarthCalc:" + dist_ec + "(delta:" + (17.8 - dist_ec) + ")");

		double[] c1 = CoordinateConversions.getXYZfromLatLonDegrees(lat1.getDecimalDegrees(), long1.getDecimalDegrees(),
				327.0);
		double[] c2 = CoordinateConversions.getXYZfromLatLonDegrees(lat2.getDecimalDegrees(), long2.getDecimalDegrees(),
				327.0);

		double dist_rel = Math.pow(c2[0] - c1[0], 2) + Math.pow(c2[1] - c1[1], 2);
		dist_rel = Math.sqrt(dist_rel);

		System.out.println("CoordinateConversions:" + dist_rel + "(delta:" + (17.8 - dist_rel) + ")");
	}

	public void testGoogle() throws Exception {
		DegreeCoordinate lat1 = new DegreeCoordinate(47.067078); //
		DegreeCoordinate long1 = new DegreeCoordinate(16.145374); //

		DegreeCoordinate lat2 = new DegreeCoordinate(47.066978); //
		DegreeCoordinate long2 = new DegreeCoordinate(16.145041); //

		Point p1 = new Point(lat1, long1);
		Point p2 = new Point(lat2, long2);

		double dist_ec = EarthCalc.getDistance(p1, p2);
		System.out.println("EarthCalc:" + dist_ec + "(delta:" + (27.3 - dist_ec) + ")");

		double[] c1 = CoordinateConversions.getXYZfromLatLonDegrees(lat1.getDecimalDegrees(), long1.getDecimalDegrees(),
				327.0);
		double[] c2 = CoordinateConversions.getXYZfromLatLonDegrees(lat2.getDecimalDegrees(), long2.getDecimalDegrees(),
				327.0);

		double dist_rel = Math.pow(c2[0] - c1[0], 2) + Math.pow(c2[1] - c1[1], 2);
		dist_rel = Math.sqrt(dist_rel);

		System.out.println("CoordinateConversions:" + dist_rel + "(delta:" + (27.3 - dist_rel) + ")");
	}

	@Test
	public void testGGAFormat( ) {
		
		String value = "4717.11399";
		
		double degLat = Double.parseDouble(value.substring(0,2));
		double minLat = Double.parseDouble(value.substring(2));
		
		degLat += minLat/60;
		
		System.out.println(degLat);
		
		
		
		
	}
}
