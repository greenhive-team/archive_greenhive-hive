package at.greenhive.bl.message;

import org.junit.Test;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import at.greenhive.protobuf.MissionOuterClass.Task;
import at.greenhive.protobuf.MissionOuterClass.TaskType;

public class TestProtobuf {

	@Test
	public void test() throws InvalidProtocolBufferException {
		Task task = Task.newBuilder()
						.setTaskType(TaskType.SPRAY)
						.setSpeed(123.456)
						.build();
		
		System.out.println(JsonFormat.printer().print(task));
		
		byte[] data = task.toByteArray();
		
		Task newTask = Task.parseFrom(data);
		
		System.out.println("Tasktype:" + newTask.getTaskType().name());
		
	}

}
