package at.greenhive.thingy.missionplaner;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.peertopark.java.geocalc.DegreeCoordinate;

import at.greenhive.bl.util.geo.GeoPoint;

public class HiveCoordinateConverterTest {

	@Ignore
	@Test
	public void testHivePos() {
		
		GeoPoint hiveLocation = new GeoPoint(new DegreeCoordinate(47.067007), 
											 new DegreeCoordinate(16.145151),
											 397);
		
		HiveCoordinateConverter c = new HiveCoordinateConverter(hiveLocation, null );
		
		GeoPoint testPoint = new GeoPoint(new DegreeCoordinate(47.067330), new DegreeCoordinate(16.144911), 400);
		
		WorkCoordinate relative = c.toRelative(testPoint);
		GeoPoint result = c.toAbsolut(relative);
		
		assertTrue(testPoint.equals(result));
	}
	
	@Ignore
	@Test
	public void testLocalCSPos() {
		
		GeoPoint origin = new GeoPoint(new DegreeCoordinate(47.067007), 
											 new DegreeCoordinate(16.145151),
											 397);
		
		GeoPoint zPos = new GeoPoint(new DegreeCoordinate(47.077007), 
				 new DegreeCoordinate(16.145151),
				 397);
		
		HiveCoordinateConverter c = new HiveCoordinateConverter(origin, zPos );
		
		GeoPoint testPoint = new GeoPoint(new DegreeCoordinate(47.067330), new DegreeCoordinate(16.144911), 400);
		
		WorkCoordinate relative = c.toRelative(testPoint);
		GeoPoint result = c.toAbsolut(relative);
		
		assertTrue(testPoint.equals(result));
	}
	
	@Test
	public void testCoordinateConversions() {
		
	}

}
