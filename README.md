# Greenhive Hive

## Coding Style Guidelines
If you want to contribute to the project please respect the coding style guidelines of the project outlined in the [.editorconfig](./webapp/.editorconfig) file.

The Eclipse plugin can be found in the [Marketplace](https://marketplace.eclipse.org/content/editorconfig-eclipse).

Please also import the [Greenhive Java Eclipse code style](./Greenhive_Java_Eclipse_code_style.xml) file to Java Code Style/Formatter in your Eclipse installation.
